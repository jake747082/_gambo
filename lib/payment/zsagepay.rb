module Payment
  class Zsagepay
    # @todo add validate : totalAmount >= 0.1

    def initialize(settings)
      @url           = settings['url']
      @merchant_code = settings['merchant_code']
      @md5_key       = settings['md5_key']
      @mer_url       = settings['mer_url']
      @notice_url    = settings['notice_url']
    end

    def pay(withdraw)
      if withdraw.pay_method == '0'
        bank_params(withdraw)
      else
        nobank_params(withdraw)
      end
    end

    def query_order(withdraw)
      params = {
        merchantCode: @merchant_code,
        outOrderId: withdraw.order_id
      }
      response = JSON.parse(post_request("#{@url}ebank/queryOrder.do", params.merge(sign: sign_msg(params, [:merchantCode, :outOrderId]))))

      if response['data'].present? && !withdraw.transferred?
        return false unless (withdraw[:credit_actual] * 100).to_i == response['data']['amount'].to_i
        # @todo think: 直接更新order_info是否適當
        update_params = if response['data']['replyCode'] == '00'
          {status: :transferred, order_info: response}
        else
          {order_info: response}
        end
        withdraw.update!(update_params)
      else
        # 無取得資料
        false
      end
    end

    def return_feedback(params)
      params
    end

    def notice_feedback(withdraw, params)
      return false unless vaild_sign_msg(params, ['instructCode', 'merchantCode', 'outOrderId', 'totalAmount', 'transTime', 'transType'])
      return false unless (withdraw[:credit_actual] * 100).to_i == params['totalAmount'].to_i
      unless withdraw.transferred?
        begin
          ActiveRecord::Base.transaction do
            # 更新訂單
            raise WithdrawUpdateError unless withdraw.update!(status: :transferred, deposit_at: params['transTime'], order_info: params.to_json)
            # 會員加值
            raise UserUpdateError unless User::CreditControl.new(withdraw.user, withdraw[:credit_actual]).deposit!
          end
          {code: '00'}
        rescue => e
          withdraw.waiting_to_deposit!
          ExceptionNotifier.notify_exception(e)
          false
        end
      else
        false
      end
    end

    private

    def post_request(url, params)
      uri              = URI.parse(url)
      http             = Net::HTTP.new(uri.host, uri.port)
      request          = Net::HTTP::Post.new(uri.request_uri)

      request.set_form_data(params)
      http.request(request).body
    end

    def bank_params(withdraw)
      params = {
        bankCode: withdraw.bank_code,
        bankCardType: '01',
        lastPayTime: Time.zone.now.strftime("%Y%m%d%H%M%S"),
        merchantCode: @merchant_code,
        merUrl: @mer_url,
        noticeUrl: @notice_url,
        orderCreateTime: (Time.zone.now + 1.day).strftime("%Y%m%d%H%M%S"),
        outOrderId: withdraw.order_id,
        totalAmount: (withdraw.credit_actual * 100).to_i, # 分
        url: "#{@url}ebank/pay.do"
      }
      params.merge(sign: sign_msg(params, [:lastPayTime, :merchantCode, :orderCreateTime, :outOrderId, :totalAmount]))
    end

    def nobank_params(withdraw)
      params = {
        model: 'QR_CODE',
        merchantCode: @merchant_code,
        outOrderId: withdraw.order_id,
        amount: (withdraw.credit_actual * 100).to_i, # 分
        orderCreateTime: (Time.zone.now + 1.day).strftime("%Y%m%d%H%M%S"),
        lastPayTime: Time.zone.now.strftime("%Y%m%d%H%M%S"),
        noticeUrl: @notice_url,
        isSupportCredit: 1,
        ip: withdraw.ip,
        payChannel: withdraw.pay_method,
      }
      response = JSON.parse(post_request("#{@url}scan/entrance.do", params.merge(sign: sign_msg(params, [:amount, :isSupportCredit, :merchantCode, :noticeUrl, :orderCreateTime, :outOrderId]))))
      return false if response['data'].nil?
      return false unless response['data']['outOrderId'] == withdraw.order_id
      return false unless response['code'] == '00'
      return false unless vaild_sign_msg(response['data'], ['merchantCode', 'orderId', 'outOrderId', 'url'])
      response['data']
    end

    def sign_msg(args = {}, fields = nil)
      fields = fields.nil? ? args : fields
      params = fields.map do |field, _|
        "#{field}=#{args[field]}"
      end.compact.join('&')
      Digest::MD5.hexdigest("#{params}&KEY=#{@md5_key}").upcase
    end
    
    def vaild_sign_msg(params, fields)
      response_sign_msg = params['sign']
      sign_params = {}
      fields.each do |field, _|
        sign_params[field] = params[field]
      end
      sign_msg(sign_params) == response_sign_msg
    end

  end
end