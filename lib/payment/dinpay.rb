module Payment
  class Dinpay

    def initialize(settings)
      @url                  = settings['url']
      @merchant_code        = settings['merchant_code']
      @dinpay_public_key    = settings['dinpay_public_key']
      @merchant_private_key = settings['merchant_private_key']
      @merchant_public_key  = settings['merchant_public_key']
      @return_url           = settings['return_url']
      @notice_url           = settings['notice_url']
    end

    def pay(withdraw)
      params = {
        input_charset: 'UTF-8',
        interface_version: 'V3.0',
        merchant_code: @merchant_code,
        notify_url: @notice_url,
        order_amount: withdraw.credit.to_f.round(2), #元, 小數後兩位
        order_no: withdraw.order_id,
        order_time: Time.zone.now.strftime("%Y-%m-%d %H:%M:%S"),
        product_name: 'Points',
        return_url: @return_url,
        service_type: 'direct_pay',
        sign_type: 'RSA-S',
      }
      params.merge(sign: sign_msg(params), url: @url)
    end

    def return_feedback(params, key = nil)
      return false unless vaild_sign_msg(params, key)
      return false unless params['trade_status'] == 'SUCCESS'
      params
    end

    # def test_return_feedback
    #   params = {
    #     'interface_version'=> 'V3.0',
    #     'merchant_code'=>     '1111110166',
    #     'notify_id'=>         '1234567',
    #     'notify_type'=>       'page_notify',
    #     'order_amount'=>      '10',
    #     'order_no'=>          '20170802155936',
    #     'order_time'=>        '2017-08-03 10:55:20',
    #     'trade_no'=>          '20170802155936',
    #     'trade_status'=>      'SUCCESS',
    #     'trade_time'=>        '2017-08-03 10:55:20',
    #   }
    #   params.merge!('sign'=> sign_msg(params))
    #   return_feedback(params, @merchant_public_key)
    # end

    # def test_notice_feedback(withdraw)
    #   params = {
    #     'interface_version'=> 'V3.0',
    #     'merchant_code'=>     '1111110166',
    #     'notify_id'=>         '1234567',
    #     'notify_type'=>       'page_notify',
    #     'order_amount'=>      '10',
    #     'order_no'=>          '20170802155936',
    #     'order_time'=>        '2017-08-03 10:55:20',
    #     'trade_no'=>          '20170802155936',
    #     'trade_status'=>      'SUCCESS',
    #     'trade_time'=>        '2017-08-03 10:55:20',
    #   }
    #   params.merge!('sign'=> sign_msg(params))
    #   notice_feedback(withdraw, params, @merchant_public_key)
    # end

    def notice_feedback(withdraw, params, key = nil)
      return false unless vaild_sign_msg(params, key)
      return false unless withdraw[:credit_actual].to_f == params['order_amount'].to_f
      status = params['trade_status'] == 'SUCCESS' ? :transferred : :failed
      unless withdraw.transferred?
        begin
          ActiveRecord::Base.transaction do
            # 更新訂單
            raise WithdrawUpdateError unless withdraw.update!(status: status, deposit_at: params['order_time'], order_info: params.to_json)
            # 會員加值
            if status == :transferred
              raise UserUpdateError unless User::CreditControl.new(withdraw.user, withdraw[:credit_actual]).deposit!
            end
          end
          'SUCCESS'
        rescue => e
          withdraw.update!(status: :waiting_to_deposit)
          ExceptionNotifier.notify_exception(e)
          false
        end
      else
        false
      end
    end

    private

    def sign_msg(args = {})
      params = args.map do |field, _|
        "#{field}=#{args[field]}" unless field == :sign_type
      end.compact.join('&')
      private_key = OpenSSL::PKey::RSA.new(@merchant_private_key)
      signature = private_key.sign('md5', params.force_encoding("utf-8"))
      signature = Base64.encode64(signature).squish.delete(' ')
    end

    def vaild_sign_msg(params, key = nil)
      response_sign_msg = params['sign']
      fields = {}
      fields.merge!({bank_seq_no: params['bank_seq_no']}) if params['bank_seq_no'].present?
      fields.merge!({extra_return_param: params['extra_return_param']}) if params['extra_return_param'].present?
      fields.merge!({
        interface_version: params['interface_version'],
        merchant_code:     params['merchant_code'],
        notify_id:         params['notify_id'],
        notify_type:       params['notify_type'],
        order_amount:      params['order_amount'],
        order_no:          params['order_no'],
        order_time:        params['order_time'],
        trade_no:          params['trade_no'],
        trade_status:      params['trade_status'],
        trade_time:        params['trade_time'],
      })

      data = fields.map do |field, value|
        "#{field}=#{value}"
      end.join('&')
      key = key.nil? ? @dinpay_public_key : key
      public_key = OpenSSL::PKey::RSA.new key
      digester   = OpenSSL::Digest::MD5.new
      sign       = Base64.decode64(response_sign_msg)
      public_key.verify(digester, sign, data)
    end
  end
end