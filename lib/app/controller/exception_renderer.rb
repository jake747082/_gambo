module ExceptionRenderer
  extend ActiveSupport::Concern

  included do
    # Exception Handler
    rescue_from ActiveRecord::RecordNotFound, with: :render_404!
    rescue_from Pundit::NotAuthorizedError, with: :render_authorized_error! if defined?(Pundit)
  end

  protected

  # 認證錯誤
  def render_authorized_error!(exception)
    policy_name = exception.policy.class.to_s.underscore
    error_message = t("pundit.#{policy_name}.#{exception.query}", default: t('pundit.shared_policy.authorized_error'))

    respond_to do |format|
      format.html { redirect_to request.referrer || root_url, alert: error_message }
      format.js { render js: "alert(\"#{error_message}\")", status: 403 }
    end
  end

  # 丟出 404 錯誤
  def render_404!
    render 'errors/404', layout: 'error', status: 404
    return false
  end

  # 丟出 500 錯誤
  def render_500!(error_message, error_code=500)
    @error_code = error_code
    @error_message = error_message
    render 'errors/500', layout: 'error', status: @error_code
    return false
  end
end