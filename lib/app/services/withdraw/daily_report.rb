class Withdraw::DailyReport < BaseService
  cattr_accessor :report

  def call(cond)
    cond = { agent: nil, begin_date: Date.today - 7.days, end_date: Date.today + 1.days }.merge(cond)
    @agent, @begin_date, @end_date = cond[:agent], cond[:begin_date], cond[:end_date]
    @report = setup_report_list
  end

  private
  
  def setup_report_list
    withdraw_sum = Withdraw.daily_sum_by_type(@begin_date, @end_date, @agent)
    report = {}
    withdraw_sum.each do |sum|
      report_sum ||= SumStatistics.new
      if sum.cash_type == :deposit
        if sum.type == :ace_pay
          report_sum.extra_deposit += sum.credit_diff
          # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
          report_sum.dc_deposit[sum.currency_id] ||= {amount: 0, credit: 0}
          report_sum.dc_deposit[sum.currency_id][:credit] += sum.credit
          report_sum.dc_deposit[sum.currency_id][:amount] += sum.amount
        elsif sum.type == :admin
          report_sum.deposit += sum.credit_actual
        end
      else
        if sum.type == :ace_pay
          # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
          report_sum.dc_cashout[sum.currency_id] ||= {amount: 0, credit: 0}
          # 以公司角度該提款直接為負數
          report_sum.dc_cashout[sum.currency_id][:credit] += sum.credit
          report_sum.dc_cashout[sum.currency_id][:amount] += sum.amount
        elsif sum.type == :admin
          report_sum.cashout += sum.credit_actual
        end
      end
      report[sum.date.strftime("%Y-%m-%d")] = report_sum
    end
    report
  end

  class SumStatistics
    attr_accessor :deposit, :cashout, :extra_deposit, 
      :dc_deposit, :dc_cashout,
      :currencies

    def initialize
      @deposit = 0
      @cashout = 0
      @extra_deposit = 0

      @dc_deposit = {}
      @dc_cashout = {}
      
      currencies = Currency.all 
      @currencies = currencies.pluck(:id, :name).to_h
      currencies.each do |cur|
        @dc_deposit[cur.id] = {amount: 0, credit: 0} if cur.in_enable?
        @dc_cashout[cur.id] = {amount: 0, credit: 0} if cur.out_enable?
      end
    end
  end

end