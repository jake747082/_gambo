# 代理充提報表
class Withdraw::ChildrenReport < BaseService

  def call(cond)
    @agent, @begin_datetime, @end_datetime = cond[:agent], cond[:begin_date], cond[:end_date]
    @begin_datetime ||= "#{Date.today} 00:00:00"
    @end_datetime ||= "#{Date.today} 23:59:59"

    stats = WithdrawStatistics.new
    stats.self_report = setup_self_report if @agent.present?
    if @agent.present? && @agent.in_final_level?
      # 玩家
      stats.player_report, stats.children_sum = setup_player_report
    else
      # 下層代理
      stats.children_report, stats.children_sum = setup_children_report
    end
    stats
  end

  private
  
  def setup_self_report
    target_agent_withdraw = Withdraw.agent_sum_by_type(@begin_datetime, @end_datetime, @agent)
    self_report = SumStatistics.new
    target_agent_withdraw.each do |withdraw|
      format_report(self_report, withdraw)
    end
    self_report
  end

  def setup_children_report
    children_withdraws = Withdraw.children_sum_by_type(@begin_datetime, @end_datetime, @agent)
    children_report = {}
    children_sum = SumStatistics.new
    children_withdraws.each do |withdraw|
      children_report[withdraw.report_agent] ||= SumStatistics.new
      format_report(children_report[withdraw.report_agent], withdraw, children_sum)
    end
    [children_report, children_sum]
  end

  def setup_player_report
    user_withdraws = Withdraw.player_sum_by_type(@begin_datetime, @end_datetime, @agent)
    player_report = {}
    children_sum = SumStatistics.new
    user_withdraws.each do |withdraw|
      player_report[withdraw.user_id] ||= SumStatistics.new
      format_report(player_report[withdraw.user_id], withdraw, children_sum)
    end
    [player_report, children_sum]
  end

  def format_report(report, withdraw, children_sum = nil)
    return nil if withdraw.nil?
    if withdraw.has_attribute?(:report_agent)
      report.agent_id = withdraw.report_agent 
      report.agent_nickname = withdraw.report_agent_nickname
    end
    report.user_id = withdraw.user_id if withdraw.has_attribute?(:user_id)
    if withdraw.cash_type == :deposit
      if withdraw.type == :admin
        report.total_deposit += withdraw.credit_actual
        report.deposit += withdraw.credit
        unless children_sum.nil?
          children_sum.total_deposit += withdraw.credit_actual 
          children_sum.deposit += withdraw.credit 
        end
      elsif withdraw.type == :extra_deposit
        report.total_extra_deposit += withdraw.credit_actual
        report.extra_deposit += withdraw.credit
        unless children_sum.nil?
          children_sum.total_extra_deposit += withdraw.credit 
          children_sum.extra_deposit += withdraw.credit
        end
      elsif withdraw.type == :ace_pay
        report.total_extra_deposit += withdraw.credit_actual
        report.extra_deposit += withdraw.credit_diff
        # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
        report.dc_deposit[withdraw.currency_id] ||= {amount: 0, credit: 0}
        report.dc_deposit[withdraw.currency_id][:credit] += withdraw.credit
        report.dc_deposit[withdraw.currency_id][:amount] += withdraw.amount
        unless children_sum.nil?
          children_sum.extra_deposit += withdraw.credit_actual
          children_sum.total_extra_deposit += withdraw.credit_diff
          children_sum.total_dc_deposit += withdraw.credit
        end
      end
    else
      if withdraw.type == :admin
        report.total_cashout -= withdraw.credit_actual
        report.cashout -= withdraw.credit
        children_sum.total_cashout -= withdraw.credit_actual unless children_sum.nil?
        children_sum.cashout -= withdraw.credit unless children_sum.nil?
      elsif withdraw.type == :extra_deposit
        report.total_extra_deposit -= withdraw.credit
        report.extra_deposit -= withdraw.credit
        unless children_sum.nil?
          children_sum.total_extra_deposit -= withdraw.credit_actual
          children_sum.extra_deposit -= withdraw.credit
        end
      elsif withdraw.type == :ace_pay
        # 因可能幣別已被關閉，但仍有存提資料需顯示，故設定預設值
        report.dc_cashout[withdraw.currency_id] ||= {amount: 0, credit: 0}
        # 以公司角度該提款直接為負數
        report.dc_cashout[withdraw.currency_id][:credit] += withdraw.credit
        report.dc_cashout[withdraw.currency_id][:amount] += withdraw.amount
        report.total_dc_cashout += withdraw.credit_actual
        children_sum.total_dc_cashout += withdraw.credit_actual unless children_sum.nil?
      end
    end
    [report, children_sum]
  end

  class WithdrawStatistics
    attr_accessor :self_report, :children_report, :player_report, :children_sum, :currencies, :currencies_arr
    
    def initialize
      @currencies_arr = Currency.all 
      @currencies = @currencies_arr.pluck(:id, :name).to_h
    end
  end

  class SumStatistics
    attr_accessor :user_id, :agent_id, :agent_nickname,
          :deposit, :cashout, :extra_deposit, :deposit_actual,
          :dc_deposit, :dc_cashout, 
          :total_deposit, :total_cashout, :total_extra_deposit, :total_dc_deposit, :total_dc_cashout, :total_deposit_actual

    def initialize
      @deposit = 0
      @cashout = 0
      @deposit_actual = 0
      @extra_deposit = 0

      @dc_deposit = {}
      @dc_cashout = {}

      @total_deposit = 0
      @total_cashout = 0
      @total_extra_deposit = 0
      @total_dc_deposit = 0
      @total_dc_cashout = 0
      @total_deposit_actual = 0

      WithdrawStatistics.new.currencies_arr.each do |cur|
        # credit不含贈金
        @dc_deposit[cur.id] = {amount: 0, credit: 0} if cur.in_enable?
        @dc_cashout[cur.id] = {amount: 0, credit: 0} if cur.out_enable?
      end
    end
  end
end