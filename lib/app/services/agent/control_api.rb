class Agent::ControlApi < BaseService

  def initialize(agent)
    @agent = agent
  end

  # 開啟API串接功能
  def open!
    begin
      ActiveRecord::Base.transaction do
        if @agent.in_final_level?
          @agent.update!(account_id: @agent.generate_account_id, secret_key: @agent.generate_secret_key)
        end
        yield(@agent) if block_given?
      end
      true
    rescue
      false
    end
  end

  # 關閉API串接功能
  def close!
    begin
      ActiveRecord::Base.transaction do
        if @agent.in_final_level?
          @agent.update!(secret_key: nil)
        end
        yield(@agent) if block_given?
      end
      true
    rescue
      false
    end
  end
end