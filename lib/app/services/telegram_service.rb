class TelegramService
  require 'net/http'
  
  def initialize(uri, chat_id)
    @uri = uri
    @chat_id = chat_id
  end

  def apply_cashout_msg(record)
    text = "#{record.id}  <b>[進行提幣申請，請至後台進行審核]</b>
交易編號: <code>#{record.trade_no}</code>
兌換數量: #{record.credit} 
給付數量: #{record.after_balance}"
    if Setting.telegram.enable == 'yes'
      res = Net::HTTP.post_form(@uri + 'sendMessage', 'chat_id' => @chat_id, 'parse_mode' => 'HTML', 'disable_web_page_preview' => 'true', 'text' => text)
    else
      res = false
    end
  end

  def signup_msg(record)
    text = "#{record.username}  <b>[已註冊成為會員]</b>"
    if Setting.telegram.enable == 'yes'
      res = Net::HTTP.post_form(@uri + 'sendMessage', 'chat_id' => @chat_id, 'parse_mode' => 'HTML', 'disable_web_page_preview' => 'true', 'text' => text)
    else
      res = false
    end
  end

  def qq_deposit_msg(record)
    text = "#{record.id} <b>[充值成功 !]</b>
幣種: #{record.currency}
虛擬幣數量: #{record.amount} 
TxID: <code>#{record.tx_hash}</code>"
    if Setting.telegram.enable == 'yes'
      res = Net::HTTP.post_form(@uri + 'sendMessage', 'chat_id' => @chat_id, 'parse_mode' => 'HTML', 'disable_web_page_preview' => 'true', 'text' => text)
    else
      res = false
    end
  end
end