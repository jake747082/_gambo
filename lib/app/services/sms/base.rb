module Sms
	class Base

		private

		def send_request(url, response_type = 'json')
			response = Net::HTTP.get_response(URI.parse(url))
			@code = response.code
			if response_type == 'xml'
				Nokogiri::XML(response.body)
			else
				response.body
			end
		end

		def send_post_request(url, params, response_type = 'json')
			begin
				uri = URI.parse(url)
				http = Net::HTTP.new(uri.host, uri.port)
				if uri.port == 443
					http.use_ssl     = true
					http.verify_mode = OpenSSL::SSL::VERIFY_NONE
				end
				request = Net::HTTP::Post.new(uri.request_uri)
				Rails.logger.info "params: #{params.inspect}"

				request.set_form_data(params)

				response = http.request(request)
				Rails.logger.info "response status: #{response.code}"
				Rails.logger.info "response: #{response.body}"
				@code = response.code
				if response.code == "200"
					if response_type == 'xml'
						Nokogiri::XML(response.body)
					else
						response.body
					end
				else
					false
				end
			rescue Exception => e
				Rails.logger.info "Sms::Base Exception: #{e.message}"
				false
			end
		end

		def request_url(url = '', args = {})
			args = args.map do |field, value|
				"#{field}=#{value}"
			end.join('&')
			"#{@url}#{url}?#{args}"
		end

	end
end