module Finance
  class Charts < BaseService

    RECORD_PER_GROUP = 5
    RECORD_PER_CHART = 300

    def call(collection=SlotBetForm)
      collection.send(:reorder, { id: :asc }).pluck(:machine_credit_diff).in_groups_of(RECORD_PER_CHART).map.with_index { |chart_machine_profits, chart_index|
        chart_data_set = chart_machine_profits.in_groups_of(RECORD_PER_GROUP).map.with_index { |machine_profits, group_index|
          company_profit = (machine_profits.map(&:to_f).sum * -1).round(2)
          unit = (chart_index + 1) * RECORD_PER_CHART + (group_index + 1) * RECORD_PER_GROUP
          [unit.to_s, company_profit]
        }
        Hash[chart_data_set]
      }
    end
  end
end