module Finance
  class ChildrenCashout < BaseService

    class CashoutStatistics
      attr_accessor :list, :player_cashout_list, :agent_children, :user_children
      attr_accessor :total_bet_count, :total_win_amount,
        :total_bet_amount, :total_owe_parent_amount,
        :total_profit_amount, :total_jackpot_amount, :total_valid_amount,
        :player_total_profit_amount, :player_total_bet_amount, :player_jackpot_amount,
        :player_total_valid_amount
      attr_accessor :current_agent, :current_agent_own_parent_amount, :current_agent_win_amount, :current_agent_withdraw_sum
      attr_accessor :begin_date, :end_date
      attr_accessor :children_report, :player_report, :children_sum
    end

    def get_user(begin_date, end_date, user)
      stats = CashoutStatistics.new

      return stats if user.nil?

      # Setup Date Range
      stats.begin_date = begin_date
      stats.end_date   = end_date
      stats.user_children = [user]
      @total_player_cashout_list = user.third_party_bet_forms.datetime_range(begin_date, end_date).player_cashout

      player_cashout_list(stats)

      withdraws = Withdraw::UserReport.call(user: user, begin_date: begin_date, end_date: end_date)
      stats.player_report = {user.id => withdraws}
      stats
    end

    def call(begin_date, end_date, agent=nil)
      @agent, @begin_date, @end_date = agent, begin_date, end_date

      # Define Next Agent Level & BetForms Collection
      @agent_level_next, @games_total_bet_forms, @agent_children = cashout_query_information

      # set default value
      @total_cashout_list = []
      @total_player_cashout_list = []

      # 整合所有遊戲的cashout list
      @games_total_bet_forms.each do |bet_forms|
        @total_cashout_list         += cashout_list_for_game(bet_forms)
        if (player_cashout_list_for_game = player_cashout_list_for_game(bet_forms)).present?
          @total_player_cashout_list += player_cashout_list_for_game
        end
      end

      stats = setup_cashout_list
      stats.agent_children = @agent_children

      # Setup current agent cashout info

      if view_agent_cashout_list_mode?

        stats.current_agent_withdraw_sum = {deposit: 0, deposit_actual: 0, cashout: 0, extra_deposit: 0}
        current_agent_withdraw = @agent.all_withdraws.list_transferred.total_withdraw_sum.datetime_range(@begin_date, @end_date)
        current_agent_withdraw.each do |sum| 
          if sum.cash_type == :deposit
            if sum.type == :extra_deposit
              stats.current_agent_withdraw_sum[:extra_deposit] += sum.total_credit
              stats.current_agent_withdraw_sum[:deposit_actual] += sum.total_credit
            else
              stats.current_agent_withdraw_sum[:deposit_actual] = sum.total_credit
              stats.current_agent_withdraw_sum[:deposit] += sum.total_credit
              stats.current_agent_withdraw_sum[:extra_deposit] += sum.total_extra_deposit
            end
          end
        end
        stats.current_agent = @agent
        stats.current_agent_own_parent_amount = 0
        stats.current_agent_win_amount        = 0 

        @games_total_bet_forms.each do |bet_forms|
          game_bet_forms                        = bet_forms.date_range(@begin_date, @end_date)
          stats.current_agent_own_parent_amount += game_bet_forms.sum("#{@agent.agent_level}_owe_parent")
          stats.current_agent_win_amount        += game_bet_forms.sum("#{@agent.agent_level}_win_amount")
        end
      end

      stats
    end

    private

    # 目前是否是觀看某代理報表
    def view_agent_cashout_list_mode?
      @agent.present?
    end

    # 目前是否是觀看某網咖報表
    def view_shop_cashout_list_mode?
      @agent.present? && @agent.in_final_level?
    end

    # 建立報表資料
    def setup_cashout_list
      stats = CashoutStatistics.new

      # Setup Date Range
      stats.begin_date = @begin_date
      stats.end_date   = @end_date

      # Setup Cashout list
      cashout_list(stats)

      # Setup Player Cashout list
      if view_shop_cashout_list_mode?
        stats.user_children = @agent.users
        player_cashout_list(stats)
      end

      # Setup Withdraw list
      withdraws_list(stats)

      stats
    end

    def withdraws_list(stats)
      withdraws = Withdraw::ChildrenReport.call(agent: @agent, begin_date: @begin_date, end_date: @end_date)
      stats.player_report = withdraws.player_report if view_shop_cashout_list_mode?
      stats.children_report = withdraws.children_report
      stats.children_sum = withdraws.children_sum
    end

    # Slot + Racing + LittleMary
    def cashout_list(stats)

      stats.total_bet_count = 0
      stats.total_bet_amount = 0
      stats.total_win_amount = 0
      stats.total_profit_amount = 0
      stats.total_jackpot_amount = 0
      stats.total_owe_parent_amount = 0
      stats.total_valid_amount = 0
      cashout_list = {}

      @total_cashout_list.each do |cashout|
        # set defualt value
        cashout_list[cashout.cashout_agent_id] ||= default_cashout_list
        cashout_list[cashout.cashout_agent_id][:cashout_agent_id]  = cashout.cashout_agent_id
        cashout_list[cashout.cashout_agent_id][:bet_count]         += cashout.bet_count.to_i
        cashout_list[cashout.cashout_agent_id][:profit_amount]     += cashout.profit_amount.to_f
        cashout_list[cashout.cashout_agent_id][:bet_amount]        += cashout.bet_amount.to_f
        cashout_list[cashout.cashout_agent_id][:jackpot_amount]    += cashout.jackpot_amount.to_f
        cashout_list[cashout.cashout_agent_id][:win_amount]        += cashout.win_amount.to_f
        cashout_list[cashout.cashout_agent_id][:owe_parent_amount] += cashout.owe_parent_amount.to_f
        cashout_list[cashout.cashout_agent_id][:valid_amount]      += cashout.valid_amount.to_f

        stats.total_bet_count         += cashout.bet_count.to_i
        stats.total_bet_amount        += cashout.bet_amount.to_f
        stats.total_win_amount        += cashout.win_amount.to_f
        stats.total_profit_amount     += cashout.profit_amount.to_f
        stats.total_jackpot_amount    += cashout.jackpot_amount.to_f
        stats.total_owe_parent_amount += cashout.owe_parent_amount.to_f
        stats.total_valid_amount      += cashout.valid_amount.to_f
      end
      stats.list = cashout_list
      stats
    end

    # 取得某款遊戲的cashout list
    def cashout_list_for_game(bet_forms)
      return bet_forms.none if view_shop_cashout_list_mode?
      bet_forms.date_range(@begin_date, @end_date).children_cashout(@agent_level_next).includes(:cashout_agent)
    end

    # cashout_list default values
    def default_cashout_list
      {
        bet_count: 0,
        profit_amount: 0,
        bet_amount: 0,
        jackpot_amount: 0,
        win_amount: 0,
        owe_parent_amount: 0,
        valid_amount: 0,
      }
    end

    # Next Agent Level & BetForms Collection
    def cashout_query_information
      if view_agent_cashout_list_mode?
        [ @agent.agent_level_next.to_s, [ @agent.all_third_party_bet_forms ], @agent.children ]
      else
        [ 'shareholder', [ ThirdPartyBetForm ], Agent.shareholders ]
      end
    end

    # 取得某款遊戲的player cashout list
    def player_cashout_list_for_game(bet_forms)
      player_cashout_list_for_game = bet_forms.datetime_range(@begin_date, @end_date).player_cashout.includes(:user)
      player_cashout_list_for_game.includes(:machine) if bet_forms.model_name.name == 'SlotBetForm' || bet_forms.model_name.name == 'RacingBetForm'
      player_cashout_list_for_game
    end

    def player_cashout_list(stats)

      stats.player_jackpot_amount = 0
      stats.player_total_profit_amount = 0
      stats.player_total_bet_amount = 0
      stats.player_total_valid_amount = 0
      player_cashout_list = {}

      @total_player_cashout_list.each do |cashout|
        # set defualt value
        type = cashout.user.nil? ? 'machine' : 'user'
        player = cashout.user.nil? ? cashout.machine : cashout.user
        player_cashout_list[player.id] ||= default_player_cashout_list
        player_cashout_list[player.id][:player]        = player
        player_cashout_list[player.id][:type]          = type
        player_cashout_list[player.id][:profit_amount] += cashout.profit_amount.to_f
        player_cashout_list[player.id][:jackpot_amount]+= cashout.jackpot_amount.to_f
        player_cashout_list[player.id][:bet_amount]    += cashout.bet_amount.to_f
        player_cashout_list[player.id][:bet_count]     += cashout.bet_count
        player_cashout_list[player.id][:valid_amount]  += cashout.valid_amount.to_f

        stats.player_jackpot_amount      += cashout.jackpot_amount.to_f
        stats.player_total_profit_amount += cashout.profit_amount.to_f
        stats.player_total_bet_amount    += cashout.bet_amount.to_f
        stats.player_total_valid_amount  += cashout.valid_amount.to_f
      end
      stats.player_cashout_list        = player_cashout_list
      stats
    end

    # player_cashout_list default values
    def default_player_cashout_list
      {
        profit_amount: 0,
        jackpot_amount: 0,
        bet_amount: 0,
        bet_count: 0,
        valid_amount: 0
      }
    end
  end
end
