# 王牌QQ錢包
module Pay
  module Qq
    class Feedback

      class Record
        attr_accessor :id
        attr_accessor :currency
        attr_accessor :amount
        attr_accessor :tx_hash
      end

      def check(params, remote_ip)
        @remote_ip = remote_ip
        if params[:type] == '1'
          deposit!(params)
        elsif params[:type] == '2'
          withdraw!(params)
        else
          false
        end
      end

      private

      def deposit!(params)
        return false unless valid_deposit_params?(params)
        ActiveRecord::Base.transaction do
          log = WalletLog.find_by(type_cd: 1, order_id: params[:serial])
          if log.nil?
            # 新增log
            log = WalletLog.new(log_params(params))
            log.save!
          else
            # update
            log.status = params[:status]
          end

          # 已儲值
          return true if Withdraw.exists?(cash_type_cd: 0, trans_id: params[:serial])

          if log.status == 'done'
            currency = Currency.in_enable.find_by(token_coin: params[:currency])
            return true if currency.nil? # 找不到相對應的幣值
            Rails.logger.info "#{params[:currency]} to #{currency.name} ..."

            user_wallet = UserWallet.find_by(address: params[:to_address], currency_id: currency.id)
            if user_wallet.nil? # 找不到相對應的錢包
              log.save!
              return true
            end

            credit, extra_deposit = transfer_credit(currency, params[:amount].to_f)
            return false if credit < 0 # 無相對應幣值比例
            if credit == 0 # 四捨五入之後等於0
              log.save!
              return true
            end
            before_balance = user_wallet.user.credit_left
            # 新增withdraw
            withdraw = Withdraw.new({
              cash_type: 0,
              user: user_wallet.user,
              trans_id: params[:serial],
              credit: credit,
              credit_diff: extra_deposit,
              credit_actual: (credit.to_f + extra_deposit.to_f).round(2),
              status: 2, # transferred
              type: :ace_pay,
              order_info: params.merge(rate: @rate).to_json,
              from_address: params[:from_address],
              to_address: params[:to_address],
              currency_id: currency.id,
              amount: params[:amount].to_f
            })
            withdraw.save!
            log.withdraw_id = withdraw.id
            log.save!

            # 更新會員最後互動時間
            update_user_last_action_at(user_wallet.user)

            user_wallet.user.create_log(:deposit, user_wallet.user, @remote_ip, credit: withdraw.credit_actual.to_f, withdraw_id: withdraw.id, trade_no: withdraw.trade_no, status: 'transferred', after_balance: get_user_after_balance(user_wallet.user), extra_deposit: extra_deposit.to_f, before_balance: before_balance.to_f)
            if Setting.try(:telegram).present? && Setting.telegram.enable == 'yes'
              uri = URI(Setting.telegram.api_webhook)
              chat_id = Setting.telegram.chat_id
              tg_service = TelegramService.new(uri, chat_id)
              record_bf = Record.new
              record_bf.id = Setting.beginning_lobby_id.to_i + withdraw.user_id.to_i
              record_bf.currency =  params[:currency]
              record_bf.amount =  params[:amount]
              record_bf.tx_hash = params[:tx_hash]
              tg_service.qq_deposit_msg(record_bf)
            true
            end
          else
            # other status
            true
          end
        end
      end

      def withdraw!(params)
        return false unless valid_withdraw_params?(params)
        ActiveRecord::Base.transaction do
          log = WalletLog.withdraws.find_by(order_id: params[:order_id])
          if log.nil?
            # 新增log
            log = WalletLog.new(log_params(params))
            log.save!
          else
            # update
            log.status = params[:status]
          end

          withdraw = Withdraw.find_by(cash_type_cd: 1, trans_id: params[:order_id])
          return true if withdraw.nil? # 找不到該筆提款資料
          return true if withdraw.transferred? || withdraw.failed? # 後台已處理完成
          return false unless withdraw.transaction_processing? # 後台尚未處理完成
          return true if withdraw.to_address.upcase != params[:to_address].upcase # 沒找到相對應的withdraw, 全部轉大寫比對

          before_balance = withdraw.user.credit_left
          if log.status == 'done'
            withdraw.finish!(from_address: params[:from_address], order_info: JSON.parse(withdraw.order_info).merge(params).to_json)
            log.withdraw_id = withdraw.id

            # 遞增提款金額記錄
            credit_control = User::CreditControl.new(withdraw.user, withdraw.credit)
            credit_control.save_cashout_logs!(UserCumulativeCredit.types[:cashout], withdraw.currency_id, withdraw.amount)
            # 金流紀錄
            withdraw.user.create_log(:cashout, withdraw.user, @remote_ip, credit: withdraw.credit_actual.to_f, withdraw_id: withdraw.id, trade_no: withdraw.trade_no, status: 'transferred', after_balance: get_user_after_balance(withdraw.user), before_balance: before_balance.to_f)
          elsif log.status == 'fail'
            withdraw.fail!
            # 金流紀錄
            withdraw.user.create_log(:cashout, withdraw.user, @remote_ip, credit: withdraw.credit_actual.to_f, withdraw_id: withdraw.id, trade_no: withdraw.trade_no, status: 'failed', after_balance: get_user_after_balance(withdraw.user), before_balance: before_balance.to_f)
          end
          log.save!
        end
      end

      # 計算充值後包含其他多錢包總額度
      def get_user_after_balance(user)
        service = GamePlatforms::UserBalance.new(user)
        if user_balance = service.get_balance
          after_balance = (user_balance + user.reload.credit_left).round(2)
        else
          after_balance = -1
        end
      end

      def valid_params?(params)
        if params[:subsystem_num].nil? || params[:subsystem_num].empty? ||
          params[:currency].nil? || params[:currency].empty? ||
          params[:tx_hash].nil? || params[:tx_hash].empty? ||
          params[:vout_index].nil? || params[:vout_index].empty?
          params[:amount].nil? || params[:amount].empty?
          params[:decimals].nil? || params[:decimals].empty?
          params[:from_address].nil? || params[:from_address].empty?
          params[:to_address].nil? || params[:to_address].empty?
          params[:status].nil? || params[:status].empty?
          return false
        end
        return false if params[:subsystem_num] == Setting.pay.qq.subsystem_num.to_s
        return false if params[:amount].to_f <= 0
        true
      end

      def valid_withdraw_params?(params)
        if params[:order_id].nil? || params[:order_id].empty? ||
          params[:created_at].nil? || params[:created_at].empty?
          return false
        end
        true
      end

      def valid_deposit_params?(params)
        if params[:serial].nil? || params[:serial].empty? ||
          params[:received_at].nil? || params[:received_at].empty?
          return false
        end
        true
      end

      def transfer_credit(currency, amount)
        @rate = currency.in_rate
        return -1 unless @rate
        if currency.in_extra_percent > 0 && amount >= currency.in_extra_limit
          @extra_rate = @rate * (currency.in_extra_percent / 100)
        end
        base_amount = @rate.nil? ? 0 : amount * BigDecimal.new(@rate.to_s)
        extra_amount = @extra_rate.nil? ? 0 : amount * BigDecimal.new(@extra_rate.to_s)
        # 計算出來的數值需小數點二位無條件捨去
        [(base_amount * 100).floor / 100.to_f, (extra_amount * 100).floor / 100.to_f]
      end

      def log_params(params)
        {
          type_cd: params[:type].to_i,
          order_id: params[:type] == '1' ? params[:serial] : params[:order_id],
          transaction_hash: params[:tx_hash],
          from_address: params[:from_address],
          to_address: params[:to_address],
          amount: params[:amount],
          coin_decimals: params[:decimals],
          coin_abbr: params[:currency],
          status: params[:status],
          info: params.to_json
        }
      end

      def update_user_last_action_at(user)
        user.update(last_action_at: Time.now)
      end

    end
  end
end