module Pay
  module Funpay
    class Cashout

      def call(withdraw)

        bank_name = withdraw.bank_account_info[:bank_id].nil? ? nil : I18n.t("bank_accounts.bank_title.#{withdraw.bank_account_info[:bank_id]}")
        data = {
          biz_no: "#{Setting.funpay.order_prefix}#{withdraw.trade_no}",
          total_amount: withdraw.credit.to_i * 100,
          total_count: 1,
          request_time: Time.zone.now.strftime("%Y%m%d%H%M%S"),
          pay_item: "#{Setting.funpay.order_prefix}#{withdraw.trade_no},#{withdraw.bank_account_info[:title]},#{withdraw.bank_account_info[:account]},#{withdraw.credit.to_i*100},#{withdraw.user.mobile},,,#{bank_name},#{withdraw.bank_account_info[:province]},#{withdraw.bank_account_info[:city]},#{withdraw.bank_account_info[:subbranch]},1",
        }

        str = {
          MERCHANT_CODE: Setting.funpay.merchant_code,
          BIZ_NO: data[:biz_no],
          TOTAL_AMOUNT: data[:total_amount],
          TOTAL_COUNT: data[:total_count],
          REQUEST_TIME: data[:request_time],
          PAY_ITEM: data[:pay_item],
          VERSION: Setting.funpay.version,
          CHARSET: Setting.funpay.charset,
          SIGN_TYPE: Setting.funpay.sign_type,
          pkey: Setting.funpay.pkey
        }.map{ |field, value| "#{field}=#{value}" }.join('&')
        data[:sign_value] = Digest::MD5.hexdigest(str)

        response = send_request(data)

        if response['ITEM']['status'] == "1"
          withdraw.finish!
          withdraw.update(status: 2, deposit_at: Time.zone.now, order_info: response.to_json, admin_note: response['ITEM']['error_msg'] || response['ERROR_MSG'])
        else
          withdraw.remove!
          withdraw.update(status: 4, deposit_at: Time.zone.now, order_info: response.to_json, admin_note: response['ITEM']['error_msg'] || response['ERROR_MSG'])
        end
        response
      end

      private

      def send_request(data)
        uri              = URI.parse(Setting.funpay.url)
        http             = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl     = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request          = Net::HTTP::Post.new(uri.request_uri)

        request.set_form_data({
          MERCHANT_CODE: Setting.funpay.merchant_code, 
          BIZ_NO: data[:biz_no], 
          TOTAL_AMOUNT: data[:total_amount], 
          TOTAL_COUNT: data[:total_count],
          REQUEST_TIME: data[:request_time],
          PAY_ITEM: data[:pay_item],
          VERSION: Setting.funpay.version,
          CHARSET: Setting.funpay.charset,
          SIGN_TYPE: Setting.funpay.sign_type,
          SIGNVALUE: data[:sign_value]
        })

        response_params = {}

        response = URI.decode(URI.escape(http.request(request).body)).split('&').map do |str| 
          str.split('=')
        end.each do |field, value| 
          response_params[field] = value.nil? ? '' : value
        end

        pay_item_fields = [ "order_id", "funpay_seq_no", "status", "amount", "error_code", "error_msg" ]
        response_pay_item = response_params['PAY_ITEM'].split(',')
  
        response_params['ITEM'] = {}
        pay_item_fields.each_with_index do |field, index|
          response_params['ITEM'][field] = response_pay_item[index].nil? ? '': response_pay_item[index]
        end

        response_params
      end

    end
  end
end