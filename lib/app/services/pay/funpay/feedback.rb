module Pay
  module Funpay
    class Feedback
      # 會員儲值後回傳
      def deposit_result(params)
        return false unless valid_check_code(params)
        withdraw = Withdraw.find(params['remark'].to_i)
        status = case params['stateCode'].to_i
        when 2
          'success'
        when 3
          'fail'
        else
          'retry'
        end
        [withdraw, status]
      end

      # 會員儲值後更新紀錄
      def update_deposit(params)
        return false unless valid_check_code(params)
        withdraw = Withdraw.find(params['remark'].to_i)
        status = case params['stateCode'].to_i
        when 2
          withdraw.finish!
          withdraw.update(status: 2, deposit_at: params['completeTime'].to_datetime.strftime("%Y-%m-%d %H:%M:%S"), order_info: params.to_json)
          User::CreditControl.new(withdraw.user, withdraw.credit_actual).deposit!
          'success'
        when 3
          withdraw.reject!
          withdraw.update(status: 5, order_info: params.to_json)
          'fail'
        else
          'retry'
        end
        [withdraw, status]
      end

      private

      def valid_check_code(params)
        order = {
          orderID: params['orderID'],
          resultCode: params['resultCode'],
          stateCode: params['stateCode'],
          orderAmount: params['orderAmount'],
          payAmount: params['payAmount'],
          acquiringTime: params['acquiringTime'],
          completeTime: params['completeTime'],
          orderNo: params['orderNo'],
          partnerID: params['partnerID'],
          remark: params['remark'],
          charset: params['charset'],
          signType: params['signType']   
        }
        if params['signType'].to_i == 2
          order.merge!(pkey: Setting.funpay.pkey)
        end

        params_string = order.map do |field, value|
          "#{field}=#{value}"
        end.join('&')

        Digest::MD5.hexdigest(params_string) == params['signMsg'] ? true : false
      end

    end
  end
end