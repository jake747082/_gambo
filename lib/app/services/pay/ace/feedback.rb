module Pay
  module Ace
    class Feedback

      def check(params, remote_ip)
        @remote_ip = remote_ip
        if params[:type] == 'withdraw'
          withdraw!(params)
        else
          deposit!(params)
        end
      end

      private

      def deposit!(params)
        return false unless valid_params?(params)
        ActiveRecord::Base.transaction do
          log = WalletLog.find_by(transaction_hash: params[:transactionHash])
          if log.nil?
            # 新增log
            log = WalletLog.new(log_params(params))
            log.save!
          else
            # update
            log.status = params[:status]
          end

          # 已儲值
          return true if Withdraw.exists?(trans_id: params[:transactionHash])

          if log.status == 'done'
            currency = Currency.in_enable.find_by(blockchain: params[:coin_abbr])
            return true if currency.nil? # 找不到相對應的幣值
            Rails.logger.info "#{params[:coin_abbr]} to #{currency.name} ..."

            user_wallet = UserWallet.find_by(address: params[:to_address], currency_id: currency.id)
            if user_wallet.nil? # 找不到相對應的錢包
              log.save!
              return true
            end

            credit, extra_deposit = transfer_credit(currency, params[:amount].to_f)
            return false if credit < 0 # 無相對應幣值比例
            if credit == 0 # 四捨五入之後等於0
              log.save!
              return true
            end
            before_balance = user_wallet.user.credit_left
            # 新增withdraw
            withdraw = Withdraw.new({
              cash_type: 0,
              user: user_wallet.user,
              trans_id: params[:transactionHash],
              credit: credit,
              credit_diff: extra_deposit,
              credit_actual: (credit.to_f + extra_deposit.to_f).round(2),
              status: 2, # transferred
              type: :ace_pay,
              order_info: params.merge(rate: @rate).to_json,
              from_address: params[:from_address],
              to_address: params[:to_address],
              currency_id: currency.id,
              amount: params[:amount].to_f
            })
            withdraw.save!
            log.withdraw_id = withdraw.id
            log.save!

            # 更新會員最後互動時間
            update_user_last_action_at(user_wallet.user)

            user_wallet.user.create_log(:deposit, user_wallet.user, @remote_ip, credit: withdraw.credit_actual.to_f, withdraw_id: withdraw.id, trade_no: withdraw.trade_no, status: 'transferred', after_balance: get_user_after_balance(user_wallet.user), extra_deposit: extra_deposit.to_f, before_balance: before_balance.to_f)
          else
            # pending
            true
          end
        end
      end

      def withdraw!(params)
        return false unless valid_withdraw_params?(params)
        ActiveRecord::Base.transaction do
          log = WalletLog.find_by(transaction_hash: params[:tx_id])
          if log.nil?
            # 新增log
            log = WalletLog.new(withdraw_log_params(params))
            log.save!
          else
            # update
            log.status = params[:status]
          end

          # 已完成
          return true if Withdraw.exists?(trans_id: params[:tx_id])

          if log.status == 'done'
            withdraw = Withdraw.find_by(trans_id: params[:id])
            return true if withdraw.nil? # 找不到該筆提款資料
            return false if !withdraw.transferred? # 後台尚未處理完成
            if withdraw.to_address != params[:to_address]
              # 沒找到相對應的withdraw
              return true
            else
              # withdraw.finish!(trans_id: params[:tx_id])
              withdraw.finish_withdraw(trans_id: params[:tx_id])
              log.withdraw_id = withdraw.id
              log.save!
            end
          end
        end
      end

      # 計算充值後包含其他多錢包總額度
      def get_user_after_balance(user)
        service = GamePlatforms::UserBalance.new(user)
        after_balance = if user_balance = service.get_balance
          (user_balance + user.reload.credit_left).round(2)
        else
          -1
        end
      end

      def valid_withdraw_params?(params)
        if params[:type].nil? || params[:type].empty? ||
          params[:id].nil? || params[:id].empty? ||
          params[:tx_id].nil? || params[:tx_id].empty? ||
          params[:to_address].nil? || params[:to_address].empty? ||
          params[:status].nil? || params[:status].empty?
          return false
        end
        unless ['pending', 'done'].include?(params[:status])
          return false
        end
        true
      end

      def valid_params?(params)
        if params[:transactionHash].nil? || params[:transactionHash].empty? ||
          params[:to_address].nil? || params[:to_address].empty? ||
          params[:amount].nil? || params[:amount].empty? ||
          params[:coin_abbr].nil? || params[:coin_abbr].empty? ||
          params[:status].nil? || params[:status].empty?
          return false
        end
        unless ['pending', 'done'].include?(params[:status])
          return false
        end
        if params[:amount].to_f <= 0
          return false
        end
        true
      end

      def transfer_credit(currency, amount)
        @rate = currency.in_rate
        return -1 unless @rate
        if currency.in_extra_percent > 0 && amount >= currency.in_extra_limit
          @extra_rate = @rate * (currency.in_extra_percent / 100)
        end
        base_amount = @rate.nil? ? 0 : amount * BigDecimal.new(@rate.to_s)
        extra_amount = @extra_rate.nil? ? 0 : amount * BigDecimal.new(@extra_rate.to_s)
        # 計算出來的數值需小數點二位無條件捨去
        [(base_amount * 100).floor / 100.to_f, (extra_amount * 100).floor / 100.to_f]
      end

      def update_user_last_action_at(user)
        user.update(last_action_at: Time.now)
      end

      def log_params(params)
        {
          type_cd: 1,
          order_id: params[:transactionHash],
          transaction_hash: params[:transactionHash],
          from_address: params[:from_address],
          to_address: params[:to_address],
          amount: params[:amount],
          coin_decimals: params[:coin_decimals],
          coin_abbr: params[:coin_abbr],
          status: params[:status],
          info: params.to_json
        }
      end

      def withdraw_log_params(params)
        {
          type_cd: 2,
          order_id: params[:tx_id],
          transaction_hash: params[:tx_id],
          from_address: "",
          to_address: params[:to_address],
          amount: "",
          coin_decimals: "",
          coin_abbr: "",
          status: params[:status],
          info: params.to_json
        }
      end

    end
  end
end