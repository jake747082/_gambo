module Pay
  module Ace
    class Wallet
      def initialize
        @url = Setting.pay.ace.url
        @api_key = Setting.pay.ace.api_key
        @subsystem_num = Setting.pay.ace.subsystem_num
      end

      def get_balance(address, currency)
        params = {
          api_key: @api_key,
          address: address,
          blockchain: currency.blockchain
        }
        data = send_post_request('/balance', params)
        unless data || data['status'].nil?
          return false
        end
        if data['status'] == 200
          data['balance'].to_f.round(2)
        else
          false
        end
      end

      def create_addr(currency)
        # ETH, erc20 共用一般地址
        blockchain = currency.blockchain
        unless ['bitcoin', 'omni', 'ethereum'].include?(blockchain)
          blockchain = 'ethereum'
        end
        params = {
          api_key: @api_key,
          blockchain: blockchain,
          subsystem_num: @subsystem_num,
        }
        data = send_post_request('/createaddress', params)
        unless data || data['status'].nil?
          return false
        end
        if data['status'] == 200
          data['address']
        else
          false
        end
      end

      def transfer(currency, to_address, amount)
        params = {
          blockchain: currency.blockchain,
          api_key: @api_key,
          amount: amount,
          fee: 'low',
          subsystem_num: @subsystem_num,
          to_address: to_address,
        }
        hash = Digest::SHA3.hexdigest(params.values.join(''), 256)
        data = send_post_request('/transfer', params.merge(hash: hash))

        unless data || data['status'].nil?
          return false
        end
        data
      end

      def deposit_history(currency, address)
        params = {
          api_key: @api_key,
          blockchain: currency.blockchain,
          address: address,
        }
        data = send_post_request('/deposit_history', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['history'] : false
      end

      def get_fee(currency)
        params = {
          blockchain: currency.blockchain,
        }
        data = send_post_request('/get_fee', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data : false
      end

      def get_confirmation(hash, currency)
        params = {
          api_key: @api_key,
          hash: hash,
          blockchain: currency.blockchain,
        }
        data = send_post_request('/get_confirmation', params)
        unless data || data['status'].nil?
          return false
        end
        data['status'] == 200 ? data['confirm_num'] : false
      end

      private

      def send_post_request(path, params)
        begin
          Rails.logger.info "url: #{@url + path}"
          uri = URI.parse(@url + path)
          http = Net::HTTP.new(uri.host, uri.port)
          if uri.port == 443
            http.use_ssl     = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          end
          request = Net::HTTP::Post.new(uri.request_uri)
          Rails.logger.info "params: #{params.inspect}"

          request.set_form_data(params)

          response = http.request(request)
          Rails.logger.info "response status: #{response.code}"
          Rails.logger.info "response: #{response.body}"
          if response.code == "200"
            JSON.parse(response.body)
          else
            false
          end
        rescue Exception => e
          Rails.logger.info "Pay::Ace::Wallet Exception: #{e.message}"
          false
        end
      end
    end
  end
end