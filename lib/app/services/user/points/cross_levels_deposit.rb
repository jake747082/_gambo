# 跨層級玩家買分(ex: 股東->玩家)
module User::Points
  class CrossLevelsDeposit

    attr_reader :user_form, :errors

    def initialize(target_agent, user, points)
      @target_agent = target_agent
      @user = user
      @points = points
    end

    def deposit!
      # 限買分代理
      return false unless @target_agent.points?

      status = true
      ActiveRecord::Base.transaction do
        # 代理層往上
        @user.agent.self_and_ancestors.each do |agent|
          if agent.agent_level_cd > @target_agent.agent_level_cd
            form = AgentWithdrawForm::Deposit.new(points: @points, username: agent.username, level_cd: agent.agent_level_cd)
            unless status = form.deposit
              @errors = form.errors
              raise ActiveRecord::Rollback
              return false
            end
          end
        end

        # 玩家
        @user_form = UserForm::Deposit.new(@user, deposit_credit: @points, trans_id: "M#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}", type: Withdraw.types[:agent])
        unless status = @user_form.deposit
          @errors = @user_form.errors
          raise ActiveRecord::Rollback
          return false
        end

      end
      status
    end

  end
end