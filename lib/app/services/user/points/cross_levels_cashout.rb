# 跨層級玩家下分 (ex: 股東<-玩家)
module User::Points
  class CrossLevelsCashout

    attr_reader :user_form, :errors

    def initialize(target_agent, user, points)
      @target_agent = target_agent
      @user = user
      @points = points
    end

    def cashout!
      # 限買分代理
      return false unless @target_agent.points?

      status = true
      ActiveRecord::Base.transaction do
        # 玩家
        @user_form = UserForm::Cashout.new(@user, cashout_credit: @points, trans_id: "M#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}", type: Withdraw.types[:agent])
        unless status = @user_form.cashout
          @errors = @user_form.errors
          raise ActiveRecord::Rollback
          return false
        end

        # 代理層往上
        @user.agent.self_and_ancestors.reverse_each do |agent|
          if agent.agent_level_cd > @target_agent.agent_level_cd
            form = AgentWithdrawForm::Cashout.new(points: @points, username: agent.username, level_cd: agent.agent_level_cd)
            unless status = form.cashout
              @errors = form.errors
              raise ActiveRecord::Rollback
              return false
            end
          end
        end

      end
    end

  end
end