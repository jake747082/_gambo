# 提款轉入玩家錢包(申請 & 提款處理)
class User::TransWallet
  include Shared::Pushable
  APPLY_CASHOUT_EVENT = 'user_apply_cashout'

  attr_reader :error
  attr_reader :error_code
  attr_reader :currency
  attr_reader :amount

  class RecordBrief
    attr_accessor :id
    attr_accessor :trade_no
    attr_accessor :credit
    attr_accessor :after_balance
  end

  def initialize
    @error = {}
  end

  def apply(user, params, remote_ip)
    @user = user
    @type = params[:type]
    @currency = params[:currency]
    # 轉入地址
    @address = params[:address]
    # 遊戲幣
    @credit = params[:amount].to_f
    @trans_id = params[:trans_id]
    @params = params
    return false unless valid_params?

    unless UserWallet.types.include?(@type.to_sym)
      @error = {type: ["invalid"]}
      return false
    end

    withdraw = Withdraw.find_by(trans_id: @trans_id)
    # 已存在trans id, 直接回傳
    unless withdraw.nil?
      @error = {trans_id: ["invalid"]}
      return false
    end

    out_currency = Currency.out_enable.find_by(name: @currency)
    # 無找到符合的幣別
    if out_currency.nil?
      @error = {currency: ["invalid"]}
      return false
    end

    @amount = transfer_credit(out_currency, @credit).round(2)
    if @amount <= 0
      @error = {amount: ["invalid"]}
      return false
    end

    before_balance = @user.credit_left
    # 新增withdraw
    withdraw = Withdraw.new({
        cash_type: 1,
        user: @user,
        trans_id: @trans_id,
        credit: @credit,
        credit_diff: 0,
        credit_actual: @credit,
        status: 0, # pending
        type: :ace_pay,
        order_info: @params.merge(amount: @amount, rate: @rate).to_json,
        to_address: @address,
        currency_id: out_currency.id,
        amount: @amount
      })
    withdraw.save!
    withdraw.user.create_log(:apply_cashout, withdraw.user, remote_ip, credit: withdraw.credit_actual.to_f, withdraw_id: withdraw.id, trade_no: withdraw.trade_no, status: 'transferred', after_balance: get_user_after_balance(withdraw.user), before_balance: before_balance.to_f)
    # 提款提醒
    push_event!(Gambo::Channel::ADMIN, APPLY_CASHOUT_EVENT, user: @user.lobby_id, trade_no: withdraw.trade_no)
    # Telegram 提款提醒
    if Setting.try(:telegram).present? && Setting.telegram.enable == 'yes'
      uri = URI(Setting.telegram.api_webhook)
      chat_id = Setting.telegram.chat_id
      tg_service = TelegramService.new(uri, chat_id)
      record_bf = RecordBrief.new
      record_bf.id = @user.lobby_id
      record_bf.trade_no =  withdraw.trade_no
      record_bf.credit =  withdraw.credit
      record_bf.after_balance = @amount
      tg_service.apply_cashout_msg(record_bf)
    end
    true
  end

  # pay_type : QQ / ACE
  def cashout(withdraw, pay_type)
    return false unless withdraw.may_finish?
    begin
      # 轉入玩家錢包處理
      ActiveRecord::Base.transaction do
        withdraw.lock!
        # 虛擬幣 transfer
        if pay_type == 'ACE'
          wallet = Pay::Ace::Wallet.new
        else
          wallet = Pay::Qq::Wallet.new
        end
        transfer_result = wallet.transfer(withdraw.currency, withdraw.to_address, withdraw.amount)

        if transfer_result['status'] == 200
          if transfer_result['id'].nil?
            @error = {cashout: ["invalid"]}
            return false
          end

          # 成功; 更新withdraw
          if pay_type == 'ACE'
            # 遞增提款金額記錄
            credit_control = User::CreditControl.new(withdraw.user, withdraw.credit)
            credit_control.save_cashout_logs!(UserCumulativeCredit.types[:cashout], withdraw.currency_id, withdraw.amount)
            withdraw.finish! # 成功
          else
            withdraw.transaction_processing! # 待處理
          end
          withdraw.update!(trans_id: transfer_result['id'])
        else
          # 失敗
          @error = transfer_result['status'] == 400 ? {address: ["wrong"]} : {cashout: ["invalid"]}
          @error_code = transfer_result['status']
          return false
        end
      end
    rescue => e
      p e
      false
    end
  end

  private

  # 計算充值後包含其他多錢包總額度
  def get_user_after_balance(user)
    service = GamePlatforms::UserBalance.new(user)
    after_balance = if user_balance = service.get_balance
      (user_balance + user.reload.credit_left).round(2)
    else
      -1
    end
  end

  def transfer_credit(currency, amount)
    @rate = 1 / BigDecimal.new(currency.in_rate.to_s)
    return -1 if @rate.infinite?
    return 0 if amount < currency.out_limit
    real_amount = @rate.nil? ? 0 : amount * BigDecimal.new(@rate.to_s)
    # 計算出來的數值需小數點二位無條件捨去
    (real_amount * 100).floor / 100.to_f
  end

  def valid_params?
    @error = {user: ["invalid"]} if @user.nil?
    @error = {type: ["invalid"]} if @type.nil? || @type.empty?
    @error = {currency: ["invalid"]} if @currency.nil? || @currency.empty?
    @error = {address: ["invalid"]} if @address.nil? || @address.empty?
    @error = {trans_id: ["invalid"]} if @trans_id.nil? || @trans_id.empty? || !@trans_id.start_with?("L")
    @error = {amount: ["invalid"]} if @credit.to_f <= 0

    @error = {type: ["invalid"]} unless UserWallet.types.include?(@type.to_sym)
    @error = {amount: ["not_enough"]} if @user.credit_left < @credit.to_f
    @error.empty? ? true : false
  end
end