class GamePlatforms::BngTransferHandler
  def initialize(params)
    @params = params
    @user = User.find_by(bng_token: @params["token"]) if @params["token"]
    @wl_mode ||= Setting.game_platform.bng.wl
  end

  def login
    return if @params["token"].nil? || @params["uid"].nil?

    # 目前只檢查是否有此會員存在，若之後有不允許遊玩的狀況需再擴充設定
    if @user.nil?
      error_params('INVALID_TOKEN')
    else
      reply = login_params
      # sent_at 為選填，之後有需要再補上
      # sent_at = { sent_at: get_responseDate }
      # reply.merge!(sent_at)
      reply
    end
  end

  def transaction
    return if @params["uid"].nil?

    @user ||= User.find_by(username: @params["args"]["player"]["id"])
    @user.lock!

    # 紀錄收到的資料
    BngTransferLog.create(
      user_id: @user.id,
      session: @params["session"],
      uid: @params["uid"],
      game_id: @params["game_id"],
      game_name: @params["game_name"],
      c_at: @params["c_at"],
      sent_at: @params["sent_at"],
      round_id: @params["args"]["round_id"].to_s,
      bet: @params["args"]["bet"],
      win: @params["args"]["win"],
      round_started: @params["args"]["round_started"],
      round_finished: @params["args"]["round_finished"],
      bonus_info: @params["bonus"]
    )

    begin
      ActiveRecord::Base.transaction do
        # bonus 為空時直接計算金額
        if @params["args"]["bonus"].nil?
          # 一局結束結算 tt
          if @params["args"]["round_started"] & @params["args"]["round_finished"]
            win_minus_bet
          else
            # 多局終局結算 ft
            case @params["args"]["round_finished"]
            when true
              count_single_money('win')
            else
              # 開局結算 tf
              if @params["args"]["round_started"]
                if @params["args"]["win"] != nil
                  win_minus_bet
                else
                  count_single_money('bet')
                end
              # 中盤結算 ff
              else
                if @params["args"]['bet'] && @params["args"]['win']
                  win_minus_bet
                elsif @params["args"]['bet']
                  count_single_money('bet')
                else
                  count_single_money('win')
                end
              end
            end
          end
        # bonus 不為空時不扣賭注
        else
          count_single_money('win')
        end
      end
    rescue => exception
      Rails.logger.info '==== bng transfer failed ===='
      Rails.logger.info exception.message
      Rails.logger.info '==========================='
    end
  end

  def rollback
    return if @params["uid"].nil?

    @user ||= User.find_by(username: @params["args"]["player"]["id"])
    @user.lock!

    # 紀錄收到的資料
    BngRollbackLog.create(
      user_id: @user.id,
      session: @params["session"],
      uid: @params["uid"],
      game_id: @params["game_id"],
      game_name: @params["game_name"],
      c_at: @params["c_at"],
      sent_at: @params["sent_at"],
      round_id: @params["args"]["round_id"].to_s,
      transaction_uid: @params["args"]['transaction_uid'],
      bet: @params["args"]["bet"],
      win: @params["args"]["win"],
      round_started: @params["args"]["round_started"],
      round_finished: @params["args"]["round_finished"]
    )

    transfer_log = BngTransferLog.find_by(uid: @params["args"]['transaction_uid']) if @params["args"]['transaction_uid']
    if transfer_log.nil?
      reply = balance_params
      BngReply.create!(uid: @params["uid"], reply_data: reply)
      reply
    else
      begin
        if @params["args"]["bet"] && @params["args"]["win"]
          money = @params["args"]["win"].to_f - @params["args"]["bet"].to_f
          service = User::CreditControl.new(@user, money.abs)
          money < 0 ? service.transfer_in! : service.transfer_out!
          update_bng_version unless money.zero?
        elsif @params["args"]["bet"]
          service = User::CreditControl.new(@user, @params["args"]["bet"].to_f.abs)
          service.transfer_in!
          update_bng_version unless @params["args"]["bet"].to_f.zero?
        elsif @params["args"]["win"]
          service = User::CreditControl.new(@user, @params["args"]["win"].to_f.abs)
          service.transfer_out!
          update_bng_version unless @params["args"]["win"].to_f.zero?
        end
        reply = balance_params(@params["uid"], 'rollback')
        BngReply.create!(uid: @params["uid"], reply_data: reply)
        reply
      rescue => exception
        Rails.logger.info '==== bng rollback failed ===='
        Rails.logger.info exception.message
        Rails.logger.info '==========================='
      end
    end
  end

  def get_balance
    return if @params["uid"].nil?

    reply = balance_params
    BngReply.create!(uid: @params["uid"], reply_data: reply)
    reply
  end

  private

  def login_params
    {
      uid: @params["uid"],
      player: {
        id: @user.username,
        brand: 'procasino',
        currency: 'CNY',
        mode: 'REAL',
        is_test: check_wl_mode
      },
      balance: {
        value: @user.credit_left.to_s,
        version: @user.get_bng_version
      },
      tag: ''
      # c_at 為選填，之後有需要再補上
      # c_at: ""
    }
  end

  def win_minus_bet
    if @params["args"]["bet"].to_f > @user.credit_left
      reply = error_params('transaction', 'FUNDS_EXCEED')
      BngReply.create!(uid: @params["uid"], reply_data: reply)
      reply
    else
      money = @params["args"]["win"].to_f - @params["args"]["bet"].to_f
      reply = if money < 0 && money.abs > @user.credit_left
                error_params('transaction', 'LOSS_EXCEED')
              else
                service = User::CreditControl.new(@user, money.abs)
                money < 0 ? service.transfer_out! : service.transfer_in!
                update_bng_version unless money.zero?
                balance_params(@params["uid"])
              end
      BngReply.create!(uid: @params["uid"], reply_data: reply)
      reply
    end
  end

  def count_single_money(cmd)
    reply = if cmd == 'bet' && @params["args"]["bet"].to_f.abs > @user.credit_left
              error_params('transaction', 'FUNDS_EXCEED')
            else
              service = User::CreditControl.new(@user, @params["args"][cmd].to_f.abs)
              cmd == 'win' ? service.transfer_in! : service.transfer_out!
              update_bng_version unless @params["args"][cmd].to_f.zero?
              balance_params(@params["uid"])
            end
    BngReply.create!(uid: @params["uid"], reply_data: reply)
    reply
  end

  def update_bng_version
    ver = @user.bng_version
    case ver.last_updated
    when nil
      ver.update!(last_updated: @params["sent_at"], version: ver.version += 1)
    else
      if ver.last_updated.to_time < @params["sent_at"].to_time
        ver.update!(last_updated: @params["sent_at"], version: ver.version += 1)
      end
    end
  end

  def balance_params(uid = nil, cmd = nil)
    user = if cmd == 'rollback'
              BngRollbackLog.find_by(uid: uid).user
           elsif uid != nil
              BngTransferLog.find_by(uid: uid).user
           else
              @user
           end
    {
      uid: @params["uid"],
      balance: {
        value: user.credit_left.to_s,
        version: user.get_bng_version
      }
    }
  end

  def error_params(cmd = nil, code)
    params = {
              uid: @params["uid"],
              error: {
                code: code,
              }
             }
    if cmd == 'transaction'
      msg = {
              balance: {
                value: @user.credit_left.to_s,
                version: @user.get_bng_version
              },
            }
      params.merge!(msg)
    end
    params
  end

  def check_wl_mode
    (@wl_mode == 'prod' || @wl_mode == 'prod1') ? true : false
  end

  # 取得傳送時間，要使用的話記得確認是否使用北京時區
  def get_responseDate
    Time.zone.now.in_time_zone("Beijing").strftime("%Y-%m-%d %H:%M:%S")
  end
end