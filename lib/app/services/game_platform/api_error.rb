class ApiError < StandardError
  attr_reader :code

  def initialize(args)
    @code = args
  end
end
