# 拉取玩家其他多錢包平台上的餘額
class GamePlatforms::UserBalance
  
  attr_reader :error
  
  def initialize(user)
    @user = user
  end
  
  # 若無法拉取異常，則回傳 false，否則為正值
  def get_balance
    # BTS
    bts = GamePlatform.find_by(name: 'bts')
    if !bts.nil? && bts.open?
      bts = GamePlatforms::Bts.new(@user)
      unless @bts_balance = bts.get_balance
        @error = 'bts'
        return false
      end
    else
      @bts_balance = 0
    end
    BigDecimal(@bts_balance.to_s)
  end
end