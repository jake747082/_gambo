# BT
class GamePlatforms::BtTransferHandler
  attr_reader :error

  def initialize(user)
    @user = user
  end
  
  def cashout(trans_id, credit, transfer_type, order_info = '')
    return false unless verify_user_credit_left(credit)
    return true if BtTransferLog.exists?(trans_id: trans_id)

    begin
      ActiveRecord::Base.transaction do
        @user.lock!
        # 新增log
        log = BtTransferLog.new({
          trans_id: trans_id,
          cash_type: 1,
          transfer_type: transfer_type,
          credit: credit,
          credit_before: @user.credit_left,
          credit_after: @user.credit_left.to_f - credit.to_f,
          status: 2, # transferred
          user: @user,
          order_info: order_info
        })
        log.save!
        # 扣款
        service = User::CreditControl.new(@user, credit)
        service.transfer_out!
      end
    rescue => e
      Rails.logger.info e.message
      return false
    end
  end

  def deposit(trans_id, credit, transfer_type, order_info = '')
    return true if BtTransferLog.exists?(trans_id: trans_id)
    begin
      ActiveRecord::Base.transaction do
        @user.lock!
        # 新增log
        log = BtTransferLog.new({
          trans_id: trans_id,
          cash_type: 0,
          transfer_type: transfer_type,
          credit: credit,
          credit_before: @user.credit_left,
          credit_after: (@user.credit_left.to_f + credit.to_f).round(2),
          status: 2, # transferred
          user: @user,
          order_info: order_info
        })
        log.save!
        # 存款
        service = User::CreditControl.new(@user, credit)
        return service.transfer_in!
      end
    rescue => e
      Rails.logger.info e.message
      return false
    end
  end

  private 

  def verify_user_credit_left(credit)
    if credit.to_f > @user.credit_left.to_f
      @error = {cashout_credit: 'Insufficient balance.'} 
      return false
    end
    true
  end
end