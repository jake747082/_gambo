class BaseService
  def self.call(*args, &block)
    if block_given?
      new.call(*args, &block)
    else
      new.call(*args)
    end
  end
end