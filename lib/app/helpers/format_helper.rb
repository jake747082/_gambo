module FormatHelper

  # 1,000
  def default_currency(amount)
    precision =
    case amount
    when Fixnum  then; 0
    when Integer then; 0
    when Float   then; 3
    when BigDecimal then; 3
    end
    number_to_currency(amount, precision: precision, unit: '', format: '%n')
  end

  # 2013-01-02 23:59
  def default_log_time(datetime)
    return "" if datetime.nil?
    datetime = datetime.to_datetime if datetime.is_a? String
    # datetime.to_s(:db)
    datetime.strftime("%Y-%m-%d %H:%M:%S")
  end

  def currency_format(number)
    if number == 0
      "<span>--</span>".html_safe
    elsif number.to_f > 0
      "<span class='font-lose'>#{default_currency(number)}</span>".html_safe
    else
      "<span class='font-win'>#{default_currency(number)}</span>".html_safe
    end
  end

  def user_currency_format(number)
    if number == 0
      "<span>--</span>".html_safe
    elsif number.to_f > 0
      "<span class='font-win'>#{default_currency(number)}</span>".html_safe
    else
      "<span class='font-lose'>#{default_currency(number)}</span>".html_safe
    end
  end

  def bonus_currency(number)
    if number == 0
      "<span>--</span>".html_safe
    else
      "<span class='font-win'>#{default_currency(number)}</span>".html_safe
    end
  end

  def number_format(number)
    if number == 0 || number.nil?
      "0.00"
    elsif number.to_f > 0
      default_currency(number)
    else
      default_currency(number)
    end
  end
end