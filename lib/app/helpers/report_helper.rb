module ReportHelper
  # 顯示虛擬貨幣列表
  def dc_info_html(dc_data, currencies, params = {})
    if dc_data.present?
      "<table class= 'table'>"+
       "<thead>"+
       "<tr>"+
        "<th></th>"+
        "<th class='right'>#{t('withdraws.withdraw_list.piece')}</th>"+
        "<th class='right'>#{t('withdraws.withdraw_list.points')}</th>"+
        "</tr>"+
        "</thead>"+
        "<tbody>"+
        dc_data.map do |key, value|
          "<tr>"+
          "<td>#{currencies[key]}</td>"+
          "<td class='right' data-th='#{t('withdraws.withdraw_list.piece')}'>#{params.empty? || value[:amount] == 0 ? currency_format(value[:amount]) : link_to(currency_format(value[:amount]), details_reports_users_path(params.merge(currency: key)))}</td>"+
          "<td class='right' data-th='#{t('withdraws.withdraw_list.points')}'>#{params.empty? || value[:credit] == 0 ? currency_format(value[:credit]) : link_to(currency_format(value[:credit]), details_reports_users_path(params.merge(currency: key)))}</td>"+
          "</tr>"
        end.join('') +
      "</tbody>"+
      "</table>".html_safe
    end
  end
end