module MachinesHelper
  def render_machine_controller(machine)
    case
    when machine.boot?
      link_to(on_light_html, machine_controls_path(machine, format: :js), method: :delete, remote: true, confirm: t('confirm.shutdown'), class: 'shutdown')
    when machine.cashout?
      link_to(off_light_html, machine_controls_path(machine, format: :js), method: :post, remote: true, confirm: t('confirm.boot'), class: 'boot')
    else
      link_to(t('btn.cashout'), new_machine_cashout_path(machine, format: :js), method: :get, remote: true, class: 'btn btn-xs btn-default')
    end
  end

  def render_machine_status(machine)
    machine.boot? ? on_light_html : off_light_html
  end

  private

  def on_light_html
    "<i class='fa fa-power-off font-on'></i>".html_safe
  end

  def off_light_html
    "<i class='fa fa-power-off font-off'></i>".html_safe
  end
end