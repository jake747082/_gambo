module IconHelper

  YES_ICON = "<i class='font-enable fa fa-check'></i>".html_safe
  NO_ICON = "<i class='font-disable-light fa fa-times'></i>".html_safe

  def icon_check(resource, method, enable=true)
    if enable
      if resource.send(method) then YES_ICON else NO_ICON end
    else
      if resource.send(method) then NO_ICON else YES_ICON end
    end
  end
end