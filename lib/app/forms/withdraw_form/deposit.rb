class WithdrawForm::Deposit < BaseForm::Basic
  form_name :withdraw

  attr_reader :user, :withdraw_id, :credit_actual, :model

  attribute :user
  attribute :credit, Float
  attribute :credit_diff, Float
  attribute :cashout_limit, Float
  attribute :credit_actual, Float
  attribute :type, String
  attribute :cash_type, String
  attribute :note, String
  attribute :admin_note, String
  attribute :city, String
  attribute :province, String
  attribute :subbranch, String
  attribute :account, String
  attribute :bank_id, String
  attribute :deposit_at, String
  attribute :title, String
  attribute :mobile, String
  attribute :trans_id, String

  # attr_accessor :city, :province, :subbranch, :account, :bank_id, :deposit_at, :title
  # validates :cashout_limit, presence: true
  validates :credit, presence: true
  validates :user, presence: true
  validates_numericality_of :credit, greater_than_or_equal_to: 0.01
  validate :verify_credit_actual
  validate :verify_agent_credit_left, unless: :verify_user

  def initialize(params={})
    super(params)
  end

  def deposit
    _credit = credit
    if(type == "extra_deposit")
      credit_diff = _credit
      _credit = 0
    else
      credit_diff = 0
    end
    User.transaction do
      return false unless valid?
      @model = Withdraw.new({
        cash_type: 0,
        user: user,
        trans_id: trans_id,
        credit: credit,
        # credit_diff: 0,
        credit_diff: credit_diff,
        credit_actual: credit_actual,
        cashout_limit: credit,
        deposit_at: deposit_at,
        # cashout_limit: cashout_limit,
        status: 2, # transferred
        note: note,
        admin_note: admin_note,
        type: type,
        bank_id: bank_id,
        city: city,
        province: province,
        subbranch: subbranch,
        account: account,
        title: title,
        mobile: mobile
      })
      model.save!
      @withdraw_id = model.id
    end
    true
  end

  private

  def verify_agent_credit_left
    return false if user.nil?
    if user.agent.is_points_type? && credit.to_f > user.agent.credit_left
      errors.add(:credit, :less_than, credit_left: user.agent.credit_left)
    end
  end

  def verify_credit_actual
    # @credit_actual = credit.to_f + credit_diff.to_f
    @credit_actual = credit.to_f
    if credit_actual <= 0
      errors.add(:credit_actual, :invalid)
    end
  end

  def verify_user
    errors.add(:user, :invalid) if user.nil?
  end
end