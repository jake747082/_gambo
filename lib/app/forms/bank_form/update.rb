class BankForm::Update < BaseForm::Reform
  model :bank_account

  property :bank_id, type: Integer
  property :city
  property :province
  property :subbranch
  property :account
  property :security_code
  property :security_code_confirmation, virtual: true

  validates :account, presence: true, length: { in: 6..25 }
  validates_uniqueness_of :account
  validates_presence_of :bank_id, :city, :province, :subbranch

  validates :security_code, confirmation: true, length: { is: 6 }, numericality: true, unless: :security_code_blank?
  validates :security_code_confirmation, presence: true, unless: :security_code_blank?

  def update(params)
    @security_code = params[:security_code]
    p @security_code
    if @security_code.blank? || @security_code.nil?
      params.delete(:security_code)
      params.delete(:security_code_confirmation)
    end
    p params
    save_with_transaction(params)
  end

  private
  
  def security_code_blank?
    @security_code.blank?
  end
end