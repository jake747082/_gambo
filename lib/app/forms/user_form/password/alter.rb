class UserForm::Password::Alter < BaseForm::Basic

  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations

  attr_accessor :user
  attr_accessor :current_password, :new_password, :new_password_confirmation

  # Add all validations you need
  validate :new_password, presence: true, length: { in: 8..24, message: 'wrong_format' }, confirmation: { message: 'confirmation' }
  validate :new_password_confirmation, presence: {message: 'confirmation'}

  def initialize(params={})
    super(params)
  end

  def alter
    begin
      return false unless valid?
      ActiveRecord::Base.transaction do
        @user.lock!
        @user.update_with_password(current_password: current_password, password: new_password, password_confirmation: new_password_confirmation)
      end
    rescue => e
      p e.message
      false
    end
  end
end