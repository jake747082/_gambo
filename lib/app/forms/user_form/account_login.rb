# 大廳帳號登入
class UserForm::AccountLogin < BaseForm::Basic

    attribute :auth_type_cd
    attribute :username
    attribute :password
    attribute :ip
  
    validates :auth_type_cd, numericality: {message: 'invalid'}, allow_nil: true
    validates :username, length: { in: 6..128 , message: 'invalid'}, format: { with: /\A[a-zA-Z0-9._]+\z/ , message: 'invalid'}, allow_blank: true
    validates :password, length: { in: 8..24, message: 'wrong_format' }, allow_nil: true
  
    attr_reader :user
  
    def initialize(params={})
      super(params)
    end
  
    def account_login
      return false unless valid?
      if auth_type_cd == 2
        @user = User.find_by(username: username)
      end  
      if @user.nil?
        errors.add(:password, 'not_match')
        return false
      end
      if @user.lock?
        errors.add(:user, 'lock')
        return false
      end
      unless @user.valid_password?(password)
        errors.add(:password, 'not_match')
        return false
      end
      @user.auth_token = User.generate_auth_token
      @user.last_sign_in_at = @user.current_sign_in_at
      @user.last_sign_in_ip = @user.current_sign_in_ip
      @user.current_sign_in_at = Time.zone.now
      @user.current_sign_in_ip = ip
      @user.save!
      return true
    end
  end