# 大廳登入
class UserForm::Login < BaseForm::Basic

  attribute :auth_type_cd
  attribute :email
  attribute :mobile
  attribute :username
  attribute :password
  attribute :ip

  validates :auth_type_cd, numericality: {message: 'invalid'}, allow_nil: true
  validates :email, format: {with: Devise::email_regexp, message: 'invalid'}, length: {maximum: 255, message: 'invalid'}, allow_blank: true
  validates :mobile, numericality: {message: 'invalid'}, length: {minimum: 11, maximum: 13, message: 'invalid'}, allow_blank: true
  validates :username, length: { in: 6..128 , message: 'invalid'}, format: { with: /\A[a-zA-Z0-9._]+\z/ , message: 'invalid'}, allow_blank: true
  validates :password, presence: true, length: { in: 8..24, message: 'wrong_format' }

  attr_reader :user

  def initialize(params={})
    super(params)
  end

  def login
    return false unless valid?
    case auth_type_cd
    when 0
      @user = User.find_by(mobile: mobile)
    when 1
      @user = User.find_by(email: email)
    end

    if @user.nil?
      errors.add(:password, 'not_match')
      return false
    end
    if @user.lock?
      errors.add(:user, 'lock')
      return false
    end
    unless @user.valid_password?(password)
      errors.add(:password, 'not_match')
      return false
    end
    @user.auth_token = User.generate_auth_token
    @user.last_sign_in_at = @user.current_sign_in_at
    @user.last_sign_in_ip = @user.current_sign_in_ip
    @user.current_sign_in_at = Time.zone.now
    @user.current_sign_in_ip = ip
    @user.save!
    return true
  end
end