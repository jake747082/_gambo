class UserForm::Cashout < BaseForm::Basic
  i18n_prefix :forms
  form_name :cashout

  attr_reader :user, :credit_left, :withdraw, :refund_model

  attribute :cashout_credit, Float
  attribute :trans_id, String
  attribute :type, Integer
  attribute :order_info

  validates :cashout_credit, presence: true
  validates_numericality_of :cashout_credit, greater_than_or_equal_to: 0.01
  validate :verify_user_credit_left

  def initialize(user, params={})
    @user = user
    @credit_left = user.credit_left
    @cashout_credit ||= 0
    super(params)
  end

  def cashout
    unless Withdraw.exists?(trans_id: trans_id)
      User.transaction do
        user.lock!
        return false unless valid?
        # 新增withdraw
        @withdraw = Withdraw.new({
          cash_type: 1,
          user: user,
          trans_id: trans_id,
          credit: cashout_credit,
          credit_actual: cashout_credit,
          status: 2, # transferred
          type: type || 0,
          order_info: order_info
        })
        withdraw.save!
        if type == Withdraw.types[:agent]
          @refund_model = AgentWithdraw.new({
            cash_type_cd: 2,
            agent: user.agent,
            points: cashout_credit
          })
          refund_model.save!
          user.agent.increase_points!(cashout_credit)
        end
      end
    end
    true
  end

  private

  def verify_user_credit_left
    if cashout_credit.to_f > user.credit_left
      errors.add(:cashout_credit, :less_than, credit_left: user.credit_left)
    end
  end
end
