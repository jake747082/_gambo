# 大廳註冊
class UserForm::Signup < BaseForm::Basic

  attribute :auth_type_cd
  attribute :email
  attribute :mobile
  attribute :checkcode
  attribute :password
  attribute :password_confirmation

  validates :auth_type_cd, numericality: {message: 'invalid'}, allow_nil: true
  validates :email, format: {with: Devise::email_regexp, message: 'invalid'}, length: {maximum: 255, message: 'invalid'}, allow_blank: true
  validates :mobile, numericality: {message: 'invalid'}, length: {minimum: 11, maximum: 13, message: 'invalid'}, allow_blank: true
  validates :password, presence: true, length: { in: 8..24, message: 'wrong_format' }, confirmation: { message: 'confirmation' }
  validates :password_confirmation, presence: {message: 'confirmation'}
  validates :checkcode, numericality: {message: 'invalid'}, length: { is: 6, message: 'invalid' }
  validate :verify_username, :verify_checkcode, :verify_agent

  attr_reader :user

  class RecordBrief
    attr_accessor :username
  end
  
  def initialize(agent, params={})
    @agent = agent
    @original_username = params[:username]
    super(params)
  end

  def create
    begin
      return false unless valid?
      ActiveRecord::Base.transaction do
        @params = {
          username: generate_username,
          auth_type_cd: @unconfirmed_user.auth_type_cd,
          email: @unconfirmed_user.email,
          mobile: @unconfirmed_user.mobile,
          password: password,
          auth_token: User.generate_auth_token
        }
        @agent.self_and_ancestors.lock!.each do |update_agent|
          # 1. 往上遞增所有代理樹 total_users_count
          update_agent.increment!(:total_users_count)
          # 2. 把 shareholder, director 依序往上填 id
          @params["#{update_agent.agent_level}_id".to_sym] = update_agent.id
        end
        @user = User.create!(@params)
        @unconfirmed_user.destroy!
      end
      # Telegram 註冊提醒
      if Setting.try(:telegram).present? && Setting.telegram.enable == 'yes'
        uri = URI(Setting.telegram.api_webhook)
        chat_id = Setting.telegram.chat_id
        tg_service = TelegramService.new(uri, chat_id)
        record_bf = RecordBrief.new
        record_bf.username = @original_username
        tg_service.signup_msg(record_bf)
      true
      end          
    rescue => e
      p e.message
      false
    end
  end

  private

  def verify_username
    if auth_type_cd == 0
      if @is_exists = User.exists?(mobile: mobile)
        errors.add(:mobile, 'unique')
        return
      end
    else
      if @is_exists = User.exists?(email: email)
        errors.add(:email, 'unique')
        return
      end
    end
  end

  def verify_checkcode
    if auth_type_cd == 0
      @unconfirmed_user = UnconfirmedUser.find_by(mobile: mobile)
      if @unconfirmed_user.nil?
        errors.add(:mobile, 'not_check')
        return
      end
    else
      @unconfirmed_user = UnconfirmedUser.find_by(email: email)
      if !@is_exists && @unconfirmed_user.nil?
        errors.add(:email, 'not_check')
        return
      end
    end
    if @unconfirmed_user.verification_code.nil? || @unconfirmed_user.verification_code.empty?
      errors.add(:checkcode, 'invalid')
      return
    end
    if Time.zone.now > @unconfirmed_user.verification_send_at + 5.minutes
      errors.add(:confirmation_token, 'invalid')
      return
    end
    unless @unconfirmed_user.verification_code == checkcode
      errors.add(:checkcode, 'invalid')
    end
  end

  def verify_agent
    errors.add(:agent, 'invalid') if @agent.nil?
  end

  # 亂數產生 username
  def generate_username
    username = loop do
      random_username = SecureRandom.hex(10)
      break random_username unless User.exists?(username: random_username)
    end
  end
end