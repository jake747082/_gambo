class AgentForm::SubAccount::Base < SharedForm::Authable
  model :agent_sub_account

  property :machine_read_permission
  property :machine_write_permission
  property :finances_permission
  property :agent_read_permission
  property :agent_write_permission


  property :agent, virtual: true
  validates :agent, presence: true
  # 暱稱
  validates :nickname, presence: true

  validate :valid_user_permission

  # 若主帳號無觀看會員權限, 則子帳號無法對machine_write_permission做編輯
  def valid_user_permission
    if !agent.user_permission? && machine_write_permission != '0'
      errors.add(:machine_write_permission, I18n.t('activerecord.errors.machine_write_permission.invalid'))
    end
  end
end