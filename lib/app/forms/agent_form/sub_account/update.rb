class AgentForm::SubAccount::Update < AgentForm::SubAccount::Base
  property :username, virtual: true

  alias_method :update, :save_with_transaction
end