class AgentWithdrawForm::Deposit < BaseForm::Basic
  form_name :agent_withdraw

  attr_reader :agent, :points, :model, :cashout_model

  attribute :points, Float
  attribute :note, String
  attribute :admin_note, String
  attribute :username, String
  attribute :city, String
  attribute :province, String
  attribute :subbranch, String
  attribute :account, String
  attribute :bank_id, String
  attribute :title, String
  attribute :mobile, String

  attribute :level_cd, Integer

  validates :points, presence: true
  validates :username, presence: true
  validates :level_cd, presence: true
  validates_numericality_of :points, greater_than_or_equal_to: 0.01
  validate :verify_agent
  validate :verify_use_points

  def initialize(params={})
    super(params)
  end

  def deposit
    Agent.transaction do
      return false unless valid?
      @model = AgentWithdraw.new({
        cash_type_cd: 0,
        agent: agent,
        points: points,
        note: note,
        admin_note: admin_note,
        bank_id: bank_id,
        city: city,
        province: province,
        subbranch: subbranch,
        account: account,
        title: title,
        mobile: mobile
      })
      model.save!
      agent.increase_points!(points)
      # points 扣上層額度
      unless agent.parent.nil?
        @cashout_model = AgentWithdraw.new({
          cash_type_cd: 3,
          agent: agent.parent,
          points: points,
          note: note,
          admin_note: admin_note,
          bank_id: bank_id,
          city: city,
          province: province,
          subbranch: subbranch,
          account: account,
          title: title,
          mobile: mobile
        })
        cashout_model.save!
        agent.parent.use_points!(points)
      end
    end
    true
  end

  private

  def verify_use_points
    errors.add(:points, :invalid, points: 0) if points < 0
    return false if agent.nil? || agent.parent.nil?
    if points.to_f > agent.parent.points
      errors.add(:points, :less_than, points: agent.parent.points)
    end
  end

  def verify_agent
    if @agent = Agent.find_by(username: username, agent_level_cd: level_cd)
      unless @agent.is_points_type?
        errors.add(:agent, :type_invalid)
      end
    else
      errors.add(:username, :invalid)
    end
  end
end