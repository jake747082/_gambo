class PlatformGame < ActiveRecord::Base
  belongs_to :game_platform
  before_save :update_jp

  scope :enable, -> { where(enable: 1).group_by(&:game_type) }

  def rate_percent
    self.jp_rate * 0.01
  end

  def update_jp
    self.jp_md = (self.bet_total_credit - self.game_reset) * self.rate_percent
  end

  def self.sort_games_from_new_to_old
    games_list = where(enable: 1).group_by(&:game_type)
    reverse_list = {}
    games_list.each { |key, val| reverse_list[key] = val.sort_by(&:id).reverse! }
    reverse_list
  end

end