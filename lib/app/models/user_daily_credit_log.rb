# (暫不使用)
class UserDailyCreditLog < ActiveRecord::Base
  belongs_to :users
  shared do
    # @notice 
    # 在做新增type時, 請確認bet_{platform_id}是否對應ThirdPartyBetForm的game_platform_id, 
    # 若不是, 則需去修改UserDailyCreditLog::Import bet_forms_logs的type_cd
    # 2020/06/08, 重新定義type : 虛擬幣存提 (dc_deposit: 2, dc_cashout: 3)
    # as_enum :type, deposit: 0, cashout: 1, dc_deposit: 2, dc_cashout: 3#, bet_east: 2, bet_bbin: 3, bet_ag: 4, bet_mg: 5, bet_cagayan: 6, bet_bt: 7, bet_kgame: 8, bt_cq9: 9, bt_sf: 10, bt_bts: 11, bt_ag_sw: 12
    as_enum :type, deposit: 0, cashout: 1

  end
  scope :today, -> {
    where('date = ?', Time.zone.now.to_date.strftime('%Y-%m-%d'))
  }

  scope :sum_credit_by_user, -> {
    select('user_id, type_cd, currency_id, SUM(count) as count, SUM(total) as total, SUM(dc_total) as dc_total')
    .group('user_id, type_cd, currency_id')
  }

  def self.increase_credit!(user, type_cd, credit, date = Time.zone.now.to_date.strftime('%Y-%m-%d'), count = 1)
    today_credit_log = find_or_create_by(user_id: user.id, date: date, type_cd: type_cd)
    today_credit_log.increment!(:count, count)
    today_credit_log.increment!(:total, credit.to_f.round(4))
  end
end