class BankAccount < ActiveRecord::Base
  include SensitiveFilter
  before_save :encrypt_security_code
  belongs_to :accountable, polymorphic: true

  validates :account, presence: true, length: { in: 6..25 }
  
  validates_uniqueness_of :account
  validates_presence_of :bank_id, :city, :province, :subbranch
  validates_presence_of :security_code, :security_code_confirmation, on: :create
  
  validates :security_code, confirmation: true, numericality: true, length: { is: 6 }, unless: :security_code_blank?
  validate :valid_security_code_format, on: :create


  def is_user_account?
    @is_user_account ||= accountable.is_a?(User)
  end

  def security_code_blank?
    security_code.nil? || security_code.blank?
  end

  def encrypt_security_code
    if security_code_changed?
      self.security_code = BCrypt::Password.create(security_code)
    end
  end

  def valid_security_code_format
    if /\A[0-9]+\z/.match(security_code).nil?
      errors.add(:security_code, I18n.t('activerecord.errors.bank_account.security_code_invalid'))
    end
  end

  def self.valid_security_code(encrypt_security_code, security_code)
    BCrypt::Password.new(encrypt_security_code) == security_code
  end

  def self.bank_options
    ['ICBK', 'ABOC', 'BKCH', 'PCBC', 'COMM', 'CMBC', 'MSBC', 'GDBK', 'EVER', 'HXBK', 'FJIB', 'SZDB', 'SPDB', 'BJCN', 'BOSH', 'DGCB', 'GZCB', 'HZCB', 'CHBH', 'HASE', 'OTHER']
  end

  def human_bank_title
    I18n.t("bank_accounts.bank_title.#{bank_id}")
  end
end