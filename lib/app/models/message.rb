class Message < ActiveRecord::Base
  default_scope { where(deleted: false).order(id: :desc) }
  belongs_to :user
  belongs_to :agent

  has_many :read_messages

  cattr_accessor :groups

  scope :search, -> (users, agents) {
    if users.present? && agents.present?
      where("user_id IN (?) OR agent_id IN (?)", users.map{ |user| user.id }, agents.map{ |agent| agent.id })
    elsif users.present?
      where(user: users)
    elsif agents.present?
      where(agent: agents)
    end
  }

  # Scopes
  # scope :admin_list, -> {
  #   where(admin: true)
  # }

  # scope :agent_list, -> {
  #   where(agent: true)
  # }

  # scope :www_list, -> {
  #   where(www: true)
  # }
end
