class HotGame < ActiveRecord::Base
    belongs_to :game1, :class_name => "PlatformGame", :foreign_key => "game1_id"
    belongs_to :game2, :class_name => "PlatformGame", :foreign_key => "game2_id"
    belongs_to :game3, :class_name => "PlatformGame", :foreign_key => "game3_id"
    belongs_to :game4, :class_name => "PlatformGame", :foreign_key => "game4_id"
    belongs_to :game5, :class_name => "PlatformGame", :foreign_key => "game5_id"
    belongs_to :game6, :class_name => "PlatformGame", :foreign_key => "game6_id"
    belongs_to :game7, :class_name => "PlatformGame", :foreign_key => "game7_id"
    belongs_to :game8, :class_name => "PlatformGame", :foreign_key => "game8_id"
    belongs_to :game9, :class_name => "PlatformGame", :foreign_key => "game9_id"
    belongs_to :game10, :class_name => "PlatformGame", :foreign_key => "game10_id"

    def games
        hot_game_list = PlatformGame.where(id:[self.game1_id,self.game2_id,self.game3_id,self.game4_id,self.game5_id,self.game6_id,self.game7_id,self.game8_id,self.game9_id,self.game10_id])

    end
end
  