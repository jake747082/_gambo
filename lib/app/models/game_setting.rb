class GameSetting < ActiveRecord::Base

  validate :valid_jackpot_range

  %w(sm md).each do |size|
    class_eval %Q{
      def jackpot_#{size}_range_diff
        jackpot_#{size}_range_end - jackpot_#{size}_range_begin
      end
      
      def jackpot_#{size}_range
        (jackpot_#{size}_range_begin..jackpot_#{size}_range_end)
      end

      def jackpot_#{size}_rand
        rand_jackpot = rand(jackpot_#{size}_range)
        rand_jackpot -= (rand_jackpot % jackpot_#{size}_interval)
      end
    }
  end

  def to_data
    {
      md_start: jackpot_md_range_begin, md_end: jackpot_md_range_end, md_interval: jackpot_md_interval,
      sm_start: jackpot_sm_range_begin, sm_end: jackpot_sm_range_end, sm_interval: jackpot_sm_interval
    }
  end

  def jackpot_range_changed?
    %w(range_begin range_end interval).each do |option|
      if self.send("jackpot_sm_#{option}_changed?") || self.send("jackpot_md_#{option}_changed?")
        return true
      end
    end
    false
  end

  private

  def valid_jackpot_range
    if jackpot_md_range_begin > jackpot_md_range_end
      errors.add(:jackpot_md_range_begin, I18n.t('shared.errors.less_than_or_equal_to', count: jackpot_md_range_end))
    elsif jackpot_md_interval > jackpot_md_range_diff
      errors.add(:jackpot_md_interval, I18n.t('shared.errors.less_than_or_equal_to', count: jackpot_md_range_diff))
    end

    if jackpot_sm_range_begin > jackpot_sm_range_end
      errors.add(:jackpot_sm_range_begin, I18n.t('shared.errors.less_than_or_equal_to', count: jackpot_sm_range_end))
    elsif jackpot_sm_interval > jackpot_sm_range_diff
      errors.add(:jackpot_sm_range_begin, I18n.t('shared.errors.less_than_or_equal_to', count: jackpot_sm_range_diff))
    end
  end
end