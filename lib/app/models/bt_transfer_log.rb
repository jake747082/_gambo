class BtTransferLog < ActiveRecord::Base
  belongs_to :user
  before_create :assign_order_id

  shared do
    as_enum :cash_type, deposit: 0, cashout: 1
  end

  def assign_order_id
    self.order_id = "#{Time.zone.now.strftime("%Y%m%d%H%M%S")}"
  end
end