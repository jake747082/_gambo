class Marquee < ActiveRecord::Base
  serialize :days, Array

  scope :enable_now,->{where("active = true And 
    (
      (by_time = false And eveny_week = false) Or 
      (by_time = true And eveny_week = false And produced_datetime <= :now And expire_datetime >= :now) Or 
      (eveny_week = true And by_time = false And begin_time <= :now And stop_time >= :now And days like :daynames ) Or
      (by_time = true And eveny_week = true And produced_datetime <= :now And expire_datetime >= :now  And begin_time <= :now And stop_time >= :now And days like :daynames )
    )",
    {now: Time.zone.now, daynames: "%"+Date::DAYNAMES[Date.today.wday]+"%"})}
  scope :language , -> (data) {
    where("language = ?", data);
  }
  def is_expired
    self.expire_datetime < Time.zone.now
  end

end