class AgentWithdraw < ActiveRecord::Base
  belongs_to :agent
  before_create :assign_order_id

  store :bank_account_info, accessors: [ :bank_id, :province, :subbranch, :city, :account, :security_code, :title, :mobile ], coder: JSON

  def trade_no
    @trade_no ||= "A#{id.to_s.rjust(8, '0')}"
  end

  shared do
    # 0: 代理買分, 1: 代理提款, 2: 退還給上層, 3: 扣款上層點數
    as_enum :cash_type, deposit: 0, cashout: 1, refund: 2, deduct: 3
  end

  default_scope { order(id: :desc)}

  scope :list_deposit, -> { where(cash_type_cd: 0) }
  scope :list_cashout, -> { where(cash_type_cd: 1) }

  scope :search, -> (query) {
    if query.present?
      joins(:agent).where("#{self.table_name}.id = ? OR agents.nickname LIKE ? OR agents.username LIKE ?", "#{query}", "%#{query}%", "%#{query}%")
    else
      all
    end
  }

  scope :withdraw_type, ->(type) {
    if type == 'cashout'
      where(cash_type_cd: 1)
    elsif type == 'deposit'
      where(cash_type_cd: 0)
    elsif type == 'refund'
      where(cash_type_cd: 2)
    elsif type == 'deduct'
      where(cash_type_cd: 3)
    end
  }

  def assign_order_id
    self.order_id = "A#{Time.zone.now.strftime("%Y%m%d%H%M%S")}"
  end

end
