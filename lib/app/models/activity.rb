PublicActivity::Activity.class_eval do
  default_scope { includes(:owner, :trackable).order(id: :desc) }
  default_scope { where(`"trackable"."id" IS NOT NULL`) }
  default_scope { where(`"owner"."id" IS NOT NULL`) }

  scope :date_range, -> (begin_date, end_date) {
    where('created_at >= ? AND created_at < ?', begin_date, end_date + 1.day)
  }

  # @refator
  scope :search, -> (search_params) {
    query = self.all
    if search_params[:owner].present?
      user_ids = User.where("username LIKE ?", "%#{search_params[:owner]}%").pluck(:id)
      agent_ids = Agent.where("username LIKE ?", "%#{search_params[:owner]}%").pluck(:id)
      admin_ids = Admin.where("username LIKE ?", "%#{search_params[:owner]}%").pluck(:id)
      query = query.where('(owner_type = ? AND owner_id IN (?) ) OR (owner_type = ? AND owner_id IN (?) ) OR (owner_type = ? AND owner_id IN (?) )', User, user_ids, Agent, agent_ids, Admin, admin_ids)
    end

    if search_params[:trackable].present?
      user_ids = User.where("username LIKE ?", "%#{search_params[:trackable]}%").pluck(:id)
      agent_ids = Agent.where("username LIKE ?", "%#{search_params[:trackable]}%").pluck(:id)
      admin_ids = Admin.where("username LIKE ?", "%#{search_params[:trackable]}%").pluck(:id)
      query = query.where('(trackable_type = ? AND trackable_id IN (?) ) OR (trackable_type = ? AND trackable_id IN (?) ) OR (trackable_type = ? AND trackable_id IN (?) )', User, user_ids, Agent, agent_ids, Admin, admin_ids)
    end

    query = query.where("activities.key IN (?)", ["admin.#{search_params[:action]}", "agent.#{search_params[:action]}", "user.#{search_params[:action]}"]) if search_params[:action].present?
    query = query.where('parameters LIKE ?', "%remote_ip: #{search_params[:ip]}%") if search_params[:ip].present?
    query = query.where(created_at: search_params[:date].to_date.beginning_of_day..search_params[:date].to_date.end_of_day) if search_params[:date].present?

    query
  }

  def trade_no
    @trade_no ||= id.to_s.rjust(8, '0')
  end
end
