class Currency < ActiveRecord::Base
  has_many :withdraws
  has_many :wallet
  has_many :cumulative_credits, class_name: :UserCumulativeCredit

  scope :in_enable, -> { where(in_enable: true) }
  scope :out_enable, -> { where(out_enable: true) }
end