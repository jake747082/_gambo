class New < ActiveRecord::Base
  default_scope { where(deleted: false).order(id: :desc) }

  # Scopes
  scope :admin_list, -> {
    where(admin_port: true)
  }

  scope :agent_list, -> {
    where(agent_port: true)
  }

  scope :www_list, -> {
    where(www_port: true)
  }

  scope :www_last_important, -> {
    where(important: true).where(www_port: true).limit(1)
  }
end
