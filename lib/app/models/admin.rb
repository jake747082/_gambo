class Admin < ActiveRecord::Base

  platform :shared do
    include DeviseAuthableHelper
    include Loggable

    validates :password, confirmation: true, on: :create
    validates :password_confirmation, presence: true, on: :create

    # Devise Settings
    devise :database_authenticatable, :trackable, :validatable, authentication_keys: [ :username]

    # Searchable & Devise
    self.auth_column = :username

    # Roles(0: 系統管理員, 1: 報表管理員, 2: 機率管理員, 3: 主管, 4: 網站管理員, 5: 客服)
    as_enum :role, super_manager: 0, report_manager: 1, game_manager: 2, main_manager: 3, site_manager: 4, custom_service: 5
    validates :role, presence: true

    # 是否有權限修改系統
    alias_method :can_use_system?, :super_manager?

    def self.enable_roles
      {
        super_manager: I18n.t("activerecord.enums.admin.roles.super_manager"),
        main_manager: I18n.t("activerecord.enums.admin.roles.main_manager"),
        site_manager: I18n.t("activerecord.enums.admin.roles.site_manager"),
        custom_service: I18n.t("activerecord.enums.admin.roles.custom_service"),
      }
    end

    # 給後端辨識用
    def system_id
      "[Admin] #{nickname}"
    end

    def title
      "#{username} (#{nickname})"
    end

    def human_role
      self.class.human_enum_name(:roles, role)
    end

    # Overwride Devise Method
    def active_for_authentication?
      super && !lock?
    end
  end
end
