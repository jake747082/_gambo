class Registry
  def self.method_missing(key, value=nil)
    key = key.to_s
    if key[-1] == "="
      key = key.tr('=', '')
      Thread.current["registry_#{key}"] = value
    else
      Thread.current["registry_#{key}"]
    end
  end
end