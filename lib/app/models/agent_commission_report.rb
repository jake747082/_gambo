class AgentCommissionReport < ActiveRecord::Base
  belongs_to :agent
  shared do
    as_enum :type, bt: 1, kgame: 2, administration: 99
  end

  scope :date_range, -> (begin_date, end_date) {
    begin_date = Time.zone.parse(begin_date.to_s)
    end_date   = Time.zone.parse(end_date.to_s)
    where('date >= ? AND date < ?', begin_date, end_date)
  }

end