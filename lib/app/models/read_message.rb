class ReadMessage < ActiveRecord::Base
  belongs_to :message
  belongs_to :user

  scope :unread, -> { where(read_at: nil) }
end
