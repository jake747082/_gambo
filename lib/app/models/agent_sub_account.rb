class AgentSubAccount < ActiveRecord::Base

  [ DeviseAuthableHelper, Loggable ].each { |m| include m }

  # Searchable & Devise
  self.auth_column = :username

  # Relations
  belongs_to :agent

  # Delegate
  delegate :title, to: :agent
  delegate :human_agent_level, to: :agent
  delegate :casino_ratio, to: :agent
  delegate :credit, to: :agent
  delegate :credit_max, to: :agent
  delegate :credit_left, to: :agent
  delegate :credit_undispatched, to: :agent
  delegate :in_final_level?, to: :agent
  delegate :in_top_level?, to: :agent
  delegate :is_points_type?, to: :agent
  delegate :reviewing?, to: :agent
  has_one :bank_account, as: :accountable

  # Scopes
  default_scope { includes(:agent) }

  shared do
    devise :database_authenticatable, :registerable, :trackable, authentication_keys: [ :username ]
  end

  # 是否為子帳號
  def sub_account?
    true
  end

  # 是否為主帳號
  def master_account?
    false
  end

  def sub_account_permission?
    false
  end

  def user_permission?
    true
  end

  def system_id
    "#{username} (#{nickname})"
  end

  def active_for_authentication?
    super && !reviewing?
  end

  def inactive_message
    reviewing? ? :agent_sub_account_reviewing : super
  end
end
