class UserGamePlatformship < ActiveRecord::Base
  belongs_to :user
  belongs_to :game_platform

  scope :mdragon, -> { find_or_create_by(game_platform_id: 1) }
  scope :east, -> { find_or_create_by(game_platform_id: 2) }
  scope :bbin, -> { find_or_create_by(game_platform_id: 3) }
  scope :ag, -> { find_or_create_by(game_platform_id: 4) }
  scope :mg, -> { find_or_create_by(game_platform_id: 5) }
  scope :cagayan, -> { find_or_create_by(game_platform_id: 6) }
  scope :bt, -> { find_or_create_by(game_platform_id: 7) }
  scope :kgame, -> { find_or_create_by(game_platform_id: 8) }
  scope :cq9, -> { find_or_create_by(game_platform_id: 9) }
  scope :sf, -> { find_or_create_by(game_platform_id: 10) }
  scope :bts, -> { find_or_create_by(game_platform_id: 11) }
  scope :ag_sw, -> { find_or_create_by(game_platform_id: 12) }
  scope :wm, -> { find_or_create_by(game_platform_id: 13) }
  scope :bng, -> { find_or_create_by(game_platform_id: 14) }

  before_create :set_password
  before_create :set_default_limit, if: :east?
  after_update :update_limit_to_east, if: :east?

  def set_password
    self.password = SecureRandom.hex(4)
  end

  def set_default_limit
    self.video_limit = 1
    self.roulette_limit = 1
    self.game_limit = '1,1,1,1,1,1,1,1,1,1,1,1,1,1'
  end

  def east?
    game_platform_id == 2
  end

  def update_limit_to_east
    game_platform = GamePlatforms::User.new('east', user)
    # limittype = 1
    game_platform.update_limit(video_limit, 1) if video_limit_changed?
    # limittype = 2
    game_platform.update_limit(roulette_limit, 2) if roulette_limit_changed?
    # limittype = 3
    game_platform.update_limit(game_limit, 3) if game_limit_changed?
  end

  # 亂數產生 session_id
  def generate_session_id
    session_id = loop do
      random_session_id = SecureRandom.uuid
      break random_session_id unless UserGamePlatformship.exists?(session_id: random_session_id)
    end
  end
end
