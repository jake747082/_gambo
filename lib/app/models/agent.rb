class Agent < ActiveRecord::Base
  require_relative 'concerns/agent/attributes'
  require_relative 'concerns/agent/helpers'
  require_relative 'concerns/agent/level_roles'

  [ DeviseAuthableHelper, Loggable, Attributes, Helpers, LevelRoles ].each { |m| include m }

  # slot machine tmp extra
  attr_accessor :extra

  # Searchable & Devise
  self.auth_column = :username

  # Relations
  has_many :users
  has_many :machines
  has_many :slot_bet_forms
  has_many :fishing_joy_bet_forms
  has_many :roulette_bet_forms
  has_many :poker_bet_forms
  has_many :little_mary_bet_forms
  has_many :third_party_bet_forms
  has_many :agent_withdraws
  has_many :withdraws
  has_many :agent_commission_logs

  has_many :accounts, class_name: :AgentSubAccount
  has_one :bank_account, as: :accountable
  has_many :messages
  has_many :read_messages

  accepts_nested_attributes_for :bank_account
  scope :list_apply, -> {list_agent.where(lock: true).where(reviewing: true)}

  shared do
    cattr_accessor :agree_terms
    devise :database_authenticatable, :registerable, :validatable, :trackable, authentication_keys: [ :username ]
    # 0: 現金; 1: 買分
    as_enum :type, cash: 0, point: 1
  end

  platform :shared do
    scope :avaliable_distributor, -> {
      list_agent.where(lock: false)
    }
  end

  platform :agent do
    default_scope { Registry.current_agent.self_and_descendants if Registry.current_agent.present? }

    scope :except_me, -> { where('id != ?', Registry.current_agent.id) if Registry.current_agent.present? }

    def current_agent?
      Registry.current_agent.id == id
    end

    def screenname
      if Registry.current_agent.id == id
        "#{nickname} (self)"
      else
        nickname
      end
    end
  end

  def increase_points!(points_to_incrument)
    increment!(:points, points_to_incrument.abs)
  end

  def use_points!(points)
    increment!(:points, -points.abs)
  end

  def have_shop?
    @is_shop ||= in_final_level? && Setting.shop_site?
  end

  def agent
    self
  end

  # 是否為子帳號
  def sub_account?
    false
  end

  # 是否為主帳號
  def master_account?
    true
  end

  # 是否有權限看財務
  def finances_permission?
    true
  end

  # 是否有權限控制機器
  def machine_read_permission?
    true
  end

  # 是否有機器控制權限
  def machine_write_permission?
    true
  end

  def agent_read_permission?
    true
  end

  def agent_write_permission?
    true
  end

  # 是否有權限管理帳號
  def sub_account_permission?
    true
  end

  # 是否有開啟串接API功能
  def open_api?
    secret_key.present?
  end

  # 亂數產生推薦碼
  def generate_recommend_code
    loop do
      random_recommend_code = SecureRandom.hex[0..4].upcase
      break random_recommend_code unless Agent.exists?(recommend_code: random_recommend_code)
    end
  end

  # 搜尋該Agent底下user的logs (用關鍵字搜尋username or nickname)
  def user_change_logs(query)
    users = self.users.where("username LIKE ? OR nickname LIKE ?", "%#{query}%", "%#{query}%")
    PublicActivity::Activity.where(trackable: users)
  end

  def active_for_authentication?
    super && !reviewing?
  end

  def inactive_message
    reviewing? ? :agent_reviewing : super
  end
end
