class User < ActiveRecord::Base
  require_relative 'concerns/user/attributes'
  require_relative 'concerns/user/helpers'

  default_scope { order(id: :desc)}
  default_scope { where(`"id" IS NOT NULL`) }

  [ DeviseAuthableHelper, Loggable, Attributes, Helpers ].each { |m| include m }

  belongs_to :agent
  belongs_to :user_group
  has_one :bank_account, as: :accountable
  has_many :withdraws
  has_many :bonuses
  has_many :little_mary_bonuses

  has_many :slot_bet_forms
  has_many :racing_bet_forms
  has_many :fishing_joy_bet_forms
  has_many :little_mary_bet_forms
  has_many :poker_bet_forms
  has_many :roulette_bet_forms
  has_many :third_party_bet_forms

  has_many :slot_histories, class_name: :SlotMachineHistoriesOfUser
  has_many :messages
  has_many :read_messages
  has_many :user_game_platformships
  has_many :user_gametokens
  has_many :game_platforms, through: :user_game_platformships
  has_many :user_wallets

  has_many :daily_logs, class_name: :UserDailyCreditLog
  has_many :cumulative_credits, class_name: :UserCumulativeCredit

  has_one :bng_version

  accepts_nested_attributes_for :user_game_platformships
  accepts_nested_attributes_for :bank_account

  # Searchable & Devise
  self.auth_column = :username

  shared do
    devise :database_authenticatable, :registerable, :validatable, :trackable, authentication_keys: [ :username ]
    scope :today, -> {
      where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_day, Time.zone.now.at_end_of_day)
    }

    scope :week, -> {
      where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_week.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_week.strftime("%Y-%m-%d 23:59:59"))
    }

    scope :month, -> {
      where('created_at >= ? AND created_at < ?', Time.zone.now.at_beginning_of_month.strftime("%Y-%m-%d 00:00:00"), Time.zone.now.at_end_of_month.strftime("%Y-%m-%d  23:59:59"))
    }

    scope :datetime_range, -> (begin_datetime, end_datetime) {
      begin_datetime = Time.zone.parse(begin_datetime.to_s)
      end_datetime   = Time.zone.parse(end_datetime.to_s)
      where('created_at >= ? AND created_at < ?', begin_datetime, end_datetime)
    }
  end

  platform :www do

    # 手機驗證碼
    # before_create :assign_verification_code

    attr_accessor :recommend_code
    # 暱稱
    # validates :nickname, :username, presence: true

    # 帳號
    validates :username, uniqueness: { message: :unique }
    # validates :email, uniqueness: { message: :unique }
    validates :username, presence: true, length: { in: 6..128 }, format: { with: /\A[a-zA-Z0-9._]+\z/ }

    # 密碼
    validates :password, confirmation: { message: :confirmation }, on: :create
    validates :password, presence: true, length: { in: 8..128 }, on: :create

    validates :recommend_code, length: { is: 5 }, format: { with: /\A[a-zA-Z0-9]+\z/ }, allow_blank: true, on: :create
    validate :valid_recommend_code, on: :create
    validates :agree_terms, acceptance: true, presence: true, on: :create

    validates_uniqueness_of :mobile, on: :create
    # validates :mobile, phone: true

  end

  platform :agent do
    default_scope { Registry.current_agent.all_users if Registry.current_agent.present? }
  end


  def valid_recommend_code
    if recommend_code.present? && !Agent.avaliable_distributor.exists?(recommend_code: recommend_code)
      errors.add(:recommend_code, I18n.t('activerecord.errors.recommend_code.invalid'))
    end
  end

  def use_credit!(amount)
    lock!
    # if amount == credit_left
      # self.class.update(id, {credit: 0, credit_used: 0})
    # else
    self.class.update_counters(id, credit_used: amount)
    # end
  end

  def increase_credit(credit_to_incrument)
    assign_attributes(credit_used: credit_used - credit_to_incrument)
  end

  def increase_credit!(credit_to_incrument)
    lock!
    increment!(:credit, credit_to_incrument.abs)
  end

  # 累積打碼量
  def bet_credit_sum
    # bet_credit_sum = if cashout_limit > 0
    #   slot_bet_forms.where("created_at >= ?", cashout_limit_at).sum(:bet_total_credit).to_f + racing_bet_forms.where("created_at >= ?", cashout_limit_at).sum(:bet_total_credit).to_f
    # else
    #   0
    # end
    # bet_credit_sum.round(4)
    0
  end

  def shareholder
    agent.root
  end

  def director
    agent.parent
  end

  def active_for_authentication?
    super && !lock?
    # super && !lock? && is_verified?
  end

  def assign_game_token
    self.assign_attributes(game_token: SecureRandom.hex(10))
    self.game_token
  end

  def assign_verification_code
    if self.verification_send_at.nil? || DateTime.now > self.verification_send_at + 5.minutes
      self.verification_code = SecureRandom.hex(3).upcase
      self.verification_send_at = Time.zone.now
      # 發送手機簡訊
      # @todo guid
      content = "欢迎！您的验证码为#{self.verification_code}，请及时完成注册程序。"
      # Sms::Mitake.new.send(self, content, Time.zone.now.strftime("%Y%m%d%H%M%S"))
      Sms::Fivec.new.send(self, content)
    end
  end

  def lobby_id
    Setting.beginning_lobby_id.to_i + id
  end

  def self.lobby_id_to_id(lobby_id)
    lobby_id.to_i - Setting.beginning_lobby_id.to_i
  end

  def self.id_to_lobby_id(id)
    id.to_i + Setting.beginning_lobby_id.to_i
  end

  # 亂數產生 auth_token
  def self.generate_auth_token
    auth_token = loop do
      random_auth_token = SecureRandom.hex(64)
      break random_auth_token unless User.exists?(auth_token: random_auth_token)
    end
  end

  def get_bng_version
    if bng_version.nil?
      create_bng_version
      bng_version.version
    else
      bng_version.version
    end
  end

  scope :search_user, ->(key, query, begin_date ,end_date) {
    if key || query || begin_date || end_date
      list = []
      if query!=''
        case key
        when 'id'
          list.push("(`id` = #{lobby_id_to_id(query)})") if lobby_id_to_id(query) > 0
        when 'username'
          list.push("(`username` LIKE '%#{query}%')")
        when 'nickname'
          list.push("(`nickname` LIKE '%#{query}%')")
        when 'mobile'
          list.push("(`mobile` LIKE '%#{query}%')")
        when 'ip'
          list.push("(`current_sign_in_ip` LIKE '%#{query}%' OR `last_sign_in_ip` LIKE '%#{query}%')" )
        when 'group'
          list.push("(`user_group_id` = #{query})")
        end
      end
      list.push("`created_at` >= '#{begin_date}'") if begin_date!=''
      list.push("`created_at` < '#{end_date}'") if end_date!=''
      where(list.join(" AND "))
    else
      all
    end
  }

  scope :search_users, ->(user_ids){
    where("id IN (?)", user_ids)
  }
end
