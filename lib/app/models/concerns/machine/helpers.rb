module Machine::Helpers
  extend ActiveSupport::Concern

  # 是否屬於 agent (跨層級)
  def is_belongs_to?(agent)
    agent_id == agent.id || send(agent.agent_level.to_s.foreign_key) == agent.id
  end

  # 所有機器 deposit , cashout log
  def cash_trade_logs
    change_logs.where(key: ['machine.cashout', 'machine.deposit'])
  end

  def operation_logs
    # 所有機器 Log (除了 admin 以外)
    return Registry.current_agent.present? ? all_logs.where.not(owner_type: 'Admin') : all_logs
  end
end