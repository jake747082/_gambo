module Agent::Helpers
  extend ActiveSupport::Concern

  # 是否屬於 parent agent (跨層級)
  def is_belongs_to?(agent)
    self_and_ancestors.ids.include?(agent.id)
  end

  # 找出所有使用者
  def all_users
    ::User.where(agent_level_foreign_key_cond)
  end

  # 找出所有機器
  def all_machines
    ::Machine.where(agent_level_foreign_key_cond)
  end

  def all_slot_bet_forms
    ::SlotBetForm.where(agent_level_foreign_key_cond)
  end

  def all_racing_bet_forms
    ::RacingBetForm.where(agent_level_foreign_key_cond)
  end

  def all_little_mary_bet_forms
    ::LittleMaryBetForm.where(agent_level_foreign_key_cond)
  end

  def all_poker_bet_forms
    ::PokerBetForm.where(agent_level_foreign_key_cond)
  end

  def all_fishing_joy_bet_forms
    ::FishingJoyBetForm.where(agent_level_foreign_key_cond)
  end

  def all_roulette_bet_forms
    ::RouletteBetForm.where(agent_level_foreign_key_cond)
  end

  def all_third_party_bet_forms
    ::ThirdPartyBetForm.where(agent_level_foreign_key_cond)
  end

  def all_withdraws
    ::Withdraw.where(agent_level_foreign_key_cond)
  end

  def all_action_logs
    PublicActivity::Activity.where(owner: all_account_ids)
  end

  def all_change_logs
    PublicActivity::Activity.where(trackable: all_account_ids)
  end

  # 是否開啟 maintain mode
  def maintain_open?
    maintain_code.present?
  end

  # 啟用 maintain 模式
  def generate_maintain_code
    if maintain_code.blank?
      chr = (0..9).to_a + ('A'..'Z').to_a
      password = (0..5).map { chr.sample }.join
      update(maintain_code: password)
    end
    maintain_code
  end

  # 亂數產生Account ID
  def generate_account_id
    account_id = loop do
      random_account_id = (SecureRandom.random_number * 1000000000000000).to_i
      break random_account_id unless Agent.exists?(account_id: random_account_id)
    end
  end

  # 亂數產生Secret Key
  def generate_secret_key
    secret_key = loop do
      random_secret_key = SecureRandom.hex
      break random_secret_key unless Agent.exists?(secret_key: random_secret_key)
    end
  end

  private

  def all_account_ids
    @all_account_ids ||= accounts.ids.push(id)
  end

  def agent_level_foreign_key_cond
    { agent_level.to_s.foreign_key => id }
  end
end
