module User::Helpers
  extend ActiveSupport::Concern

  # 是否屬於 agent (跨層級)
  def is_belongs_to?(agent)
    agent_id == agent.id || send(agent.agent_level.to_s.foreign_key) == agent.id
  end

  # 亂數產生 game_token
  def generate_game_token
    game_token = loop do
      random_game_token = SecureRandom.hex(10)
      break random_game_token unless User.exists?(game_token: random_game_token)
    end
  end
end