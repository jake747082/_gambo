namespace :gambo do

  task :build => :environment do
    # (股東)
    shareholder = Agent.new(credit_max: 0)
    AgentForm::Create.new(shareholder).create(username: 'agent1', nickname: '股東', password: '88888888', password_confirmation: '88888888', credit_max: 1000000, casino_ratio: 100)

    # # (總代)
    director = shareholder.children.new(credit_max: 0)
    AgentForm::Create.new(director).create(username: 'agent2', nickname: '總代', password: '88888888', password_confirmation: '88888888', credit_max: 1000000, casino_ratio: 100)

    # # (代理)
    agent = director.children.new(credit_max: 0)
    AgentForm::Create.new(agent).create(username: 'agent3', nickname: '代理', password: '88888888', password_confirmation: '88888888', credit_max: 1000000, casino_ratio: 100)

    # # 買分
    # # (股東)
    # shareholder = Agent.new(credit_max: 0)
    # AgentForm::Create.new(shareholder).create(username: 'point_agent1', nickname: '股東(買分)', type_cd: 1, password: '888888', password_confirmation: '888888', credit_max: 1000000, casino_ratio: 100)

    # # (總代)
    # director = shareholder.children.new(credit_max: 0)
    # AgentForm::Create.new(director).create(username: 'point_agent2', nickname: '總代(買分)', type_cd: 1, password: '888888', password_confirmation: '888888', credit_max: 80000, casino_ratio: 100)

    # # (代理)
    # agent = director.children.new(credit_max: 0)
    # AgentForm::Create.new(agent).create(username: 'point_agent3', nickname: '代理(買分)', type_cd: 1, password: '888888', password_confirmation: '888888', credit_max: 20000, casino_ratio: 100)

    # 管理員帳號
    Admin.create!(username: 'admin', nickname: 'admin', password: '88888888', password_confirmation: '88888888', role: :super_manager)

    # GamePlatform
    game_platforms = %w(mdragon east bbin ag mg cagayan bt kgame cq9 sf bts ag_sw wm)
    game_platforms.each { |game_platform| GamePlatform.create!(name: game_platform) }
    GamePlatform.find_by_name('sf').update!(is_single_wallet: true)
    GamePlatform.find_by_name('bt').update!(is_single_wallet: true)
    GamePlatform.find_by_name('ag_sw').update!(is_single_wallet: true)
    GamePlatform.find_by_name('wm').update!(is_single_wallet: true)

    # sf_platform_games = %w(WarriorEmperor CashanovaCashpots SushiSlots PoppinPizzaPrizes CheeseChase DoubleWinAgent SnakesAndLadders KrispyKash CasinoCrops SuperBigWilds SideshowSlots GoldenMileSlots HeavensWealth JugglingJokers)
    # sf_platform_games.each { |game_code| 
    #   GamePlatform.find_by_name('sf').platform_games.create(code: game_code, game_type: 'slot', tech: 'html5', plat: 'web', lang: '["en","zh-tw"]', status: 1, nameset: '{"en":"'+game_code+'"}')
    # }
    # Site Config
    SiteConfig.create!

    Currency.create(name: 'ETH', blockchain: 'ethereum', token_type: 'ETH', token_coin: 'ETH', in_rate: 1137, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'BTC', blockchain: 'bitcoin', token_type: 'BTC', token_coin: 'BTC', in_rate: 59285, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'USDT_ERC20', blockchain: 'USDT', token_type: 'ETH', token_coin: 'USDT', in_rate: 7, in_limit: 0.1, out_limit: 1, in_enable: true, out_enable: true)
    Currency.create(name: 'USDT_OMNI', blockchain: 'omni', token_type: 'BTC', token_coin: 'USDT', in_rate: 7, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'DET', blockchain: 'DET', token_type: 'ETH', token_coin: 'DET', in_rate: 0.18, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'IBC', blockchain: 'IBC', token_type: 'ETH', token_coin: 'IBC', in_rate: 0.1, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'UNI', blockchain: 'UNI', token_type: 'ETH', token_coin: 'UNI', in_rate: 1, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)
    Currency.create(name: 'kVNDT', blockchain: 'kVNDT', token_type: 'ETH', token_coin: 'kVNDT', in_rate: 0.2, in_limit: 0.1, out_limit: 0, in_enable: true, out_enable: false)

    # Player
    # player = User.create(nickname: 'test001', username: 'test001', password: 'passw0rd', auth_type_cd: 0, password_confirmation: 'passw0rd', agent_id: 3, director_id: 2, shareholder_id: 1, credit: 1000, mobile: '886912345678')

    # Page.create(page_id: :about, title: '关于BT', content: '<p>BT娱乐城重视客户需求、不断地提出创新构想，让BT成为爱用者的最佳博弈娱乐平台，也提供现金经营者最佳的经营合作平台。</p><p>我们知道使用者希望体验多变有趣的产品内容，也知道经营商 希望拥有方便高效率的经营平台的服务。</p><p>透过经营团队提供创新的经营平台让经营伙伴与爱用者零距离接触提供最佳服务与互动，让爱用者有最好最刺激得娱乐体验。</p><p>BT不断创新及研发，累积了丰富的经验与技术，提供最完整最多的产品服务，相信更能满足经营者与爱用者多变需求。</p>')
    # Page.create(page_id: :qa, title: '常见问题', content: '<h4>加入会员年龄要求？</h4><p>加入BT娱乐城，您需要同意的条款及条件，并且至少要在18岁或以上。大多数国家于于法律上对于线上游戏都有订定相关的详细规范，请您务必详细阅读并遵守您所在当地的法律条文。</p><h4>我的个人资料安全吗？</h4><p>在这裡我们可确保您的个人资料是安全无虞的，BT娱乐城坚持使用最高等级安全措施及最安全的通讯协定，并存储在最安全的作业环境，并且BT娱乐城永远不会向任何第三方人员透露您的各项个人资料，敬请放心。</p><h4>BT娱乐城最低投注额是多少？</h4><p>最低投注额各个项目有所不同，当您打开每一项游戏时，游戏画面会显示该项游戏最低投注额。</p>')
    # Page.create(page_id: :duty, title: '责任博彩', content: '<h4>概况：</h4><p>对于大多数人来说，赌博是一个娱乐休闲和具娱乐性的活动，然而，对于另一些人来说，博弈有负面影响。</p><p>我们对“小赌怡情、适可而止"的宗旨非常重视。</p><p>我们希望我们的顾客在投注时得到娱乐，但也希望赌博不会影响到他们的财政状况和生活。</p><p>我们建议您:</p><p>1.把博彩当做一种娱乐休闲项目</p><p>2.避免连续的损失</p><p>3.对博彩有自己的认识</p><p>4.合理安排自己在博彩中的时间和精力</p>')
    # Page.create(page_id: :privacy_policy, title: '隐私政策', content: '<p>一、我们致力为客户保护隐私并提供一个最安全的游戏平台，我们在此网站搜集的资料将会为您提供最卓越的服务，</p><p>我们不会出卖或租赁您的个人资料给第三方，客户所提供的个人资料均经过SSL128加密技术处理，</p><p>并储存在安全的、非公开的作业系统，对于有机会接触客户的个人资料的协助伙伴也必需遵守我们订立的隐私保密规则。</p><p>二、您需要保管好您的帐号和密码安全。不得允许任何其他人或协力厂商，</p><p>包括任何未成年人使用或重复使用您的帐户进行任何网站访问及投注操作。</p><p>本公司将有权拒绝支付由此产生的任何奖励或奖金。您将独自为协力厂商在帐户中产生的所有损失负责。</p><p>三、您可能会收到一些定期的电邮，向您更新有关本公司产品的资讯与服务，当本网推出新颖，</p><p>好玩和有趣的推广活动，这些电邮就可帮助您充分享受到本网的服务。</p><p>四、我们的系统会自动记录浏览者的网路IP位址，但我们决不会记录网站访问者的电邮位址，而此项主要是用作网站流量统计之用。</p><p>五、本公司保留权力可自行对隐私政策做出任何修改，此政策如有任何更改均具有约束力，并立即生效。</p>')
    # Page.create(page_id: :terms, title: '条款与规则', content: '<p>一、遵守会员规范及法律规定：</p><p>您了解您注册成为会员后，即可使用本公司所提供之各项服务(以下称本服务)。当会员使用本服务时，即表示除了同意遵守本服务条款外，还同意接受本公司对会员规范(包括各项游戏规则、球赛投注规则、公告及注意事项等)及相关法令规定之拘束，本公司所有产品与客户服务、软件系统、网络架构等相关业务事宜，皆由合法认证机构授权和监管，使用本服务之会员请遵守使用者当地法令之许可，如有违反之情事恕非本服务之负责范围。</p><p>二、服务简介：</p><p>(一)本公司旨在创造一个『安全可靠』、『即时便利』、『公平公正』、『专业营运』的优质娱乐服务平台，强调的是让会员不受时空的限制，24小时随时上线就可以参与一个公平公正的游戏，亦可以享受到与世界各地玩家切磋的乐趣，我们秉持以客为尊的态度，不断开发创新及了解客户需求是本公司引以为傲的经营理念，冀望能创造出崭新的娱乐价值以及达到多方普及的目的，成为具有领导指标性的娱乐网站。</p><p>(二)虚拟货币银行服务：本公司提供网路金流付费平台，你所购买的点数将可享受符合本公司平台的服务。若您使用虚伪不正之方式进行「储值」，本公司也将保留随时终止您会员资格及使用各项服务之权利。</p><p>三、真实登录义务：</p><p>基于本公司所提供之各项服务，您同意于注册时依注册申请程序所提示之项目，登录您本人正确、真实及完整之个人资料；</p><p>当您的个人资料有异动时，请立即更新，以维持您个人资料之正确、真实及完整。如因您登录不实资料或冒用他人名义以致于侵害他人之权利或违法时，应自负法律责任；并同意您所提供之个人资料不实或个人资料有异动但没有更新以致于与原登录之资料不符时，本公司有权随时终止您的会员资格及使用各项会员服务之权利。</p><p>四、服务之停止与更改：</p><p>1.会员登录之资料不实。2.使用他人的名义申请本服务。3.违反游戏公平原则。4.参与游戏或比赛时，故意钻研系统漏洞或利用游戏弱点以影响结果。5.违反本服务条款时。6.本公司不允许不公平的下注方式、双边下注、无风险下注及任何无风险解冻之下注方式，如有发现上述问题之玩家，本公司有终止玩家帐号使用之权力。如有任何争议问题，本公司有最终决定权。无论任何情形，就停止或更改服务或终止会员帐户服务所可能产生之困扰、不便或损害，本服务对任何会员或第三人均不负任何责任。</p><p>五、服务暂停或中断：</p><p>(一)本公司于下列情形之一时，得暂停或中断本服务之全部或一部，对于使用者不负担任何赔偿责任：</p><p>1.对于本服务相关系统设备进行迁移、更换或维护时。2.因不可归责于本公司所造成服务停止或中断。3.因不可抗力所造成服务停止或中断</p><p>(二)如因本公司对于本网站相关系统设备进行迁移、更换或维护而必须暂停或中断全部或一部之服务时，本公司于暂停或中断前将以电子邮件通知或于本网站上公告。</p><p>(三)对于本服务之暂停或中断，可能造成您使用上的不便、资料遗失或其他经济及时间上之损失，您平时应采取适当的防护措施，以保障您的权益。</p>')
    # Page.create(page_id: :statement, title: '声明', content: '<p>一、保管义务：</p><p>会员有义务妥善保管在本网站之帐号与密码，并为此组帐号与密码登入系统后所进行之一切活动负责。为维护会员自身权益，请勿将帐号与密码泄露或提供予第三人知悉，或出借或转让他人使用。</p><p>二、同意各项球赛及游戏规则：</p><p>为避免使用者于本平台投注球赛或游戏时产生争议，各项规则于游戏中心和球赛中心均有详细说明，请务必详细阅读本服务所定之各项规则，会员一经开始使用本服务，即被视为已接受所有之规定。</p><p>三、会员规范之增订及修改：</p><p>本服务条款如有增订或修改，您同意自该修订条款于本网站公告之时起受其拘束，本网站将不对会员个别通知。如您于公告后继续使用本服务，则视为您已经同意该修订条款。</p><p>四、隐私权声明：</p><p>我们致力为客户保护隐私并提供一个最安全的游戏平台，我们在此网站搜集的资料将会为您提供最卓越的服务，我们不会出卖或租赁您的个人资料给第三方，客户所提供的个人资料均经过SSL128加密技术处理，并储存在安全的、非公开的作业系统，对于有机会接触客户的个人资料的协助伙伴也必需遵守我们订立的隐私保密规则。</p><p>五、免责声明：</p><p>部份地区或国家法律尚未明定线上博彩的合法性问题，甚至某些地区或国家已明确规范线上博彩为非法行为。我们无意邀请任何人在这些地区或国家非法使用本服务平台。使用者必需确定在您所居住的地区或国家使用线上博彩是否合法，并负完全的责任，使用本服务之会员请遵守使用者当地法令之许可，如有违反之情事恕非本公司之负责范围。</p>')
    # Page.create(page_id: :deposit, title: '存款提醒', content: '<p>1.注册成为会员</p><p>2.绑定银行卡讯息，并联系在线客服申请存款</p><p>3.成功存款后，您的账户内会添加您所存的金额的等值点数。</p><p>4.您可以直接选择电子馆进行老虎机或是捕鱼机游戏，</p><p>   也可将点数转移至您喜爱的真人娱乐后进行游戏。<a href="http://www.mdb88.net/transferl" target="_blank">如何转移点数</a></p><p>5.尽情游戏，赢取获利！</p><p>如需要更多协助，请与我门在线客服联络</p>')
    # Page.create(page_id: :withdrawals_application, title: '提款提醒', content: '<p>取款</p><p>提款处理时间取决于您选择的支付方式和VIP等级</p><p>處理時間通常的為幾個小時。如果您是高级别玩家，您的提款处理时间也会更快。</p><p>请注意，如果您使用信用卡和电子钱包进行存取款，其账户信息需要和您在钱龙娱乐的注册信息相符。第三方交易是不允许的。</p><p>提款请求将显示"处理中"直到娱乐场处理完成您的提款。在此期間您仍可以撤銷取款請求。如果您选择电汇的方式进行提款，您可能会被要求提供必要的银行信息。</p><p>请注意电汇产生的所有费用都由娱乐场支付，但是可能有一些费用会超过我们的控制范围。这些费用可能是基于您本地银行的政策。这些费用可能会影响到您的提款金额。</p><p>请注意，不正常的存取款将会使您的帐户处於监管状态下，此状态每笔存取款将酌收5-10%的金流服务费。</p>')
    # Page.create(page_id: :bank_information, title: '银行帐户申请变更流程', content: '<p>为了保证您的帐户资金安全，您的收款银行帐户姓名必须与您注册时的真钱帐号姓名一致，方可办理取款。</p><p>若是您日后需要修改取款银行信息，您可以联系24小时在线客服为您协助，核实信息后将为您处理。</p><p>*取款温馨提示</p><p>您存款后需要全额投注方可申请提款；</p><p>如享受了其他优惠，则需要达到相应有效投注额方可申请提款；</p><p>所有的提款彩金部门会进行审核，对未达到提款要求的，将会取消您的提款，并发送站内信息通知您。</p>')
  end
end
