class GamePlatforms::Games

  attr_accessor :config

  def initialize(game_platform_name)
    if game_platform_name == 'kgame' || game_platform_name == 'cq9'
      self.config = "GamePlatforms::#{game_platform_name.camelize}".constantize.new().config
    else
      self.config = "GamePlatforms::#{game_platform_name.camelize}".constantize.config
    end
  end

  def slot_games(category_id = 0)
    games = {}
    if category_id == 0
      config['slots']['games'].each do |category_id, game_config|
        games[category_id] ||= {}
        game_config.each do |game_name, game_id|
          games[category_id][game_name] = game_id || game_name
        end
      end
      games
    else
      config['slots']['games'][category_id]
    end
  end

  def slot_games_class_names
    games = {}
    config['slots']['games'].each do |category_id, game_config|
      game_config.each do |game_name, game_id|
        games[game_id || game_name] ||= {}
        games[game_id || game_name][:class_names] ||= []
        games[game_id || game_name][:class_names] << "c-#{category_id}"
        games[game_id || game_name][:name] = game_name
      end
    end
    games
  end

  def slot_categories
    config['slots']['category']
  end

  # 各平台遊戲類別
  def self.categories(game_platform_name = nil)
    categories = {
      'all' => {
        'video' => 1,
        'slot' => 2,
        'fishing' => 3,
        'table' => 6,
      },
      'ag' => {
        'live' => 1,
        'slot' => 2,
        'fishing' => 3
      },
      'ag_sw' => {
        'video' => 1
      },
      'mg' => {
        'slot' => 2,
      },
      'bbin' => {
        # 'live' => 3,
        # 'slot' => 5,
        # 'sport' => 1,
        # 'lottery' => 12
        'live' => 1,
        'slot' => 2,
        'sport' => 7,
        'lottery' => 8
      },
      'east' => {
        'live' => 1
      },
      'cagayan' => {
        'live' => 1
      },
      'bt' => {
        'slot' => 2,
        'fishing' => 3,
        'table' => 6,
        # 'roulette' => 3
      },
      'kgame' => {
        'chess' => 6,
        'slot' => 2
      },
      'cq9' => {
        'slot' => 2
      },
      'sf' => {
        'slot' => 2
      },
      'bts' => {
        'table' => 6
      },
      'wm' => {
        'video' => 1
      },
      'bng' => {
        'slot' => 2,
        'table' => 6
      }
    }
    game_platform_name.nil? ? categories : categories[game_platform_name]
  end

  def self.update_games_list
    games_list = []
    bt = GamePlatforms::Bt.new
    btid = GamePlatform.find_by(name: "bt")
    # btsid = GamePlatform.find_by(name: "bts")
    bngid = GamePlatform.find_by(name: 'bng')
    if bt.games_list
      bt.games_list.keys.each do |key|
        gamedatas = bt.games_list[key]
        gamedatas.each do |gamedata|
          nameset = {"zh-cn"=> gamedata["cn"],"zh-tw"=> gamedata["tw"],"en"=> gamedata["en"]}
          nameset.merge!({'vi-vn'=> gamedata['vi']}) if gamedata.include?('vi')
          game_params = {code: gamedata["game_code"],game_type: key,game_platform_id: btid.id,maintain: gamedata["maintain"],nameset: nameset.to_json}
          game = PlatformGame.new(game_params)
          games_list.push(game)
        end
      end
    end
    # @bts = GamePlatforms::Bts.new()
    # if @bts.games_list
    #   @bts.games_list.each do |gamedata|
    #     nameset = ActiveSupport::JSON.encode({"zh-cn"=> gamedata["GameName"]})
    #     game_params = {code: gamedata["GameID"],game_type: "table",game_platform_id: btsid.id,nameset: nameset.to_s}
    #     game = PlatformGame.new(game_params)
    #     games_list.push(game)
    #   end
    # end
    bng = GamePlatforms::Bng.new()
    if bng.games_list
      bng.games_list.each do |gamedata|
        name = gamedata["i18n"]
        nameset = { "zh-cn"=> ActiveSupport::JSON.encode(name["zh"]['title']),
                    "zh-tw"=> ActiveSupport::JSON.encode(name["zh-hant"]['title']),
                    "en"=> name["en"]['title']
                  }
        game_params = { code: 'BNG_' + gamedata["game_id"],
                        game_type: gamedata["type"].downcase,
                        game_platform_id: bngid.id,
                        nameset: nameset.to_json
                      }
        game = PlatformGame.new(game_params)
        games_list.push(game)
      end
    end
    PlatformGame.import(games_list, on_duplicate_key_update: [:maintain, :nameset])
  end
end