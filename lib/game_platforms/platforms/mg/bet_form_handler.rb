module GamePlatforms::Mg::BetFormHandler

  GAME_PLATFORM_ID = 5

  def bet_form_format(data)
    if data.present?
      bet_forms = data.map do |bet_form|
        user = User.find_by_username(bet_form['AccountNumber'][Setting.game_platform.mg.prefix.size..-1])
        if user.present? && !ThirdPartyBetForm.exists?(transaction_id: bet_form['TransactionId'])
          default_bet_form = default_bet_form_params(user, bet_form)
          # 遞增遊玩總輸贏
          # ＠todo better
          User.update_counters(user, play_total: default_bet_form[:user_credit_diff])
          # 計算代理分成
          final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
          ThirdPartyBetForm.new(final_params)
        end
      end.compact
    end
  end

  def default_bet_form_params(user, bet_form)
    bet_total_credit = (bet_form['TotalWager'].to_i / 100).to_f.round(2)
    reward_amount = (bet_form['TotalPayout'].to_i / 100).to_f.round(2)
    {
      user_id: user.id,
      game_platform_id: GAME_PLATFORM_ID,
      bet_total_credit: bet_total_credit,
      reward_amount: reward_amount,
      user_credit_diff: (reward_amount - bet_total_credit).round(2),

      row_id: "mg_#{bet_form['RowId']}",
      game_id: GamePlatforms::Mg.games[bet_form['ModuleId']][bet_form['ClientId']],
      category_id: bet_form['DisplayGameCategory'],
      betting_at: bet_form['GameEndTime'].to_datetime + 8.hours,
      transaction_id: bet_form['TransactionId'],
      extra: {
        session_id: bet_form['SessionId'],
        progressive_wage: bet_form['ProgressiveWage'],
        iso_code: bet_form['ISOCode'],
        game_platform: bet_form['GamePlatform'],
      }.to_json,
    }

  end
end
