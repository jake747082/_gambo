class GamePlatforms::FreeGame < GamePlatforms::Base::Interface
  def initialize(params)
    @code = params["game_code"]
    @lang = params["lang"].downcase
  end

  def transfer_free_game
    # 目前只有 BT 和 BTS 兩家有提供免費試玩，若還有其他家再依情況增補
    case @code.split('_')[0]
    when 'BTS'
      # 棋牌免費試玩
      Setting.game_platform.bts.free_url
    else
      @game_code = @code.split('_')[-1]
      data = PlatformGame.select(:game_type, :nameset).find_by(code: @game_code)
      url = get_bt_free_game_url(data.game_type, data.nameset) if data.game_type
      url.nil? ? '' : url
    end
  end

  private
  def get_bt_free_game_url(game_type, nameset)
    case game_type
    when 'slot'
      host = Setting.game_platform.bt.slot_free
      path = "/h5/#{@game_code}?token=guest&lang=#{@lang}"
      host + path
    when 'fish'
      keyword = @code.split('VG')[-1]
      host = Setting.game_platform.bt.btg_free
      path = if keyword == 'SEAFOOD2'
              "/fish/?token=guest&lang=#{@lang}"
             else
              "/#{keyword.downcase}/?token=guest&lang=#{@lang}"
             end
      host + path
    when 'table'
      keyword = JSON.parse(nameset)['en']
      host = Setting.game_platform.bt.btg_free
      path = "/#{keyword.downcase}/?token=guest&lang=#{@lang}"
      host + path
    end
  end
end