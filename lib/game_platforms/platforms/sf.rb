class GamePlatforms::Sf < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  attr_reader :front_url, :front_demo_url, :back_url, :licensee_name, :lobby_url
  attr_accessor :gamecode, :lang, :demo_mode

  autoload :BetFormHandler, 'game_platforms/platforms/sf/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  def initialize(user = nil)
    @front_url      ||= Setting.game_platform.sf.front_url
    @front_demo_url ||= Setting.game_platform.sf.front_demo_url
    @back_url       ||= Setting.game_platform.sf.back_url
    @licensee_name  ||= Setting.game_platform.sf.licensee_name
    @lobby_url      ||= Setting.game_platform.sf.lobby_url
    @user = user
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'sf/config.yml')
    YAML::load(File.open(abs_path))
  end

  def check_and_create_account
    true
  end

  def get_balance
    @user.present? ? @user.credit_left : 0
  end

  def transfer_credit(credit, type)
    false
  end

  def transfer_game
    @gamecode ||= "WarriorEmperor"
    @lang ||= "zh-hans"
    @demo_mode ||= false
    if @demo_mode
      "#{front_demo_url}/launch/?gn=#{@gamecode}&lb=#{@lobby_url}&lc=#{@lang}&fr=1"
    else
      return false unless @user.present?
      account_id = @user.username
      platform_game = PlatformGame.find_by_code(@gamecode)
      user_gametoken = @user.user_gametokens.find_or_create_by(game_platform_id: platform_game.game_platform_id, platform_game_id: platform_game.id)
      auth_token = user_gametoken.token
      "#{@front_url}/launch/?gn=#{@gamecode}&ln=#{@licensee_name}&ad=#{account_id}&at=#{auth_token}&lc=#{@lang}&lb=#{@lobby_url}"
    end
  end

  def betting_records
    {}
  end

  def create_bet_form(data)
    bet_forms_format(data)
  end

  # ex: 
  # UTC+0
  # From: 2017-06-20 15:56:18
  # To: 2017-06-21 15:56:18
  def playreport(from_time, to_time)
    @gamecode ||= "WarriorEmperor"
    params = {
      ReportType: 'PlayerReport',
      AccountID: @user.username,
      GameName: @gamecode,
      LicenseeName: @licensee_name,
      From: from_time,
      To: to_time,
      Timestamp: (Time.now.to_f.round(3)*1000).to_i.to_s
    }
    response = post_request('/playreport', params)
  end

  # ex: 
  # UTC+0
  # From: 2017-06-20 15:56:18
  # To: 2017-06-21 15:56:18
  def play_report(from_time, to_time)
    @gamecode ||= "WarriorEmperor"
    params = {
      ReportType: 'PlayerReport',
      AccountID: @user.username,
      LicenseeName: @licensee_name,
      From: from_time,
      To: to_time,
      Timestamp: (Time.now.to_f.round(3)*1000).to_i.to_s
    }
    response = post_request('/playreport', params)
    response
  end

  # ex: 
  # UTC+0
  # From: 2017-06-20 15:56:18
  # To: 2017-06-21 15:56:18
  def game_report(from_time, to_time)
    @gamecode ||= "WarriorEmperor"
    params = {
      ReportType: 'GameReport',
      GameName: @gamecode,
      LicenseeName: @licensee_name,
      From: from_time,
      To: to_time,
      Timestamp: (Time.now.to_f.round(3)*1000).to_i.to_s
    }
    response = post_request('/playreport', params)
    response
  end

  def round_report(round_id)
    params = {
      ReportType: 'RoundDetails',
      RoundID: round_id,
      LicenseeName: @licensee_name,
      Timestamp: (Time.now.to_f.round(3)*1000).to_i.to_s
    }
    response = post_request('/playreport', params)
    response
  end

  def trans_report(trans_id)
    params = {
      ReportType: 'TransactionDetails',
      TransactionID: trans_id,
      LicenseeName: @licensee_name,
      Timestamp: (Time.now.to_f.round(3)*1000).to_i.to_s
    }
    response = post_request('/playreport', params)
    response
  end

  private

  def post_request(path, params)
    begin
      uri              = URI.parse(@back_url+path)
      http             = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl     = true
      request          = Net::HTTP::Post.new(uri.request_uri)
      request.body = params.to_hash.to_json
      request["Content-Type"] = "application/json"
      request["HMAC"] = encode_hmac(params)
      request["Content-Length"] = @body.size
      JSON.parse(http.request(request).body)
    rescue => error
      puts error
      false
    end
  end

  def encode_hmac(params)
    @body ||= params.to_hash.to_json
    digest = OpenSSL::Digest.new('sha256')
    @key   ||= Setting.wallet.sf.key
    hmac = OpenSSL::HMAC.digest(digest, @key, @body.to_s)
    Base64.strict_encode64(hmac.to_s)
  end
end