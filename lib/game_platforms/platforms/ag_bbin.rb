class GamePlatforms::AgBbin < GamePlatforms::Ag
  include Game::ProfitableBetFormHandler

  def initialize(user)
    @cagent           = Setting.game_platform.bbin.ag.cagent
    @prefix           = Setting.game_platform.bbin.ag.prefix
    @game_type        = 1
    super
  end

  def transfer_credit(credit, type)
    super(credit.to_i, type)
  end

  def transfer_credit_confirm(billno, credit, type)
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(10)}"
    super(billno, credit.to_i, type)
  end

  # 1: 真人娛樂, 3: 彩票, 4: BB體育, 5: 電子遊藝
  def transfer_single_game_url(game_id)
    @game_type = game_id
    GamePlatforms::Ag.instance_method(:transfer_game).bind(self).call
  end
end