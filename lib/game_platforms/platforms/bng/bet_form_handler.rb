module GamePlatforms::Bng::BetFormHandler
  GAME_PLATFORM_ID = 14

  def bet_forms_format(result)
    bet_forms = []

    if result.present?
      result.map do |data|
        user = User.find_by(username: data['player_id'])

        if User.exists?(id: user.id) && !ThirdPartyBetForm.bng.exists?(vendor_id: get_order_number(data))
          default_bet_form = default_bet_form_params(data, user.id)
          # 計算代理分成
          final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff].to_f)
          bet_forms << ThirdPartyBetForm.new(final_params.merge!(order_number: final_params[:order_number]))
        end

      end.compact
    end

    unless bet_forms.empty?
      Rails.logger.info bet_forms
      # insert data
      ActiveRecord::Base.transaction do
        ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: [:bet_total_credit, :reward_amount, :user_credit_diff, :valid_amount, :result]
      end
    end
  end

  private

  def default_bet_form_params(result, user_id)
    {
      user_id: user_id,
      game_platform_id: GAME_PLATFORM_ID,
      bet_total_credit: result['bet'].to_f,
      reward_amount: result['win'].to_f,
      user_credit_diff: result['win'].to_f - result['bet'].to_f,
      order_number: get_order_number(result),
      game_name_id: result['game_id'],
      valid_amount: result['bet'].to_f,
      round: result['round_id'].to_s,
      balance: result['balance_after'].to_f,
      # 轉成美東時間
      betting_at: result['c_at'].in_time_zone('America/New_York'),
      vendor_id: get_order_number(result),
      currency: result['currency'],
      result: get_result(result),
      response: result.to_json,
      from: 'bng'
    }
  end

  def get_order_number(result)
    result['type'] == 'ROLLBACK' ? result['original_transaction_id'] : result['transaction_id']
  end

  def get_result(result)
    (result['win'].to_f - result['bet'].to_f) > 0 ? 'W': 'L'
  end
end