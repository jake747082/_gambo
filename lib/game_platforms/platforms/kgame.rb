class GamePlatforms::Kgame < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  autoload :BetFormHandler, 'game_platforms/platforms/kgame/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :url, :agent_id, :prefix
  attr_reader :username, :password, :access_token
  attr_accessor :app_id, :gamecode

  def initialize(user = nil)
    @url      ||= Setting.game_platform.kgame.url
    @agent_id ||= Setting.game_platform.kgame.agent_id
    @prefix   ||= Setting.game_platform.kgame.prefix

    @encrypt_key  ||= Setting.game_platform.kgame.encrypt_key
    @secret_key   ||= Setting.game_platform.kgame.secret_key
    
    if user.present?
      # actype: 0 => 正式帳號 , 1 => 測試帳號
      @actype = 0
      game_platform_user = user.user_game_platformships.kgame
      @username = "#{@prefix}#{user.username}"
      game_platform_user.update!(username: @username) if game_platform_user.username.blank?
      @password = game_platform_user.password
    else
      @actype = 1
      @username = "demo_#{Time.zone.now.strftime("%Y%m%d%H%M%S")}#{SecureRandom.hex(3)}"
      @password = SecureRandom.hex(6)
    end
    # 檢查帳號是否存在對方平台, 若無則建立帳號
    check_and_create_account
  end

  # 取得設定
  def config
    config = {
      'slots' => {
        'category' => {
          1 => 'chess',
          2 => 'slots'
        },
        'games' => {
          1 => games_list(),
          2 => slots_games_list()
        }
      }
    }
  end

  def check_and_create_account
    params = {
      username: username,
      password: password,
      acType: @actype.to_i,
    }
    data = send_request(request_url('v2/do', 'RegisterOrLoginUser', params))
    @access_token = data['message'] if data['code'] == '0'
    data['code'] == '0' ? true : false
  end

  def get_balance
    params = {
      username: username,
      password: password,
      acType: @actype.to_i,
    }
    data = send_request(request_url('v2/do', 'GetBalance', params))
    (data['code'].to_i / 100).to_f.round(2)
  end

  def transfer_credit(credit, type)
    time_now = Time.zone.now.strftime("%Y%m%d%H%M%S")
    billno = "#{time_now}#{Random.rand(1000)}"
    params = {
      username: username,
      password: password,
      currency: 0,
      amount: credit * 100, #分
      orderId: billno,
      type: type,
      acType: @actype.to_i,
    }
    data = send_request(request_url('v2/do', 'TransactionCreate', params))
    if data['code'] == '0'
      transfer_credit_confirm(billno, credit, type)
    else
      false
    end
  end

  def games_list
    data = send_request(request_url('v2/do', 'GetGamesList', {}))
    data['message'].reject { |value|
      value["name"] == "美式老虎机"
    }
  end

  def slots_games_list
    data = send_request(request_url('v2/do', 'GetSlotsGamesList', {}))
    data['message']
  end

  def transfer_game
    @app_id ||= '0a636149fdf36c98e47e7b4e62fdbc7a'; # 百人牛牛H5
    @gamecode ||= ''
    params = {
      appId: @app_id,
      gamecode: @gamecode,
      accessToken: @access_token,
      lang: 'cn',
    }
    data = send_request(request_url('v2/do', 'GetGameUrl', params))
    data['message']
  end

  # 投注紀錄
  def betting_records
    get_betting_records('SLOT')
    get_betting_records('CHESS')
  end
  
  private

  def get_betting_records(prefix = 'SLOT')
    Rails.logger.info "======GamePlatforms::Kgame - #{prefix}======"
    now_at = Time.zone.now
    # 若拉的是第一筆, 以now_at為開頭
    # last_bet_form_at  = last_bet_form.nil? ? multiple_number_format(now_at - 5 * 60) : multiple_number_format(last_bet_form.betting_at)
    last_updated_at = GamePlatform.kgame.updated_at
    if prefix == 'SLOT'
      # 前45分鐘
      now_at = now_at - 45 * 60
      last_updated_at = last_updated_at - 45 * 60
      slot_bet_forms = ThirdPartyBetForm.unscoped.slot.kgame
      last_bet_form  = slot_bet_forms.empty? ? nil : slot_bet_forms.last
    else
      chess_bet_forms = ThirdPartyBetForm.unscoped.video.kgame
      last_bet_form = chess_bet_forms.empty? ? nil : chess_bet_forms.last
    end
    Rails.logger.info "======last_updated_at: #{last_updated_at}======"
    last_bet_form_at  = last_bet_form.nil? ? multiple_number_format(now_at - 5 * 60) : multiple_number_format(last_updated_at - 5 * 60)
    now_at = multiple_number_format(now_at)
    records = []
    # 一間距五分鐘
    Rails.logger.info "start time: #{last_bet_form_at} !!"
    Rails.logger.info "end time: #{now_at} !!"
    (last_bet_form_at..now_at).step(60*5).each do |timestamp|
      batchName = Time.at(timestamp).strftime("%Y%m%d%H%M")
      page = 1
      while page > 0
        params = {
          batchName: batchName.to_s,
          page: page, # 一頁兩千筆
        }
        data = send_request(request_url('v2/data', 'GetBetDetail', params))
        Rails.logger.info data
        page += 1
        if data.present? && data.key?('code') && data['code'] == '0'
          Rails.logger.info data['message']
          if data['message'].nil? || data['message'].count == 0
            page = 0
          else
            records += data['message']
          end
        else
          page = 0
        end
      end
    end
    Rails.logger.info "records size: #{records.size} !!"
    bet_forms_format(records)
  end

  # 倍數的數字
  def multiple_number_format(number, multiple = 5)
    number.to_i - number.to_i % (60 * multiple)
  end

  def transfer_credit_confirm(billno, credit, type)
    params = {
      username: username,
      password: password,
      currency: 0,
      amount: credit * 100, #分
      orderId: billno,
      type: type,
      acType: @actype.to_i,
    }
    data = send_request(request_url('v2/do', 'TransactionConfirm', params))
    data['code'] == '0' ? true : false
  end

  def send_request(url)
    begin
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request.initialize_http_header({"User-Agent" => "KG_#{@agent_id}"})

      response = http.request(request)
      @code = response.code
      JSON.parse(response.body)
    rescue Exception => e
      {}
    end
  end

  def request_url(path, action_key, args = {})
    args = {api: action_key, agent: @agent_id.to_i}.merge(args).to_json
    params = get_encode_params(args)
    sign = get_encode_sign(params)
    "#{@url}/#{path}?params=#{CGI.escape(params)}&sign=#{sign}"
  end

  def get_encode_params(str)
    cipher = ::OpenSSL::Cipher.new('des-ecb')
    cipher.encrypt
    if @encrypt_key.length < cipher.key_len
      @encrypt_key = @encrypt_key.ljust(cipher.key_len, "\0")
    end
    cipher.key = @encrypt_key

    cipher.padding = 1
    update_value = cipher.update(str)
    up_final = update_value + cipher.final

    Base64.encode64(up_final).gsub(/\n/, "")
  end

  def get_encode_sign(encode_params)
    params = Digest::MD5.hexdigest(encode_params + @secret_key)
    params[6, 16]
  end
end