class GamePlatforms::AgMg < GamePlatforms::Ag
  include Game::ProfitableBetFormHandler

  def initialize(user)
    @cagent           = Setting.game_platform.mg.ag.cagent
    @prefix           = Setting.game_platform.mg.ag.prefix
    super
  end

  def transfer_single_game_url(game_id)
    @game_type = game_id
    GamePlatforms::Ag.instance_method(:transfer_game).bind(self).call
  end
end