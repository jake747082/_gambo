# New - 金程捕魚機
class GamePlatforms::Bt < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  autoload :BetFormHandler, 'game_platforms/platforms/bt/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  attr_reader :url, :account_id, :security_code, :session_guid, :prefix
  attr_reader :username, :password
  attr_accessor :game_type, :game_name, :lang, :line

  def initialize(user = nil)
    
    @url           ||= Setting.game_platform.bt.url
    @account_id    ||= Setting.game_platform.bt.account_id
    @security_code ||= Setting.game_platform.bt.security_code
    @prefix        ||= Setting.game_platform.bt.prefix

    @session_guid = get_session_guid
    if session_guid && user.present?
      game_platform_user = user.user_game_platformships.bt
      @username = "#{@prefix}#{user.lobby_id}"
      game_platform_user.update!(username: @username) if game_platform_user.username.blank?
      @password = password_encrypt(game_platform_user.password, @username)
      check_and_create_account
    end
  end

  # 取得設定
  def self.config(current_file=__FILE__)
    abs_path = File.join(File.dirname(current_file), 'bt/config.yml')
    YAML::load(File.open(abs_path))
  end

  def check_and_create_account
    params = {
      account_id: account_id,
      username: username,
      password: password,
      currency: 'CNY',
      session_guid: session_guid
    }
    response = post_request('/v1/user', params.merge({check_code: check_code(params)}))
    !response['status'].nil? && response['status']['code'] == 0
  end

  def transfer_credit(credit, type)
    params = {
      account_id: account_id,
      username: username,
      amount: credit,
      session_guid: session_guid
    }
    path = (type == 'IN') ? '/v1/user/deposit' : '/v1/user/cashout'
    response = post_request(path, params.merge({check_code: check_code(params)}))
    !response['status'].nil? && !response['data'].nil? && response['status']['code'] == 0 ? response['data']['balance'].to_f : false
  end

  def get_balance
    params = {
      account_id: account_id,
      username: username,
      session_guid: session_guid
    }
    response = get_request('/v1/user/balance', params.merge({check_code: check_code(params)}))
    p response
    !response['status'].nil? && !response['data'].nil? && response['status']['code'] == 0 ? response['data']['balance'].to_f : false
  end

  def transfer_game
    # params = {
    #   account_id: account_id,
    #   username: username,
    #   password: password,
    #   game_type: game_type,
    #   locale: 'zh-CN',
    # }
    # response = post_request('/v1/api/lobby', params.merge({check_code: check_code(params)}))
    # !response['status'].nil? && response['status']['code'] == 0 ? response['url'] : false
  end

  def transfer_single_game
    params = {
      account_id: account_id,
      username: username,
      password: password,
      currency: 'CNY',
      game_code: game_name,
      game_type: game_type,
      lang: lang || 'zh-CN',
      line: line || '',
    }
    response = post_request('/v1/game/login', params.merge({check_code: check_code(params)}))
    response && !response['status'].nil? && !response['data'].nil? && response['status']['code'] == 0 ? response['data']['url'] : false
  end

  def logout
    params = {
      account_id: account_id,
      username: username,
      currency: 'CNY',
    }
    response = post_request('/v1/user/logout', params.merge({check_code: check_code(params)}))
    !response['status'].nil? && response['status']['code'] == 0
  end

  def games_list
    params = {
      account_id: account_id,
      session_guid: session_guid
    }
    response = post_request('/v1/game/list', params.merge({check_code: check_code(params)}))
    !response['status'].nil? && !response['data'].nil? && response['status']['code'] == 0 ? response['data'] : false
  end

  def lost_betting_records
    # 捕魚機
    for i in 1..115 do
      get_betting_records(2, "2019-10-28 14:52:17", i, 5000)
    end
  end

  def betting_records
    # 捕魚機
    get_betting_records(3)
    # 牌桌table
    get_betting_records(6)
    # slot
    get_betting_records(2)
    # 輪盤
    # get_betting_records(3)
  end

  private

  def get_betting_records(game_type_id = 3, date_from = nil, page = 1, page_count = 1000)
    total = 0
    bt_last_bet_form = ThirdPartyBetForm.bt.where(game_type_id: game_type_id).first
    bt_updated_time = GamePlatform.find_by_name('bt').updated_at.in_time_zone('America/Puerto_Rico').strftime("%Y-%m-%d %H:%M:%S")
    bt_default_start_time = bt_updated_time.nil? ? '2019-01-13 00:00:00' : bt_updated_time
    date_from  ||= bt_last_bet_form.nil? ? bt_default_start_time : (bt_last_bet_form.betting_at).in_time_zone('America/Puerto_Rico').strftime("%Y-%m-%d %H:%M:%S")
    date_to = (Time.zone.now-1.seconds).strftime("%Y-%m-%d %H:%M:%S")
    Rails.logger.info date_from
    Rails.logger.info date_to
    days = split_days(date_from, date_to)
    Rails.logger.info days
    days.each do |day|
      current_page = page
      loop do
        params = {
          account_id: account_id,
          datetime_from: day[:datetime_from],
          datetime_to: day[:datetime_to],
          page: current_page,
          page_count: page_count,
          game_type: game_type_name(game_type_id),
          session_guid: session_guid
        }
        response = post_request('/v1/game/histories', params.merge({check_code: check_code(params)}))
        if !response['status'].nil? && !response['data'].nil? && response['status']['code'] == 0
          total = response['data']['total']
          bet_forms_format(response['data']['histories'], game_type_id)
        else
          break
        end
        # 是否停止拉取下一頁（total已小於等於拉取的資料量）
        is_stop = total <= current_page.to_i * page_count.to_i
        current_page+=1
        break if is_stop
      end
    end

  end

  def split_days(datetime_from, datetime_to)
    from = Date.parse(datetime_from)
    to = Date.parse(datetime_to)
    if from == to
      # 同一天
      [
        {datetime_from: datetime_from, datetime_to: datetime_to}
      ]
    else
      (from..to).group_by do |group|
        group.strftime("%Y-%m-%d")
      end.keys.map do |day|
        from_time = day == from.strftime("%Y-%m-%d") ? DateTime.parse(datetime_from).strftime("%H:%M:%S") : "00:00:00"
        to_time = day == to.strftime("%Y-%m-%d") ? DateTime.parse(datetime_to).strftime("%H:%M:%S") : "23:59:59"
        {datetime_from: day + " #{from_time}", datetime_to: day + " #{to_time}"}
      end
    end
  end

  # game_type : 2=>slot ; 3=>fish ; 6=>table
  def game_type_name(game_type_id = 3)
    if game_type_id == 6
      return 'table'
    end
    if game_type_id == 2
      return 'slot'
    end
    'fish' #default
  end

  def get_session_guid
    params = {account_id: account_id}
    response = post_request('/v1/session', params.merge({check_code: check_code(params)}))
    response != false && !response['status'].nil? && response['status']['code'] == 0 ? response['session'] : false
  end

  def check_code(params)
    params = {security_code: security_code}.merge(params).except(:check_code, :session_guid)
    params = params.map do |field, value|
      "#{field}=#{value}"
    end.join('&')
    Digest::MD5.hexdigest(params)
  end

  def post_request(path, params)
    begin
      uri              = URI.parse(url+path)
      http             = Net::HTTP.new(uri.host, uri.port)
      if uri.port == 443
        http.use_ssl     = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request          = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data(params)
      JSON.parse(http.request(request).body)
    rescue
      false
    end
  end

  def get_request(path, params)
    uri = URI.parse(url+path)
    params = params.map do |field, value|
      "#{field}=#{value}"
    end.join('&')
    request = Net::HTTP::Get.new(uri.request_uri + '?' + params)
    http = Net::HTTP.new(uri.host, uri.port)
    response = http.request(request)
    JSON.parse(response.body)
  end

  def password_encrypt(password, salt)
    Digest::SHA1.hexdigest("#{salt}-#{password}--")
  end
end