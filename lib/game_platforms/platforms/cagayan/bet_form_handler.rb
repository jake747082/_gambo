module GamePlatforms::Cagayan::BetFormHandler

  GAME_PLATFORM_ID = 6

  def bet_forms_format(record, prefix = 'SLOT')
    if record.present?
      bet_forms = record.map do |data|
        # @todo refactor : remove loop query
        username = data['userName'].split(Setting.game_platform.cagayan.prefix)[1]
        user = User.find_by_username(username)
        if user.present? && !ThirdPartyBetForm.cagayan.exists?(vendor_id: "#{prefix}_#{data['sequenceNo']}")
          default_bet_form = default_bet_form_params(user, data)
          User.update_counters user, play_total: default_bet_form[:user_credit_diff]
          # 計算代理分成
          final_params = params_with_dispatched_credit_for_each_agent(user, default_bet_form, - default_bet_form[:user_credit_diff])
          # insert data
          bet_form = ThirdPartyBetForm.find_or_initialize_by(order_number: final_params[:order_number])
          bet_form.assign_attributes(final_params)
          bet_form.save
        end
      end.compact
    end
  end

  def update_bet_forms_format(records)
    if records.present?
      max_record_id = 0
      records.each do |record|
        record['recordIdList'].each do |id|
          bet_form = ThirdPartyBetForm.cagayan.video.find_by(order_number: "CAGAYAN_#{id}")
          if record['tableInfoId'].to_i == bet_form.table_id.to_i && record['shoeInfoId'].to_i == bet_form.inning.to_i
            bet_form.update(state: 0, extra: record.to_json)
          end
        end
        max_record_id = record['id']
      end
      GamePlatform.cagayan.update(record_id: max_record_id) if max_record_id > 0
    end
  end

  private

  def default_bet_form_params(user, data)
    # 2: slot, 1: 視訊
    if data['slotsType'].present?
      game_type_id = 2
      game_name_id =  data['slotsType']
      result = (data['stakeAmount'].to_f + data['winLoss'].to_f) > 0 ? 'W' : 'L'
      prefix = 'SLOT'
    else
      game_type_id = 1
      game_name_id =  data['gameType']
      result = data['bankerResult'].to_json
      prefix = 'LIVE'
    end
    {
      user_id:            user.id,
      game_platform_id:   GAME_PLATFORM_ID,
      bet_total_credit:   data['stakeAmount'],
      reward_amount:      data['stakeAmount'].to_f + data['winLoss'].to_f,
      user_credit_diff:   data['winLoss'],
      game_type_id:       game_type_id,

      order_number:       "CAGAYAN_#{data['id']}",
      table_id:           data['tableInfoId'] || nil, # 桌號
      inning:             data['shoeInfoId'] || nil,  # 靴號
      stage:              data['issueNo'] || nil,     # 期号，一个台桌中某一局的唯一标识

      game_name_id:       game_name_id,
      balance:            data['balanceAfter'] || nil,
      betting_at:         Time.zone.at(data['endTime'].to_i/1000),
      vendor_id:          "#{prefix}_#{data['sequenceNo']}",
      valid_amount:       data['validStake'],
      
      game_kind:          data['gameType'],
      round:              data['roundId'],
      result:             result,
      ip:                 data['ip'] || nil,
      currency:           data['currency'],
      response:           data.to_json,
      state:              data['bankerResult'].present? && data['bankerResult'] == '-1' ? 0 : 1,
      from:               'cagayan',
    }
  end

end
