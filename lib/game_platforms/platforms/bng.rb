class GamePlatforms::Bng < GamePlatforms::Base::Interface
  include Game::ProfitableBetFormHandler

  # 處理遊戲歷程
  autoload :BetFormHandler, 'game_platforms/platforms/bng/bet_form_handler'
  [ BetFormHandler ].each { |m| include m }

  def initialize(user = nil)
    @user ||= user
    @url ||= Setting.game_platform.bng.url
    @project_name ||= Setting.game_platform.bng.project_name
    @wl ||= Setting.game_platform.bng.wl
  end

  def games_list
    uri = "#{@url}/#{@project_name}/api/v1/game/list"
    params = { api_token: Setting.game_platform.bng.api_token }
    res = send_request('games_list', uri, params)
    res['items']
  end

  def transfer_single_game(game_id, lang)
    # 建立user gametoken
    token = bng_game_token
    tz = "-240" #美東-4
    platform = "desktop" #desktop / mobile
    "#{@url}#{@project_name}/game.html?token=#{token}&game=#{game_id}&ts=#{Time.now.to_i}&wl=#{@wl}&lang=#{bng_lang(lang)}&tz=#{tz}&platform=#{platform}"
  end

  def betting_records
    uri = "#{@url}/#{@project_name}/api/v1/transaction/list"
    Rails.logger.info "-------------bng report query start-----------"
    Rails.logger.info betting_params
    res = send_request('betting_records', uri, betting_params)
    bet_forms_format(res["items"]) unless res["items"].empty?
    Rails.logger.info "-------------bng report query end-------------"
    res["items"]
  end

  private

  # en /zh / zh-hant / vi
  def bng_lang(lang)
    langs = {
      "zh-CN" => "zh",
      "zh-TW" => "zh-hant"
    }
    langs.include?(lang) ? langs[lang] : "en"
  end

  def bng_game_token
    game_token = loop do
      random_game_token = SecureRandom.hex(10)
      break random_game_token unless User.exists?(bng_token: random_game_token)
    end
    @user.update(bng_token: game_token)
    game_token
  end

  def betting_params
    last_update_time = ThirdPartyBetForm.bng.exists? ? ThirdPartyBetForm.bng.order(:updated_at).first.updated_at.strftime("%Y-%m-%dT%H:%M:%S%:z") : 1.day.ago.strftime("%Y-%m-%dT%H:%M:%S%:z")
    last_query_time = convert_timeZone_to_UTC(last_update_time)
    now_query_time = Time.zone.now.in_time_zone("London").strftime("%Y-%m-%dT%H:%M:%S%:z")
    {
      api_token: Setting.game_platform.bng.api_token,
      start_date: last_query_time,
      end_date: now_query_time
    }
  end

  def convert_timeZone_to_UTC(serverTime)
    (DateTime.parse(serverTime) - 4.hours).strftime("%Y-%m-%dT%H:%M:%S%:z")
  end

  def send_request(cmd, url, params)
    Rails.logger.info "=== bng #{cmd} START==="
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
    request.body = params.to_json
    response = http.request(request)
    Rails.logger.info "response.body: #{response.body}"
    Rails.logger.info "=== bng #{cmd} END  ==="
    JSON.parse(response.body)
  end
end