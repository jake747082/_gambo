require 'activerecord-import/base'
class GamePlatforms::Histories
  ActiveRecord::Import.require_adapter('mysql2')

  attr_accessor :game_platform

  def initialize(game_platform_name, user = nil)
    @game_platform_name = game_platform_name
    self.game_platform = "GamePlatforms::#{game_platform_name.camelize}".constantize.new(user)
  end

  # 將下注資料匯入db
  def import!(lost_folder_path = nil)
    begin
      ActiveRecord::Base.transaction do
        Rails.logger.info '======GamePlatforms::Histories======'
        bet_forms = lost_folder_path.nil? ? game_platform.betting_records : game_platform.lost_betting_records

        # 更新updated_at
        if lost_folder_path.nil?
          the_game_platform = GamePlatform.find_by_name(@game_platform_name.to_s)
          GamePlatform.update(the_game_platform.id, updated_at: Time.zone.now)
        end
        Rails.logger.info '======GamePlatforms::Histories end======'
        true
      end
    rescue => e
      Rails.logger.info "========Exception :#{e.message}============"
      ExceptionNotifier.notify_exception(e)
      false
    end
  end

  # 將電子遊戲下注資料匯入db
  # def import_electronic_games!
  #   bet_form_column = [:shareholder_id, :shareholder_win_amount, :shareholder_owe_parent, :director_id, :director_win_amount, :director_owe_parent, :agent_id, :agent_win_amount, :agent_owe_parent, :user_id, :game_platform_id, :bet_total_credit, :reward_amount, :user_credit_diff, :row_id, :game_id, :category_id, :betting_at, :transaction_id, :extra]
  #
  #   bet_forms = game_platform.betting_records
  #   ThirdPartyBetForm.import bet_forms, on_duplicate_key_update: bet_form_column if bet_forms.present?
  # end

  def check_betting_at!
    begin
      ThirdPartyBetForm.transaction do
        ThirdPartyBetForm.from_ag.each do |bet_form|
          response_betting_at = JSON.parse(bet_form.response)[0].select{ |field, date| field == 'betTime' || field == 'SceneStartTime'}[0][1]
          unless bet_form.betting_at - 12.hours == Time.zone.parse(response_betting_at).to_datetime
            bet_form.update_columns(betting_at: Time.zone.parse(response_betting_at).to_datetime + 12.hours)
          end
        end
        true
      end
    rescue => e
      ExceptionNotifier.notify_exception(e)
      false
    end    
  end
  
  def self.count_jackpot
    site_config = SiteConfig.first
    site_config.bet_total_credit = PlatformGame.all.sum(:bet_total_credit)
    site_config.save
  end
end
