module Gambo
  class Railtie < Rails::Railtie
    config.before_initialize do
      # Append shared i18n file path
      shared_i18n_file_path = Pathname.new(File.dirname(__FILE__)).join('../..', 'config', 'locales', '**', '*.{rb,yml}')
      config.i18n.load_path += Dir[shared_i18n_file_path]
    end

    initializer "gambo.view_helpers" do
      [ ::DateRangePickerHelper, ::FormHelper, ::FormatHelper, ::LinkHelper, ::MachinesHelper, ::IconHelper, ::BankHelper, ::HistoriesHelper ].each do |m|
        ActionView::Base.send(:include, m)
      end
    end
  end
end