module Gambo::Reform::AtomicSave
  extend ActiveSupport::Concern

  included do
    include ActiveSupport::Callbacks

    define_callbacks :transaction

    def self.before_transaction(method_name)
      set_callback :transaction, :before, method_name
    end

    def self.before_commit(method_name)
      set_callback :transaction, :after, method_name
    end
  end

  ReformAtomicSaveError = Class.new(StandardError)

  def save_with_transaction(params={}, &block)
    begin
      save_with_transaction!(params, &block)
    rescue ReformAtomicSaveError => e
      false
    end
  end

  def save_with_transaction!(params={}, &block)
    ActiveRecord::Base.transaction do
      run_callbacks :transaction do
        # 1. update Record's data for callbacks / validates
        update!(params)

        # 2. do validate data
        unless validate(params)
          raise ReformAtomicSaveError, "Not Validate => #{self.inspect}"
        end

        # 3. define how to store data in block
        yield if block_given?
        save
      end
    end
    true
  end
end