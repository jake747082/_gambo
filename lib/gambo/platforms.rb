module Gambo::Platforms
  extend ActiveSupport::Concern

  mattr_accessor :platforms
  @@platforms ||= {
    shared: [],
    www: [],
    api: [],
    agent: [],
    admin: []
  }

  module ClassMethods
    def shared(&block)
      Gambo::Platforms.platforms[:shared].push(block)
    end

    def platform(platform, &block)
      Gambo::Platforms.platforms[platform].push(block)
    end
  end

  def self.load_platform(platform)
    @@platforms[:shared].each { |proc| proc.call }
    @@platforms[platform].each { |proc| proc.call }
  end
end
