require 'rails_helper'

describe Registry do
  it "should return true when set a valiable" do
    expect(Registry.helloworld = 10).to eq 10
    expect(Registry.helloworld).to eq 10
    expect(Thread.current["registry_helloworld"]).to eq 10
  end
end