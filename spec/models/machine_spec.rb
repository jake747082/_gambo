describe Machine do
  it "#title should display nickname and mac_address" do
    machine = Machine.new
    allow(machine).to receive(:mac_address) { '2A-18-78-51-C1-7C' }
    allow(machine).to receive(:nickname) { 'PC-1' }
    expect(machine.title).to eq "PC-1 (2A-18-78-51-C1-7C)"
  end

  context "#credit_left with credit_max: 1000" do
    before(:all) { @machine = Machine.new(credit_max: 1000) }
    subject { @machine }
    it "should returns 1000 with credit_used: 0" do
      subject.credit_used = 0
      expect(subject.credit_left).to eq 1000
    end

    it "should returns 1 when credit_used: 999" do
      subject.credit_used = 999
      expect(subject.credit_left).to eq 1
    end
  end

  context "#cashout depends on credit_left" do
    it "will returns TRUE when credit_left: 0" do
      allow_any_instance_of(Machine).to receive(:credit_left) { 0 }
      expect(Machine.new).to be_cashout
    end

    it "will returns FALSE when credit_left > 0" do
      allow_any_instance_of(Machine).to receive(:credit_left) { 10 }
      expect(Machine.new).to_not be_cashout
    end
  end

  context "#can_deposit? depends on boot OR credit_left (return false means need to cashout first)" do
    subject { Machine.new }

    it "will return TRUE when boot: TRUE OR credit_left: 0" do
      allow_any_instance_of(Machine).to receive(:boot?) { true }
      allow_any_instance_of(Machine).to receive(:credit_left) { 10 }
      expect(subject).to be_can_deposit
    end

    it "will return TRUE when boot: FALSE, and credit_left: 0" do
      allow_any_instance_of(Machine).to receive(:boot?) { false }
      allow_any_instance_of(Machine).to receive(:credit_left) { 0 }
      expect(subject).to be_can_deposit
    end

    it "will return TRUE when boot: TRUE, and credit_left > 0" do
      allow_any_instance_of(Machine).to receive(:boot?) { false }
      allow_any_instance_of(Machine).to receive(:credit_left) { 10 }
      expect(subject).to_not be_can_deposit
    end
  end

  it "#shutdown is antonym with boot?" do
    expect(Machine.new(boot: true)).to_not be_shutdown
    expect(Machine.new(boot: false)).to be_shutdown
  end

  it "#private_channel should returns private-machine-\#{mac_address}" do
    allow_any_instance_of(Machine).to receive(:mac_address) { '2A-18-78-51-C1-7C' }
    expect(Machine.new.private_channel).to eq "private-machine-2A-18-78-51-C1-7C"
  end
end
