FactoryGirl.define do

  factory :admin do
    username "manager"
    nickname "Manager"

    factory :new_manager do
      password "password"
      password_confirmation "password"
    end
    factory :incorrect_manager do
      username "incorrect_manager"
      nickname "IncorrectManager"
      password "password"
    end
    factory :incorrect_manager2 do
      username "incorrect_manager"
      nickname "IncorrectManager"
      password "password"
      password_confirmation "password"
      role nil
    end
  end

end