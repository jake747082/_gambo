describe Slot::UnderseaKing do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  it_behaves_like 'slot machine with predefine strategy'
  it_behaves_like 'slot machine with lines'

  context "When Assigned dispatched_spin_amount: 60, and got bonus (2 linked)" do
    it_behaves_like 'a spin with response bonus, machines, bet_form' do
      let(:dispatched_amount) { 60 }
      let(:linked_bonus_count) { 2 }
      let(:bonus_amount) { 50 }

      it_behaves_like 'a spin without free spin'
      it_behaves_like 'a spin with icons in each reels'
    end
  end

  context "When Assigned dispatched_spin_amount: 100, and got bonus (3 linked)" do
    it_behaves_like 'a spin with response bonus, machines, bet_form' do
      let(:dispatched_amount) { 100 }
      let(:linked_bonus_count) { 3 }
      let(:bonus_amount) { 100 }
      
      it_behaves_like 'a spin with free spin'
      it_behaves_like 'a spin with icons in each reels'
    end
  end
end