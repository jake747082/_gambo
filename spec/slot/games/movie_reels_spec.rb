describe Slot::MovieReels do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  it_behaves_like 'slot machine with predefine strategy'
  it_behaves_like 'slot machine with lines'
end