describe Slot::PuppyPower do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  it_behaves_like 'slot machine with predefine strategy'
  it_behaves_like 'slot machine with lines'

  context "When trigger a bonus event" do
    pending
    # before(:all) {
    #   @machine.update!(credit_max: 2000, credit_used: 0)
    #   @service = Slot::Spin.new(@slot_puppy_power, @machine)
    # }
    # let(:service) { @service }
    # let(:bonuses) { service.events[:bonus] }
    # let(:bonus) { bonuses.first }
    #
    # it "#spin! should returns true" do
    #   allow(Slot::PuppyPower).to receive(:trigger_event) { true }
    #   allow(Slot::PuppyPower).to receive(:is_bonus) { true }
    #   expect(service.spin!(10, 25)).to be true
    # end
    #
    # it "should have only 1 bonus" do
    #   expect(bonuses.size).to eq 1
    # end
    #
    # it "should have linked position" do
    #   expect(bonus[:scatter].size).to eq 3 # [1, 3, 5] reels
    #   bonus[:scatter].each do |position|
    #     expect(position).to have_key :x
    #     expect(position).to have_key :y
    #   end
    # end
  end

  context "When trigger a free spin event" do
    pending
    # before(:all) {
    #   @machine.update!(credit_max: 2000, credit_used: 0)
    #   @service = Slot::Spin.new(@slot_puppy_power, @machine)
    # }
    # let(:service) { @service }
    #
    # it "#spin! should returns true" do
    #   allow(Slot::PuppyPower).to receive(:trigger_event) { true }
    #   allow(Slot::PuppyPower).to receive(:is_bonus) { false }
    #   expect(service.spin!(10, 25)).to be true
    # end
    #
    # it_behaves_like 'a spin with free spin'
  end
end
