RSpec.describe Slot::Spin do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "slot game"

  context "When Machine without credit spin" do
    pending
    # before(:all) {
    #   @machine.update!(credit_max: 0, credit_used: 0)
    #   @service = Slot::Spin.new(@slot_mayan, @machine)
    #   @status = @service.spin!(10, 10)
    # }
    #
    # it { expect(@status).to eq false }
    # it { expect(@service.errors).to have_key :credit }
    # it { expect(@service.errors).to_not have_key :bet_lines }
  end

  context "When Machine Spin with enough credit" do
    pending
    # before(:all) { @machine.update!(credit_max: 10000) }
    #
    # context "and incorrent Lines and credit" do
    #   before(:all) {
    #     @service = Slot::Spin.new(@slot_mayan, @machine)
    #     @status = @service.spin!(-10, 30)
    #   }
    #   it { expect(@status).to eq false }
    #   it { expect(@service.errors).to have_key :credit }
    #   it { expect(@service.errors).to have_key :bet_lines }
    # end
    #
    # context "and corrent Lines and credit" do
    #   before(:all) {
    #     @service = Slot::Spin.new(@slot_mayan, @machine)
    #     @status = @service.spin!(10, 25)
    #   }
    #
    #   it { expect(@status).to eq true }
    #   it { expect(@machine.credit_used).to eq (250 - @service.win_amount) }
    # end
  end

  context "When Machine Spin with credit 0.001" do
    pending
    # before(:all) { @service = Slot::Spin.new(@slot_mayan, @machine) }
    #
    # it { expect(@service.spin!(0.001, 25)).to eq false }
    # it { expect(@service.errors).to have_key(:credit) }
  end
end
