RSpec.describe Slot::Strategy::Predefine do

  context "when spin with no credit" do
    it "should be exception when line is not array or empty array" do
      expect {
        Slot::Strategy::Predefine.new(Slot::MayanEclipse).prepare(win_max: 10, credit: 0.1, bet_lines: []).spin!
      }.to raise_error(Slot::Strategy::Base::ParamError)

      expect {
        Slot::Strategy::Predefine.new(Slot::MayanEclipse).prepare(win_max: 10, credit: 0.1).spin!
      }.to raise_error(Slot::Strategy::Base::ParamError)
    end
  end

  context "When dispatch a line with position assign special (event) icon" do
    EVENT_ICON_1 = 11
    EVENT_ICON_POSITION_1 = 1

    EVENT_ICON_2 = 12
    EVENT_ICON_POSITION_2 = 10
    DISPATCHED_LINE_ID = 2

    before(:all) {
      @strategy = Slot::Strategy::Predefine.new(Slot::MayanEclipse).prepare(win_max: 3000, credit: 1, bet_lines: [1,2,3,4])
      @strategy.result.grids[EVENT_ICON_POSITION_1] = EVENT_ICON_1
      @strategy.result.grids[EVENT_ICON_POSITION_2] = EVENT_ICON_2
      payout = {id: 2, links: 5, multiple: 3000} # POSITIONS => 6, 7, 8, 9, 10
      @strategy.send(:set_line_payout, DISPATCHED_LINE_ID, payout, 3000)
    }
    let(:strategy) { @strategy }
    let(:result) { strategy.result }

    it "should rollback this line assignment" do
      # DISPATCHED_LINE_ID's position routes, not include last position: 10
      [6, 7, 8, 9].each do |position|
        expect(result.grids[position]).to be_blank
      end
    end

    it { expect(result.grids[10]).to be EVENT_ICON_2 }
  end

  context "When dispatch a line with position assign wildcard icon" do
    WILDCARD_ICON = 1
    DISPATCHED_LINE_WITH_WILDCARD_ID = 2
    WILDCARD_ICON_POSITION = 2

    before(:all) {
      @strategy = Slot::Strategy::Predefine.new(Slot::MayanEclipse).prepare(win_max: 3000, credit: 1, bet_lines: [1,2,3,4])
      @strategy.result.grids[WILDCARD_ICON_POSITION] = WILDCARD_ICON
      payout = {id: 2, links: 5, multiple: 3000} # POSITIONS => 6, 7, 8, 9, 10
      @strategy.send(:set_line_payout, DISPATCHED_LINE_WITH_WILDCARD_ID, payout, 3000)
    }

    let(:strategy) { @strategy }
    let(:result) { strategy.result }

    it "should rollback this line assignment" do
      # DISPATCHED_LINE_ID's position routes, not include last position: 10
      [1, 3, 4, 5].each do |position|
        expect(result.grids[position]).to be 2
      end
    end

    it { expect(result.grids[2]).to be WILDCARD_ICON }
  end

  context "#should_not_put_icon_ids_for_position" do
    let(:strategy) { Slot::Strategy::Predefine.new(Slot::MayanEclipse).prepare(win_max: 0, credit: 1, bet_lines: (1..25).to_a) }
    let(:grids) { strategy.result.grids }

    context "When previous icon is wildcard" do
      it "and line has exists payout should find icon #6" do
        grids[1], grids[2], grids[3] = 6, 1, 1
        expect(strategy.send(:should_not_put_icon_ids_for_position, 4)).to include 6
      end

      it "but line NOT HAS exists payout should find icon #6" do
        grids[1], grids[2] = 6, 1
        expect(strategy.send(:should_not_put_icon_ids_for_position, 3)).to include 6
      end
    end

    context "When previous icon is payouts" do
      it "should find icon #4 when previous icon is #4" do
        grids[1] = 4
        expect(strategy.send(:should_not_put_icon_ids_for_position, 2)).to include 4
      end

      it "but line NOT HAS exists payout should find icon #6" do
        grids[1], grids[2], grids[3] = 6, 1, 6
        expect(strategy.send(:should_not_put_icon_ids_for_position, 4)).to include 6
      end
    end
  end
end
