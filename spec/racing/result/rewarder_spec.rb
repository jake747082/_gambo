describe Racing::Result::Rewarder do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"
  include_context "racing machine bet"
  include_context "racing user bet"

  context "When Racing Reward" do
    pending
    # before { @rewarder = Racing::Result::Rewarder.new(@racing_horses_schedule, 80) }
    # it "#full_ranking should be get all ranks" do
    #   full_ranking = @rewarder.full_ranking("1,2,3")
    #   expect(full_ranking.size).to eq 8
    #   expect(full_ranking[0]).to eq 1
    #   expect(full_ranking[1]).to eq 2
    #   expect(full_ranking[2]).to eq 3
    # end
    #
    # it "reward result should get 3 rank and reward amount" do
    #   reward_result = @rewarder.rand_reward_result
    #   expect(reward_result[0].split(',').map(&:to_i).size).to eq 3
    #   expect(reward_result[1]).to be > 0
    # end
  end
end
