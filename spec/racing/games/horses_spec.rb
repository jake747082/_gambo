describe Racing::Horses do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"

  it_behaves_like 'racing game with config' do
    let(:human_game_name) { 'Horses' }
    let(:champion_odds_for_bet_number_1) { 3 }
    let(:champion_uncounted_odds_for_bet_number_1) { '3/1' }
  end
end