shared_context "rebuild history" do
  before(:all) {
    RacingGame.delete_all
  }
  include_context "racing game"
  include_context "racing schedule"
  include_context "racing machine bet"
  include_context "racing user bet"
end

RSpec.describe Racing::Cashout::HistoryHandler do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"
  include_context "racing machine bet"
  include_context "racing user bet"

  context "When Schedule Cashout" do
    context "but not reward" do
      before(:all) {
        Racing::Cashout.new(@racing_horses_schedule.id, ['2,3,1', 0]).call!
        @history = @racing_horses.reload
      }
      it "history and schedule #total_lose_amount should be 10" do
        expect(@history.total_lose_amount).to eq 20
        expect(@racing_horses_schedule.reload.total_lose_amount).to eq 20
      end
      it "#history should increase preparing credit for each cycle's ratio" do
        expect(@history.preparing_xs).to eq 13
        expect(@history.preparing_sm).to eq 3.4
        expect(@history.preparing_md).to eq 1.6
        expect(@history.preparing_lg).to eq 0.8
        expect(@history.preparing_xl).to eq 0.2
      end
    end

    context "and reward" do
      include_context "rebuild history"
      before(:all) {
        @racing_horses.update!(dispatching_xs: 40, dispatching_sm: 30)
        Racing::Cashout.new(@racing_horses_schedule.id, ['1,2,3', 80]).call!
        @history = @racing_horses.reload
      }
      it "history and schedule #total_win_amount should be 30" do
        expect(@history.total_win_amount).to eq 60
        expect(@racing_horses_schedule.reload.total_win_amount).to eq 60
      end
      it "#history should NOT deduct from dispatching" do
        expect(@history.dispatching_xs).to eq 10
        expect(@history.dispatching_sm).to eq 0
        expect(@history.dispatching_md).to eq 0
        expect(@history.dispatching_lg).to eq 0
        expect(@history.dispatching_xl).to eq 0
      end
    end
  end
end