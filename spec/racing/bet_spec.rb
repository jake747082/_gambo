RSpec.describe Racing::Bet do
  include_context "site config"
  include_context "agent tree and machine"
  include_context "racing game"
  include_context "racing schedule"

  before(:all){
    @bet_types = ['champion', 'place', 'quinella']
    @service = Racing::Bet.new(@racing_horses_schedule.id, @machine)
    @bet_info = {'sub_bets' => [{'bet_credit' => rand(1..99), 'bet_number' => rand(1..8), 'bet_type' => @bet_types[rand(0..1)]}]}
  }

  context "When Machine without credit and shutdown" do
    before(:all) {
      @machine.update!(credit_max: 0, credit_used: 0, boot: 0)
      @bet_service = Racing::Bet.new(@racing_horses_schedule.id, @machine)
      bet_info = {'sub_bets' => [{"bet_credit" => rand(1..99), "bet_number" => rand(1..8), "bet_type" => @bet_types[rand(0..1)]}]}
      @status = @bet_service.bet!(bet_info)
    }

    it { expect(@status).to eq false }
    it { expect(@bet_service.errors).to have_key :machine }
    it { expect(@bet_service.errors).to have_key :bet_total_credit }
  end

  context "When Machine with enough credit and boot" do
    before(:all) { @machine.update!(credit_max: 10000, boot: 1) }

    context "and input corrent data" do
      pending
      # before(:all) {
      #   bet_info = {'sub_bets' => [{"bet_credit" => 10, "bet_number" => 8, "bet_type" => @bet_types[0]}]}
      #   @status = @service.bet!(bet_info)
      # }
      #
      # it { expect(@status).to eq true }
    end
    context "and Schedule not bet mode" do
      before(:all) {
        @racing_horses_schedule.update!(stop_receive_bet_form_time: Time.zone.now)
        @bet_service = Racing::Bet.new(@racing_horses_schedule.id, @machine)
        bet_info = {'sub_bets' => [{"bet_credit" => rand(1..99), "bet_number" => rand(1..8), "bet_type" => @bet_types[rand(0..1)]}]}
        @status = @bet_service.bet!(bet_info)
      }

      it { expect(@status).to eq false }
      it { expect(@bet_service.errors).to have_key :racing_schedule }
    end

    context "and Machine duplicate bet this Schedule" do
      pending
      # before(:all) {
      #   bet_info = {'sub_bets' => [{"bet_credit" => rand(1..99), "bet_number" => rand(1..8), "bet_type" => @bet_types[rand(0..1)]}]}
      #   @status = @service.bet!(bet_info)
      #   @status_duplicate_bet = @service.bet!(bet_info)
      # }
      #
      # it { expect(@status_duplicate_bet).to eq false }
      # it { expect(@service.errors).to have_key :machine }
    end

    context "and input incorrent #bet_type and #bet_number" do
      before(:all) {
        bet_info = {'sub_bets' => [{"bet_credit" => 0, "bet_number" => 9, "bet_type" => @bet_types[2]}]}
        @status = @service.bet!(bet_info)
        @status_duplicate_bet = @service.bet!(bet_info)
      }

      it { expect(@status_duplicate_bet).to eq false }
      it { expect(@service.errors).to have_key :bet_info }
    end

    context "and input incorrent #bet_credit" do
      before(:all) {
        bet_info = {'sub_bets' => [{"bet_credit" => 0, "bet_number" => 8, "bet_type" => @bet_types[0]}]}
        @status = @service.bet!(bet_info)
      }

      it { expect(@status).to eq false }
      it { expect(@service.errors).to have_key :bet_info }
    end
  end

end
