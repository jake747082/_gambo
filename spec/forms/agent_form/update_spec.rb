describe AgentForm::Update do
  include_context "agent tree and machine"

  context "When pass blank password" do
    before(:all) { @form = AgentForm::Update.new(@agent) }

    it "#update will return true" do
      expect(@form.update(password: '', password_confirmation: '')).to be true
    end

    it "agent's password still old" do
      expect(@agent.valid_password?('888888')).to be true
    end
  end

  context "When parent agent have no enough credit to dispatch" do
    before(:all) {
      @agent.update!(credit_max: 0, credit_dispatched: 0, credit_used: 0)
      @director.update!(credit_max: 1000, credit_dispatched: 0, credit_used: 0)
      @form = AgentForm::Update.new(@agent)
    }

    it "#update will return false" do
      expect(@form.update(credit_max: 10000)).to be false
    end

    it "#errors should with credit_max error" do
      expect(@form.errors[:credit_max]).to be_present
    end
  end

  context "when input #credit_max" do
    before(:all) { @form = AgentForm::Update.new(@agent) }

    it "2 decimal should return true" do
      expect(@form.update(credit_max: 0.01)).to be true
    end
  end

  context "when parent agent have enough credit to dispatch" do
    it "#update will return true" do
      agent.update!(credit_max: 0, credit_dispatched: 0, credit_used: 0)
      director.update!(credit_max: 1000, credit_dispatched: 0, credit_used: 0)
      expect(AgentForm::Update.new(agent).update(credit_max: 100)).to be true
    end

    it "#director #credit_dispatched should be increase to 100" do
      expect(director.credit_dispatched).to eq 100
    end
  end
end
