describe AgentForm::SubAccount::Update do
  include_context "agent tree and machine"

  context "when update agent sub account" do
    let(:exists_sub_account) { @sub_account_form.model.reload }

    it "should return true, when username, password valid" do
      form = AgentForm::SubAccount::Update.new(exists_sub_account)
      expect(form.update(nickname: 'new_nickname', username: 'sub_account', password: '222222', password_confirmation: '222222')).to be true
      expect(exists_sub_account.nickname).to eq 'new_nickname'
      expect(exists_sub_account.valid_password?('222222')).to be true
    end
  end
end