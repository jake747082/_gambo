RSpec.describe MachineForm::Create do
  include_context "agent tree and machine"

  context "When create a NOT EXISTS account" do
    context "with invalid mac address" do
      let(:create_machine_form) { MachineForm::Create.new(Machine.new(mac_address: '3c:15:c2:d4:a6:b1'))  }

      it "invalid maintain code" do
        expect(create_machine_form.create(maintain_code: '1234')).to be false
        expect(create_machine_form.errors[:maintain_code].size).to eq(1)
        expect(create_machine_form.errors[:mac_address].size).to eq(1)
        expect(create_machine_form.errors[:nickname].size).to eq(1)
      end
    end

    context "with invalid mac address" do
      let(:create_machine_form) { MachineForm::Create.new(Machine.new)  }
      it "should show error with mac address" do
        expect(create_machine_form.create(mac_address: '3-15-c2-d4-a6-b1', maintain_code: '1234')).to be false
        expect(create_machine_form.errors[:mac_address]).to include "Invalid mac address format"
        expect(create_machine_form.errors[:nickname].size).to eq(1)
      end
    end

    context "with valid mac and maintain code" do
      let(:create_machine_form) { MachineForm::Create.new(Machine.new) }
      let(:machine_params) {
        { nickname: '電腦一號', mac_address: '3c-15-c2-d4-a6-22', maintain_code: @agent.generate_maintain_code }
      }
      it "should be successful to create" do
        expect(create_machine_form.create(machine_params)).to be true
        expect(create_machine_form.model.id).not_to be_nil
      end
    end
  end

  context "When create a EXISTS mac address account" do
    context "and machine is deleted" do
      let!(:machine) {
        @machine.update!(deleted: true)
        @machine
      }
      let!(:exists_machine_form) {
        MachineForm::Create.new(machine)
      }

      it {
        expect(exists_machine_form.create(maintain_code: @agent.maintain_code)).to eq true
        expect(machine).to_not be_deleted
        expect(exists_machine_form.model).to eq(@machine)
        expect(machine.valid_password?(exists_machine_form.password)).to eq true
      }
    end
  end
end
