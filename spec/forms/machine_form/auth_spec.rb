describe MachineForm::Auth do
  include_context "agent tree and machine"

  before(:all) { @machine.update(password: '888888', password_confirmation: '888888')  }

  context "when input valid machine" do
    before(:all) { @form = MachineForm::Auth.new(mac_address: @machine.mac_address, password: '888888') }

    it { expect(@form).to be_valid }
  end

  context "when input invalid machine" do
    before(:all) { @form = MachineForm::Auth.new(mac_address: 'WRONG-MAC-ADDRESS', password: '999999') }

    it { expect(@form).to be_invalid }
    it { expect(@form.errors[:mac_address]).to include "This machine does not exist" }
  end

  context "when input incorrect password" do
    before(:all) { @form = MachineForm::Auth.new(mac_address: @machine.mac_address, password: '999999') }
    it { expect(@form).to_not be_valid }
    it { expect(@form.errors[:password]).to include "Invalid password" }
  end
end