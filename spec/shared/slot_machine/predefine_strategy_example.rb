shared_examples "slot machine with predefine strategy" do
  let(:slot_machine_model) { described_class }

  context "Predefine Strategy => Whin spin assigned max win amount", slow: true do
    (0..2000).each do |multiple|
      assigned_amount = multiple * 3
      it "win amount should not over: #{assigned_amount}" do
        strategy = Slot::Strategy::Predefine.new(slot_machine_model)
        bet_line = (1..slot_machine_model.lines.count)
        strategy.prepare(win_max: assigned_amount, credit: 1, bet_lines: bet_line.to_a).spin!
        expect(strategy.dispatched_credit).to eq strategy.result.win_amount
      end
      it "win amount should not over dispatched credit" do
        slot_machine = SlotMachine.find_by_game_model_name(slot_machine_model)
        service = ::Slot::Spin.new(slot_machine, @machine)
        status = service.spin!(1, slot_machine_model.lines.count)
        expect(service.reward_amount).to be <= service.win_max * service.reward_amount_multiple
      end
    end
  end
end