shared_context "slot spin" do
  before(:all) {
    @machine.update!(credit_max: 2000, credit_used: 0)
    @service = Slot::Spin.new(@slot_mayan, @machine)
    @status = @service.spin!(10, 25)
  }

  let(:service) { @service }
end