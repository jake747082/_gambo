RSpec.shared_context "racing schedule" do
  before(:all) {
    # Racing Schedule
    instance_variable_set("@racing_horses_schedule", RacingSchedule.create(racing_game_id: @racing_horses.id, start_receive_bet_form_time: Time.zone.now - 10, stop_receive_bet_form_time: Time.zone.now + 10, run_racing_time: Time.zone.now + 15, end_time: Time.zone.now + 20))
    instance_variable_set("@racing_eight_dogs_schedule", RacingSchedule.create(racing_game_id: @racing_eight_dogs.id, start_receive_bet_form_time: Time.zone.now - 10, stop_receive_bet_form_time: Time.zone.now + 10, run_racing_time: Time.zone.now + 15, end_time: Time.zone.now + 20))
    instance_variable_set("@racing_six_dogs_schedule", RacingSchedule.create(racing_game_id: @racing_six_dogs.id, start_receive_bet_form_time: Time.zone.now - 10, stop_receive_bet_form_time: Time.zone.now + 10, run_racing_time: Time.zone.now + 15, end_time: Time.zone.now + 20))
  }

  let(:racing_horses_schedule) { @racing_horses_schedule }
  let(:racing_eight_dogs_schedule) { @racing_eight_dogs_schedule }
  let(:racing_six_dogs_schedule) { @racing_six_dogs_schedule }
end
