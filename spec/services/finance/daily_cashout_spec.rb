describe Finance::DailyCashout do
  include_context "agent tree and machine"
  include_context "site config"
  include_context "slot game"
  include_context "slot spin"
  # include_context "racing game"
  # include_context "racing schedule"
  # include_context "racing bet"

  context "when generated agent finance" do
    pending
    # before(:all) {
    #   @service = Finance::DailyCashout.new
    #   @stats = @service.call(begin_date: Date::today, end_date: Date::today, agent: @agent)
    # }
    #
    # it "should have self daily list" do
    #   expect(@stats).to be_present
    # end
    #
    # it "#cashout_query_information should be agent and agent #slot_bet_form" do
    #   agent_level, total_bet_forms = @service.send(:cashout_query_information)
    #   expect(agent_level.to_s).to eq('agent')
    #   expect(total_bet_forms).to eq([ agent.all_slot_bet_forms, agent.all_racing_bet_forms])
    # end
  end

  context "when generated admin finance" do
    pending
    # before(:all) {
    #   @service = Finance::DailyCashout.new
    #   @stats = @service.call(begin_date: Date::today, end_date: Date::today)
    # }
    #
    # it "should have self daily list" do
    #   expect(@stats).to be_present
    # end
    #
    # it "#cashout_query_information should be shareholder and SlotBetForm" do
    #   agent_level, total_bet_forms = @service.send(:cashout_query_information)
    #   expect(agent_level.to_s).to eq('shareholder')
    #   expect(total_bet_forms).to eq([ SlotBetForm, RacingBetForm ])
    # end
  end

  context "when generated shareholder finance" do
    pending
    # before(:all) {
    #   @service = Finance::DailyCashout.new
    #   @stats = @service.call(begin_date: Date::today, end_date: Date::today, agent: @shareholder)
    # }
    #
    # it "should have self daily list" do
    #   expect(@stats).to be_present
    # end
    #
    # it "#cashout_query_information should be shareholder and shareholder #slot_bet_form" do
    #   agent_level, total_bet_forms = @service.send(:cashout_query_information)
    #   expect(agent_level.to_s).to eq('shareholder')
    #   expect(total_bet_forms).to eq([ shareholder.all_slot_bet_forms, shareholder.all_racing_bet_forms ])
    # end
  end
end
