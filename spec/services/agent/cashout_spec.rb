describe Agent::Cashout do
  include_context "agent tree and machine"

  context "when agent cashout" do
    before(:all) { @agent.update(credit_used: 10) }

    it "should set agent credit left to 0" do
      Agent::Cashout.call(agent)
      expect(agent.credit_used).to eq(0)
    end

    # it "should run block when cashout success" do
    #   expect { Agent::Cashout.call(agent) { raise "" }
    # end

    specify { expect { |b| Agent::Cashout.call(agent, &b) }.to yield_control } 
  end
end