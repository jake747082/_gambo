class AddColumnLastActionAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :last_action_at, :datetime
  end
end