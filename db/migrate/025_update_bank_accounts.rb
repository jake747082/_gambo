class UpdateBankAccounts < ActiveRecord::Migration
  def change
    add_column :bank_accounts, :sub_bank_name, :string, limit: 255, default: '',  after: :bank_title
    add_column :bank_accounts, :bank_id, :string, limit: 255, default: '', after: :bank_title
    remove_column :bank_accounts, :bank_title, :string
    remove_column :bank_accounts, :bank_code, :string
  end
end
