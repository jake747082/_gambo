class ChangeRoundIdTypeInBngRollbackLog < ActiveRecord::Migration
  def self.up
    change_column :bng_rollback_logs, :round_id, :string
  end

  def self.down
    change_column :bng_rollback_logs, :round_id, :integer
  end
end