class RenameBngVersionToBngVersions < ActiveRecord::Migration
  def self.up
    rename_table :bng_version, :bng_versions
  end

  def self.down
    rename_table :bng_versions, :bng_version
  end
end