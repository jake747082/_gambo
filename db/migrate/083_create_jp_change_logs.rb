class CreateJpChangeLogs < ActiveRecord::Migration
  def change
    create_table(:jp_change_logs, force: true) do |t|
      t.boolean :manual # 手動調整或是系統計算後jp變動紀錄
      t.decimal :befor_jp_change, precision: 12, scale: 4,default: 0.0 # 紀錄變動前jp值
      t.decimal :after_jp_change, precision: 12, scale: 4,default: 0.0 # 紀錄變動後jp值
      t.decimal :befor_bet_total_credit, precision: 15, scale: 4,default: 0.0
      t.decimal :after_bet_total_credit, precision: 15, scale: 4,default: 0.0
      t.decimal :jp_change, precision: 12, scale: 4,default: 0.0 # jp變動值
      t.decimal :jp_rate, precision: 5, scale: 2 # jp抽成比例
      t.references :admin, unsigned: true # 操作者
      t.text :note  # 備註
      t.string :game_platform_name #平台
      t.references :platform_game, unsigned: true, index: true #遊戲
      t.timestamps
    end
  end
end