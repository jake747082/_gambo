# 用來儲存每日會員存款/提款/打碼總額
class CreateUserDailyCreditLogs < ActiveRecord::Migration
  def change
    create_table :user_daily_credit_logs, force: true do |t|
      t.date :date
      # 類型(deposit: 0, cashout: 1, bet_east: 2, bet_bbin: 3, bet_ag: 4, bet_mg: 5)
      t.integer :type_cd, null: false, unsigned: true
      # 玩家
      t.integer :user_id, null: false, unsigned: true
      # 筆數
      t.integer :count, default: 0, null: false, unsigned: true
      # 總額
      t.decimal :total, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.timestamps
    end
    add_index :user_daily_credit_logs, :user_id
    add_index :user_daily_credit_logs, :date
    add_index :user_daily_credit_logs, [:date, :type_cd, :user_id], unique: true
  end
end
