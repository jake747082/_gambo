class CreateUnconfirmedUsers < ActiveRecord::Migration
  # 尚未驗證成功的註冊資料
  def change
    create_table(:unconfirmed_users, force: true) do |t|
      ## Database authenticatable
      # 0: phone, 1: email
      t.integer :auth_type_cd, default: 2 #預設帳號
      t.string :email, limit: 255, default: ''
      t.string :mobile, limit: 20, default: ''
      t.string :verification_code, limit: 8, default: ''
      t.datetime :verification_send_at
      t.integer :verification_send_count, default: 0, null: false, unsigned: true
      t.timestamps
    end

    add_index :unconfirmed_users, :email
    add_index :unconfirmed_users, :mobile
    add_index :unconfirmed_users, [:email, :mobile], unique: true
  end
end
