class AddSessionIdToUserGamePlatformships < ActiveRecord::Migration
  def change
    add_column :user_game_platformships, :session_id, :string, null: false, default: "", after: :password
  end
end
