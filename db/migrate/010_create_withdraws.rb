class CreateWithdraws < ActiveRecord::Migration
  def change
    create_table :withdraws do |t|
      t.decimal :credit, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.integer :status, default: 0, null: false, unsigned: true
      t.integer :user_id, default: 0, null: false, unsigned: true
      t.text :bank_account_info
      t.text :note
      t.timestamps
    end

    add_index :withdraws, :user_id
  end
end
