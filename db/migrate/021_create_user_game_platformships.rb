class CreateUserGamePlatformships < ActiveRecord::Migration
  def change
    create_table :user_game_platformships, force: true do |t|
      t.integer :user_id, null: false
      t.integer :game_platform_id, null: false
      t.string :password, limit: 255, null: false, default: ''
      t.integer :video_limit
      t.integer :roulette_limit
      t.string :game_limit, limit: 255, default: ''
      t.timestamps
    end
  end
end
