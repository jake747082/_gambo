class CreateBngRollbackLogs < ActiveRecord::Migration
  def change
    create_table :bng_rollback_logs do |t|
      t.integer :user_id, default: 0, null: false, unsigned: true
      t.string :session
      t.string :uid
      t.string :game_id
      t.string :game_name
      t.string :c_at
      t.string :sent_at
      t.integer :round_id, limit: 8, default: 0
      t.string :transaction_uid
      t.string :bet
      t.string :win
      t.boolean :round_started
      t.boolean :round_finished
      t.timestamps
    end

    add_index :bng_rollback_logs, :user_id
    add_index :bng_rollback_logs, :session
  end
end