class AddIsSingleWalletToGamePlatforms < ActiveRecord::Migration
  def change
    add_column :game_platforms, :is_single_wallet, :boolean, default: false, after: :status_cd
  end
end