class CreateNews < ActiveRecord::Migration
  def change
    create_table :news, force: true do |t|
      t.string :title, limit: 255, null: false, default: ''
      t.text :content
      t.boolean :admin_port, default: false, null: false
      t.boolean :agent_port, default: false, null: false
      t.boolean :www_port, default: false, null: false
      t.boolean :deleted, default: false, null: false
      t.timestamps
    end
  end
end
