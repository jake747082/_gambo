class DeviseCreateAgents < ActiveRecord::Migration
  def change
    create_table(:agents, force: true) do |t|
      ## Database authenticatable
      t.string :nickname, default: '', null: false, limit: 32 #暱稱
      t.string :username,              null: false, default: "" #帳號
      t.string :encrypted_password,    null: false, default: "" #加密密碼

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false, unsigned: true #登入次數
      t.datetime :current_sign_in_at #最近登入
      t.datetime :last_sign_in_at #最近登入
      t.string   :current_sign_in_ip #最近登入ip
      t.string   :last_sign_in_ip #最近登入ip

      # Credit & Ratio
      t.decimal :credit_max, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.decimal :credit_dispatched, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.decimal :credit_used, null: false, default: 0, precision: 15, scale: 2
      t.decimal :casino_ratio, null: false, default: 0, unsigned: true, precision: 4, scale: 1 #佔成
      t.decimal :commission_ratio, null: false, default: 0, unsigned: true, precision: 4, scale: 1

      # Counters
      t.integer :total_users_count, null: false, default: 0, null: false, unsigned: true #會員數
      t.integer :total_agents_count, null: false, default: 0, null: false, unsigned: true #代理數
      t.integer :agents_count, null: false, default: 0, null: false, unsigned: true
      t.integer :total_machines_count, null: false, default: 0, null: false, unsigned: true
      
      # Nested structure
      t.integer :parent_id, unsigned: true
      t.integer :lft, null: false, unsigned: true
      t.integer :rgt, null: false, unsigned: true

      # Level enum
      t.integer :agent_level_cd, default: 0, null: false, limit: 1, unsigned: true

      # Status
      t.boolean :lock, default: false, null: false #是否啟用(0:是, 1:否)
      t.string :maintain_code, unique: true, limit: 10
      t.timestamps
    end
    add_index :agents, :username, unique: true
    add_index  :agents, :parent_id
    add_index  :agents, :rgt
    add_index  :agents, [:lft, :rgt]
  end
end