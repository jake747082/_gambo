class AddAgentToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :agent_id, :integer, unsigned: true, after: :user_id
  end
end