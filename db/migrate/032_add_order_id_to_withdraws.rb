class AddOrderIdToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :order_id, :string, null: false, default: '', after: :id
    add_column :withdraws, :trans_id, :string, null: false, default: '', after: :order_id

    add_index :withdraws, :order_id
  end
end
