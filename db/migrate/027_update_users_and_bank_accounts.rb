class UpdateUsersAndBankAccounts < ActiveRecord::Migration
  def change
    remove_column :bank_accounts, :mobile
    remove_column :bank_accounts, :ssn
    remove_column :bank_accounts, :title
    remove_column :bank_accounts, :sub_bank_name
    add_column :bank_accounts, :city, :string, limit: 255, default: ''
    add_column :bank_accounts, :province, :string, limit: 255, default: ''
    add_column :bank_accounts, :subbranch, :string, limit: 255, default: ''
    # 0: phone, 1: email
    add_column :users, :auth_type_cd, :boolean, default: 0, after: :username
    add_column :users, :email, :string, limit: 255, default: '', after: :auth_type_cd
    add_column :users, :mobile, :string, limit: 20, default: '', after: :email
    add_column :users, :user_group_id, :integer, unsigned: true, after: :nickname
    add_column :users, :name, :string, limit: 255, default: '', after: :nickname
    add_column :users, :qq, :string, limit: 255, default: '', after: :nickname
    add_column :users, :play_total, :decimal, default: 0, null: false, precision: 15, scale: 2, after: :lock
    add_column :users, :cashout_total, :decimal, default: 0, null: false, unsigned: true, precision: 15, scale: 2, after: :lock
    add_column :users, :cashin_total, :decimal, default: 0, null: false, unsigned: true, precision: 15, scale: 2, after: :lock
    add_column :users, :cashout_count, :integer, default: 0, null: false, unsigned: true, precision: 15, scale: 2, after: :lock
    add_column :users, :cashin_count, :integer, default: 0, null: false, unsigned: true, precision: 15, scale: 2, after: :lock
  end
end
