class ChangeDecimalToUsers < ActiveRecord::Migration
  def change
    change_column :users, :credit, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :users, :credit_used, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :users, :cashout_limit, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :users, :cashin_total, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :users, :cashout_total, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :users, :play_total, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :user_cumulative_credits, :total, :decimal, default: 0, null: false, precision: 18, scale: 4
    change_column :user_daily_credit_logs, :total, :decimal, default: 0, null: false, precision: 18, scale: 4
  end
end