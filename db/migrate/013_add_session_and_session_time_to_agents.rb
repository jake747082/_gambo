class AddSessionAndSessionTimeToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :session, :string, after: :secret_key
    add_column :agents, :session_time, :datetime, after: :session
  end
end
