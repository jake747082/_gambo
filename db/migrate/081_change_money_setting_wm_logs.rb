class ChangeMoneySettingWmLogs < ActiveRecord::Migration
  def change
    change_table :wm_transfer_logs do |t|
      t.change :money, :decimal, {default: 0, null: false, unsigned: false, precision: 15, scale: 2}
    end
    
    change_table :wm_bet_return_logs do |t|
      t.change :money, :decimal, {default: 0, null: false, unsigned: false, precision: 15, scale: 2}
    end
  end
end