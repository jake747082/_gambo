class AddJpToSiteConfigs < ActiveRecord::Migration
  def change
    change_table(:site_configs, force: true) do |t|
      t.decimal :bet_total_credit, precision: 15, scale: 4, default: 0.0 #下注總額度
      t.decimal :jp_primary, precision: 12, scale: 4, default: 0.0 #jp
      t.decimal :base_jp, precision: 12, scale: 4, default: 0.0 # jp固定值
      t.decimal :jp_reset, precision: 15, scale: 4, default: 0.0 # jp重置值
      t.decimal :jp_rate, precision: 5, scale: 2, default: 0.0 # 大jp n
    end
  end
end