class AddAdminBankAccountToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :admin_bank_account_info, :string, after: :bank_account_info
    add_column :withdraws, :deposit_at, :string, after: :admin_bank_account_info
  end
end
