class AddWithdrawOrderInfo < ActiveRecord::Migration
  def change
    add_column :withdraws, :order_info, :text
  end
end
