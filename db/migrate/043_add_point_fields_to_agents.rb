class AddPointFieldsToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :points, :decimal, default: 0, null: false, unsigned: true, precision: 15, scale: 2, after: :last_sign_in_at #目前額度	
    add_column :agents, :commission_view, :boolean, default: 0, null: false, after: :lock
    add_column :agents, :commission_at, :datetime, after: :commission_view
    
    add_column :agents, :name, :string, default: '', null: false, limit: 32, after: :username
    add_column :agents, :mobile, :string, default: '', null: false, limit: 20, after: :name
    add_column :agents, :qq, :string, default: '', null: false, limit: 32, after: :mobile
    add_column :agents, :email, :string, default: '', null: false, limit: 32, after: :qq
    add_column :agents, :type_cd, :integer, default: 0, null: false, limit: 1, unsigned: true, after: :email #類型(0:現金, 1:買分)
    add_column :agents, :reviewing, :integer, default: 0, null: false, limit: 1, unsigned: true, after: :lock #是否審核(0:是, 1:否)
  end
end