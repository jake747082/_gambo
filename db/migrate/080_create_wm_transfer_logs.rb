class CreateWmTransferLogs < ActiveRecord::Migration
    def change
      create_table :wm_transfer_logs do |t|
        t.string :account_name , null: false, default: '', unique: true
        t.decimal :money , default: 0, null: false, unsigned: true, precision: 15, scale: 2
        t.string :request_date, limit: 100, default: ''
        t.string :dealid, limit: 50, default: '', unique: true
        t.string :gtype, limit: 50, default: ''
        t.string :bet_type, limit: 50, default: ''
        t.string :betdetail, limit: 100, default: ''
        t.string :gameno, limit: 100, default: ''
        t.string :code, limit: 50, default: ''
        t.string :category, limit: 50, default: ''
        t.integer :user_id, default: 0, null: false, unsigned: true
        t.string :result_status, limit: 50, default: ''
        t.string :bet_returned, limit: 50, default: ''
        t.timestamps
      end
      
      add_index :wm_transfer_logs, :user_id
    end
end