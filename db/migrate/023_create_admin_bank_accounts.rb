class CreateAdminBankAccounts < ActiveRecord::Migration
  def change
    create_table :admin_bank_accounts do |t|
      # 銀行ID
      t.string :bank_id, limit: 255, default: ''
      # 支行名稱
      t.string :sub_bank_name, limit: 255, default: ''
      # 戶名
      t.string :title, limit: 255, default: ''
      # 帳號
      t.string :account, limit: 255, default: ''
      # 累積金額
      t.decimal :cumulative_amount, precision: 11, scale: 2, default: 0
      # 存取次數限制
      t.integer :limit_update_count, default: 0, null: false, unsigned: true
      # 存取次數
      t.integer :update_count, default: 0, null: false, unsigned: true
      # 存取款設定
      t.boolean :deposit, default: true, null: false
      # 開啟
      t.boolean :open, default: false, null: false
      t.timestamps
    end

    add_index :admin_bank_accounts, :deposit
    add_index :admin_bank_accounts, :open
  end
end
