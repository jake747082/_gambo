class CreateBngReplies < ActiveRecord::Migration
  def change
    create_table(:bng_replies, force: true) do |t|
      t.string :uid
      t.text :reply_data
      t.timestamps
    end
    add_index :bng_replies, :uid
  end
end
