class AddColumnPlayFreeToPlatformGames < ActiveRecord::Migration
  def change
    add_column :platform_games, :play_free, :boolean, default: false
  end
end