class CreateBtTransferLogs < ActiveRecord::Migration
  def change
    create_table :bt_transfer_logs do |t|
      t.string :order_id, null: false, default: '', unique: true
      t.string :trans_id, null: false, default: '', unique: true
      t.integer :cash_type_cd, unsigned: true, limit: 2
      t.integer :transfer_type, unsigned: true, limit: 2
      t.decimal :credit, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.decimal :credit_before, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.decimal :credit_after, default: 0, null: false, unsigned: true, precision: 15, scale: 2
      t.integer :status, default: 0, null: false, unsigned: true
      t.integer :user_id, default: 0, null: false, unsigned: true
      t.integer :currency_cd, null: false, default: 0, unsigned: true
      t.text :order_info, :text
      t.timestamps
    end
    
    add_index :bt_transfer_logs, :user_id
    add_index :bt_transfer_logs, :cash_type_cd
    add_index :bt_transfer_logs, :order_id
  end
end
