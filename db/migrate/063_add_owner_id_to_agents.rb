class AddOwnerIdToAgents < ActiveRecord::Migration
  def change
    # 代理所屬id
    add_column :agents, :owner_id, :integer, unsigned: true, after: :type_cd
    add_index  :agents, :owner_id
  end
end
