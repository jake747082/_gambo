class AddWithdrawAdminNote < ActiveRecord::Migration
  def change
    add_column :withdraws, :admin_note, :text
  end
end
