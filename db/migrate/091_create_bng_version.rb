class CreateBngVersion < ActiveRecord::Migration
  def change
    create_table :bng_version do |t|
      t.integer :user_id, default: 0, null: false, unsigned: true
      t.string :last_updated
      t.integer :version, limit: 8, default: 0
    end
  end
end